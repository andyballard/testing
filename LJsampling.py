import numpy as np
from database_eigenvecs import *
from pele.utils.hessian import get_sorted_eig
from pele.mc import MonteCarlo
from pele.takestep import displace 
from bh_sampling import *
# from ljcluster_new import *
#import ljcluster_new
import pele.systems as sys
from pele.thermodynamics import free_energy
from pele.mindist import MinPermDistAtomicCluster, ExactMatchAtomicCluster

import pylab as plt

class dFSimulation():

    def __init__(self,natoms=31,ntypeA=30,epsAB=1.2,sigAB=1.0,epsBB=1.0,sigBB=1.0,rcut=10.0,T=0.02,numit=100,numperm=10):

        self.natoms = natoms 
        self.ntypeA = ntypeA
        self.numit = numit
        self.numperm = numperm
        self.T = T

        self.epsAB = epsAB
        self.sigAB = sigAB
        self.epsBB = epsBB
        self.sigBB = sigBB
        self.rcut = rcut

        #self.system = ljcluster_new.LJCluster_new(natoms)
        self.system = sys.LJCluster(natoms)

        self.f = open("wf"+str(epsAB),"w")
        self.fr = open("wr"+str(epsAB),"w")

        # system = sys.LJCluster(natoms)
        self.db = self.system.create_database()
        self.pot = self.system.get_potential()

        # initialize Binary LJ system
        self.system_blj = sys.BLJCluster(self.natoms,ntypeA=self.ntypeA,epsAB=self.epsAB,sigAB=self.sigAB,rcut=self.rcut)
        self.pot_blj = self.system_blj.get_potential()
        self.db_blj = self.system_blj.create_database("db_blj.sqlite")

        # minpermdist objects
        self.quench_homogeneous = self.system.get_minimizer()
        permlist = [range(natoms)]
        self.mindist = MinPermDistAtomicCluster(niter=100, permlist=permlist, verbose=False)

        dat = np.loadtxt("/home/ab2111/projects/testing/input_files/min.data")
        energylist = dat[:,0]

        cdat = np.loadtxt("/home/ab2111/projects/testing/input_files/points.min")
        coordsall = np.reshape(cdat,3*len(cdat))

        Nminima = int(len(coordsall)/(3*natoms))
        print natoms,len(coordsall),coordsall
        coordlist = np.split(coordsall,Nminima)

        for i in range(Nminima):
            energy = energylist[i]
            coords = coordlist[i]
            m = self.db.addMinimum(energy, coords)

        coords = coordlist[0]
        energy = energylist[0]
    #     epair = NormalModes(m, eval, evec, nzero=6, nnegative=0)
        e, g, hess = self.pot.getEnergyGradientHessian(coords)
        eval, evec = get_sorted_eig(hess)

        get_thermodynamic_information(self.system,self.db)


        m = self.db.minima()[0]
        nm = m.hessian_eigs[0]
        print nm.eigenvalues
        exit()

    def makedb_blj(self,minimum=0):

        db_blj = self.db_blj
        
        quench = self.system_blj.get_minimizer()

        c0 = self.db.minima()[minimum].coords
        
        catoms0 = np.reshape(c0,(self.natoms,3))

        """
        # only one min
        cperm = np.copy(c0)
        catoms = np.copy(catoms0)
        (catoms[self.natoms-1],catoms[self.natoms-10])=(catoms0[self.natoms-10],catoms0[self.natoms-1])
        cperm = np.reshape(catoms,3*self.natoms)
        ret = quench(cperm)
        m = db_blj.addMinimum(ret.energy, ret.coords)
        """
        for i in range(self.natoms):
            catoms = np.copy(catoms0)
#             cperm = np.copy(c)
            (catoms[self.natoms-1],catoms[i])=(catoms0[i],catoms0[self.natoms-1])
            cperm = np.reshape(catoms,3*self.natoms)
            ret = quench(cperm)
            m = db_blj.addMinimum(ret.energy, ret.coords)

        get_thermodynamic_information(self.system_blj,db_blj)

        E = np.array([ m.energy for m in db_blj.minima() ])
        pgo = np.array([ m.pgorder for m in db_blj.minima() ]) 
        vib = np.array([ m.fvib for m in db_blj.minima() ] )

        self.Pw = self.calcPw(E,pgo,vib,self.system.k,self.T)
        # TODO : adjust Pw vals to reflect degeneracies in db creation

        return db_blj

    def calcPw(self,E,pgo,vib,kappa,T):

        F=free_energy(E,pgo,vib,T,kappa,h=1)
        Fmin=np.min(F)

        P=np.exp(-(F-Fmin)/T)
        dummy=0.0
        for i in range(len(F)): dummy = dummy + P[i]
        P = P / dummy

        return P

    def select_minimum(self,Pw,T):

        ran = random.random()
        dummy = 0.0
        i = -1
        while dummy < ran:
            i = i + 1
            dummy = dummy + Pw[i];

        return i 

    def runr(self):

        self.db_blj = self.makedb_blj()

        for i in range(self.numit):
#             m = self.db_blj.minima()[np.random.randint(len(self.db_blj.minima()))]
            minimum = self.select_minimum(self.Pw,self.T)
            m = self.db_blj.minima()[minimum]
            (cb0,Eb0) = sample_uniformly_in_basin(m,-100.0,self.pot_blj,self.system.k, self.T)
            Eb1 = self.pot.getEnergy(cb0)
            wr = (Eb1-Eb0)/self.T
            self.fr.write(str(wr)+"\n")

    def run(self):

    #     sample_from_database(system,db.minima(),-100.0)
        m = self.db.minima()[0]
        for i in range(int(self.numit/self.natoms)):

            # forward direction permutations 
            (c0,E0) = sample_uniformly_in_basin(m,-100.0,self.pot,self.system.k, self.T)
            catoms0 = np.reshape(c0,(self.natoms,3))
            for j in range(self.natoms):
#             for j in range(self.natoms-10,self.natoms-9):
#                 c = np.copy(c0)
                catoms = np.copy(catoms0)
#                 (c[self.natoms-1],c[j])=(c0[j],c0[self.natoms-1])
#                 catoms = np.reshape(c,(self.natoms,3))
                (catoms[self.natoms-1],catoms[j])=(catoms0[j],catoms0[self.natoms-1])
                c = np.reshape(catoms,3*self.natoms)
#                 np.random.shuffle(catoms)
                E1 = self.pot_blj.getEnergy(c)
#                 E1 = self.pot.getEnergy(c)
                w = (E1-E0)/self.T
                self.f.write(str(w)+"\n")


    def sample_hsa(self,temperature):
        
        m = self.db.minima()[0]
        
        # forward direction permutations 
        coords = sample_uniformly_in_basin_harmonic(m,-100.0,self.system.k, self.T)
        Ehsa = self.calc_Ehsa(m,coords)
        
        return coords,Ehsa
    
    def calc_Ehsa(self,m,coords):
        
        coords0 = m.coords
        nm = m.hessian_eigs[0]
        evals = nm.eigenvalues
        vectors = nm.eigenvectors
        
#        (e,g,Hess) = self.pot.getEnergyGradientHessian(coords0)

#        E = 0.5 * np.dot(coords-coords0,np.dot(Hess,coords-coords0))
        
        x0 = self.quench_homogeneous(coords).coords
#        x0 = coords0       
        (e,g,Hess) = self.pot.getEnergyGradientHessian(x0)
        #E = 0.5 * np.dot(coords-x0,np.dot(Hess,coords-x0))
        
#        dist, dummy, newcoords = self.mindist(x0, coords)
#        coords = newcoords
        E = 0.5 * np.dot(coords-x0,np.dot(Hess,coords-x0))
#        print "ediff ", Eminperm-E
        return m.energy + E
    
    def get_cnumbers(self,coords,evecs):
    
        c = np.zeros(len(evecs))
        for i,evec in enumerate(evecs):
            c[i] = np.dot(evec,coords)
        return c
    
    def apply_map(self,m,coords):
        
        coords0 = m.coords
        nm = m.hessian_eigs[0]
        evals = nm.eigenvalues
        vectors = nm.eigenvectors

        c = self.get_cnumbers(coords,vectors)
        alpha = np.zeros(len(evals))
        alpha[6:] = 0.00025 * np.max(evals) * np.divide(1.0,evals[6:])
        #print evals,alpha
        x = np.zeros(len(coords))
        #x = np.copy(coords)

        for i in range(len(evals)):
            x = x + (1.+ alpha[i]) * c[i] * vectors[i] 
    
        return x
        
    def apply_map_inverse(self,m,coords):
        
        #coords0 = m.coords
        nm = m.hessian_eigs[0]
        evals = nm.eigenvalues
        vectors = nm.eigenvectors

        #dist, dummy, newcoords = self.mindist(coords0, coords)

        c = self.get_cnumbers(coords,vectors)
        alpha = np.ones(len(evals))
        alpha[6:] = (np.max(evals)*1./0.00025) * evals[6:]
        
        x = np.zeros(len(coords))
        
        for i in range(len(evals)):
            x = x + alpha[i] * c[i] * vectors[i]
            
        #for i,evec in enumerate(vectors):
        #    coords = coords + alpha[i] * evec 
                
        return x        
        
    def sample_crystal(self,temperature,kappa):
    
        # sample configuration from an Einstein crystal with lattice points given by minimum m
        
        m = self.db.minima()[0]

        x0 = m.coords
        x = x0
        ranvec = np.array([np.random.normal() for i in range(len(x))])
        for i in range(len(x)): 
            x[i] = x0[i] + np.sqrt(temperature/kappa) * ranvec[i]

        Ehsa = m.energy + 0.5 * temperature * np.dot(ranvec,ranvec)
        E = self.pot.getEnergy(x)
    
        return x,Ehsa,E

    def calc_crystal_energy(self,coords,kappa):
        
        # calculate energy of configuration coords as einstein crystal
        
        m = self.db.minima()[0]
        
        x0 = self.quench_homogeneous(coords).coords
        #x0 = m.coords
        
        E = m.energy + 0.5 * kappa * np.dot(coords-x0,coords-x0)
        #for i in range(len(coords)):
        #    E = E + 0.5*kappa*(coords[i]-x0[i])**2
        
        return E
    
    def sample_square_well(self,R):
        
        mcoords = self.db.minima()[0].coords
        N = len(mcoords)
        ranvec = np.array([np.random.normal() for i in range(N)])
        r = np.dot(ranvec,ranvec)
        r = np.sqrt(r)
        
        unit_ran = np.random.random()
        coords = unit_ran**(1./N) * ranvec / r
        
        coords = R * coords
        
        return mcoords + coords
    
    def calc_square_well_energy(self,coords,R):
        
        mcoords = self.db.minima()[0].coords
        
        r = self.calc_r(coords)
        
        if r < R:
            return 0.0
        else:
            return float("inf") 

    def calc_r(self,coords):
        
        #mcoords = self.db.minima()[0].coords
        
        ret = self.quench_homogeneous(coords)
        mcoords = ret.coords
        menergy = ret.energy
        
        r = 0.0
        for i in range(len(mcoords)):
            r = r + (coords[i]-mcoords[i])**2
        r = np.sqrt(r)
    
        return r
    
    def display(self,F,R,kappa):
        
        plt.plot(F)
        plt.plot(R)
        plt.legend(["work F","work R"])
        plt.show()
        
    def iterate(self,temperature,kappa,R):
        
        coords = self.db.minima()[0].coords
        
        step = displace.RandomDisplacement(stepsize=0.0025)
        mc = MonteCarlo(coords,self.pot,temperature=temperature,takeStep=step,iprint=100000)
        
        #equilibrate
        mc.run(10000)
        print 'equilibration complete, R = ',R

        wFlist = []
        wRlist = []
        rlist = []
        n = 0
        for i in range(1000):
            
            #if i % 100 == 0:
            #    print "kappa ", kappa, " iteration ",i
            #epsAB = 0.9 + i*1./3*(0.3)
            #sim = dFSimulation(numit=numit,epsAB=epsAB,ntypeA=31,natoms=31)
            #sim.runr()
            #sim.run()
        
#            (chsa,Ehsa) = self.sample_hsa(temperature)
#            outE.write(str(Ehsa)+"\n")

#            chsaprime = sim.apply_map(m, chsa)
#            E = self.pot.getEnergy(chsa)

#            wF = (E-Ehsa)/temperature
#            Ehsaprime = sim.pot.getEnergy(chsaprime)
#            wF = (Ehsaprime-Ehsa)/temperature
#            print E,Ehsa,Ehsaprime-E,wF, "  ",
        
            mc.run(100)
            c = mc.coords
            #print self.calc_square_well_energy(c,R), self.pot.getEnergy(c),
            wR = (self.calc_square_well_energy(c,R)-self.pot.getEnergy(c))/temperature
#            wR = (self.calc_crystal_energy(c,kappa)-self.pot.getEnergy(c))/temperature
            if self.calc_square_well_energy(c, R)==0.0: n = n + 1
#        Ehsa = sim.calc_Ehsa(m,c)
#        E = sim.pot.getEnergy(c)        
#        cprime = sim.apply_map_inverse(m,c)
#        Eprime = sim.calc_Ehsa(m,cprime)
#        wR = (Ehsa-E)/temperature
#        wR = (Eprime-E)/temperature
#        print Ehsa,E,Eprime-Ehsa,wR
#        print wF,wR,wF+wR
            rlist.append(self.calc_r(c))

            x = self.sample_square_well(R)
            Ehsa = 0.0
            E = self.pot.getEnergy(x)
#            (x,Ehsa,E) = self.sample_crystal(temperature,kappa)
            wF = (E-Ehsa)/temperature
            #print Ehsa, E, wF+wR
#            print self.calc_crystal_energy(c,kappa),self.pot.getEnergy(c),wR
#        ctest = sim.apply_map(m,cprime)
#        Etest = sim.pot.getEnergy(ctest)
#        print Etest,E
#            print Ehsa,self.calc_crystal_energy(x,kappa), Ehsa-self.calc_crystal_energy(x,kappa)
        
#            print Ecrystal,self.pot.getEnergy(c),wR
#            out.write(str(wF)+" "+str(wR)+" "+str(wF+wR)+"\n")
            #np.savetxt(out, (wF,wR,wF+wR),fmt="%f %f %f")
            #print wF,wR,wF+wR
            wFlist.append(wF)
            wRlist.append(wR)
        

    #for i in range(numit):
    #    mc.run(100)
    #    c = mc.coords
    #    E = sim.pot.getEnergy(c)
    #    outE.write(str(E)+"\n")
    #    (chsa,Ehsa) = sim.sample_hsa(temperature)
    #    outE.write(str(Ehsa)+"\n")
    #exit()
        #plt.plot(wRlist)
        #plt.show()
        
        wFlist = np.array(wFlist)
        wRlist = np.array(wRlist)
        
        wFave = np.sum(wFlist) / len(wFlist)
        wRave = np.sum(wRlist) / len(wRlist)
        #wRave = 0.001 * n 
        print "temp ", temperature, " wF, wR wF+wR",wFave,wRave,wFave+wRave
        
        print np.min([x for x in -wRlist if x != float("-inf")]),
        print np.max([x for x in -wRlist if x != float("inf")])
        
        Fmin = np.min(wFlist)
        Fmax = np.max(wFlist)
        Rmin = np.min([x for x in -wRlist if x != float("-inf")])
        Rmax = np.max([x for x in -wRlist if x != float("inf")])

        wFhst = np.histogram(wFlist, np.arange(Fmin,Fmax,step=0.01*(Fmax-Fmin)), normed=1)
        wRhst = np.histogram(-wRlist,np.arange(Rmin,Rmax,step=0.01*(Rmax-Rmin)),normed=1)
        #plt.hist(wFlist, np.arange(np.min(wFlist),np.max(wFlist)), normed=1)
        #plt.hist(-1*wRlist,np.arange(np.min([x for x in -wRlist if x != float("-inf")]),np.max([x for x in -wRlist if x != float("inf")])),normed=1)
        #plt.show()
        
        np.savetxt("wF"+str(R)+".hst",np.column_stack((wFhst[0],wFhst[1][1:])))
        np.savetxt("wR"+str(R)+".hst",np.column_stack((wRhst[0],wRhst[1][1:])))

        return wFhst, wRhst
    
def main():
    numit=10000
    temperature = 0.025
    Niter = 10
    

    sim = dFSimulation(numit=numit,ntypeA=31,natoms=31,T=temperature)


#    out = open("work","w")
    wFaveall = np.zeros(Niter)
    wRaveall = np.zeros(Niter)
    kappaall = np.zeros(Niter)
    tempall = np.zeros(Niter)

    kappa = 125. # minimizes <w_tot> @ T=0.0125
    
    wF = []
    wR = []
    
    
    for i in range(Niter):
        #kappa = 5.0 + 5*i
        #temperature = 0.0125 + 0.00125*i
        R = 0.2 + 0.05*i
        wFlist,wRlist = sim.iterate(temperature,kappa,R)
        wF.append(wFlist)
        wR.append(wRlist)
        #wFaveall[i] = wFave
        #wRaveall[i] = wRave
        #kappaall[i] = kappa
        tempall[i]=temperature
        #data.append([wFave,wRave,kappa])
    
    print wFaveall
    print wRaveall
    print tempall
    sim.display(wFaveall,wRaveall,kappaall)

if __name__ == "__main__":
    main()
