import numpy as np
import matplotlib.pyplot as plt
from matplotlib import cm
from mpl_toolkits.mplot3d import Axes3D
import pele

def Energy(x,y,m,b):
    
    resid = y - (m*x+b)
    return 0.5*np.dot(resid,resid)

B=np.arange(0,5,0.01)
M=np.arange(0,5,0.01)
sigma = 3.0

m = 2.0
b = 1.0
x = np.arange(0,5,0.01)
#y = m*x*(1-x) + b + np.sqrt(sigma)*np.random.random(len(x))
y = m*x + b + np.sqrt(sigma)*np.random.random(len(x))


Es = np.zeros((len(B),len(M)))
Es[0,1] = 1
#BM0 = []
#BM1 = []

for i,b in enumerate(B):
    for j,m in enumerate(M):
        #BM0.append(b)
        #BM1.append(m)
        #print Energy(x,y,m,b)
        Es[i,j]= Energy(x,y,m,b)

print Energy(x,y,2.0,1.0)
print Energy(x,y,0,0)
B,M = np.meshgrid(B,M)
fig = plt.figure()
#ax = fig.add_subplot(111,projection='3d')
ax = Axes3D(fig)
ax.contour(B,M,Es, rstride=1, cstride=1, cmap=cm.coolwarm, antialiased=False)
plt.show()
#plt.plot(B,Es)
#plt.show()

