"""
an example of how to create a new potential.
"""

from pele.potentials import BasePotential
from pele.takestep import RandomDisplacement,RandomCluster

class LinearModel(object):
    
    def __init__(self,npoints=100,sigma=0.3,points=None):
            
        self.npoints = npoints    
        
        if points == None:
            self.points = self.generate_points()
        else:
            self.points = points
            
    def generate_points(self,R=1.0):
        
        theta = 2*np.pi*np.random.random(self.npoints)    
        r = np.random.random(10*self.npoints)
        rnew = []
        for ri in r:
            if np.random.random()<(ri/R):   
                rnew.append(ri)
            if len(rnew)==self.npoints:
                break
        r = rnew

        x = r * np.cos(theta)
        y = r * np.sin(theta)       
        
        #plt.plot(x,y,'x')
        #plt.show() 
        #exit()
        return zip(x,y)
    
    def model(self,x,params):
        
        return params[0]*x  
    
        
class MyModel(object):
    
    def __init__(self,params0=None,npoints=100,sigma=0.3):
        
        self.params0 = params0
        self.nparams = len(self.params0)
        self.sigma = sigma
        
        self.points = None
        self.npoints = npoints

        self.points = self.generate_points()
        
    def generate_points(self):
        
        x = np.random.random(self.npoints)
        z = np.random.random(self.npoints)
        y = self.model(x,z,self.params0) + np.random.normal(scale=self.sigma,size=self.npoints)
        
        return zip(x,z,y) 
    
    def model(self,x,z,params):
        
        return params[0] + np.sqrt((params[1] - x)**2 + (params[2] - z)**2)         

class MyPot(BasePotential):
    """a quadratic potential for fitting
    
    V(xi,yi|alpha) = 0.5 * (yi-(f(xi|alpha))**2
    where 
    f(x|alpha) = alpha_0 + alpha_1 * x
    """
    def __init__(self,model):
        """ instance of MyModel class"""
        self.model = model
        self.points = self.model.points
        
                
    def getEnergy(self,params):
        
        #params = np.reshape(params, [self.natoms,3])
        E = 0.
        #for xi,zi,yi in self.points:
            #E += 0.5 * (yi - self.model.model(xi,zi,params))**2
        for xi,yi in self.points:
            E -= np.abs((xi + params[0]*yi)*(1./(np.sqrt(1+params[0]**2))))
        
        return E
    
    
    """
    def getEnergyGradient(self, coords):
        coords = np.reshape(coords, [self.natoms,3])
        E = 0.
        grad = np.zeros(coords.shape)
        for i in range(self.natoms):
            for j in range(i):
                dr = coords[i,:] - coords[j,:]
                r = np.sqrt(np.sum(dr**2)) 
                E += 4. * (r**(-24) - r**(-12))
                g = 4. * ( 24. * r**(-25) - 12. * r**(-13))
                grad[i,:] += -g * dr/r
                grad[j,:] += g * dr/r
        return E, grad.reshape(-1)
    """
def align1d(x1, x2):
    # align the center of mases
    #dr = np.mean(x1) - np.mean(x2)
    #x2 += dr
    dist = np.linalg.norm(x2 - x1)
    return dist, x1, x2
    
from pele.systems import BaseSystem
from pele.mindist import MinPermDistAtomicCluster, ExactMatchAtomicCluster
from pele.transition_states import orthogopt
class MySystem(BaseSystem):
    def __init__(self, natoms, model):
        super(MySystem, self).__init__()
        self.natoms = natoms
        self.model = model
        self.params.database.accuracy =0.1

    def get_potential(self):
        return MyPot(self.model)
    
    def get_mindist(self):
        # no permutations of parameters
        return align1d
        #permlist = []
        #return MinPermDistAtomicCluster(permlist=permlist, niter=10)

    def get_orthogonalize_to_zero_eigenvectors(self):
        return orthogopt
    
    def get_minimizer(self, **kwargs):
        return lambda coords: myMinimizer(coords, self.get_potential(),**kwargs)
    
    def get_compare_exact(self, **kwargs):
        # no permutations of parameters
        mindist = self.get_mindist()
        return lambda x1, x2: mindist(x1, x2)[0] < 1e-3

def myMinimizer(coords,pot,**kwargs):
    from pele.optimize import lbfgs_cpp as quench
    print coords
    return quench(coords,pot,**kwargs)

import numpy as np

def generate_dataset(pot,length,alpha):
    
    sigma = 0.001
    x0 = 0.1
    #x = np.arange(0,0.01*length,0.01)
    x = np.random.random(100)
    x = x - 0.5
    z = np.random.random(100)
    z = z - 0.5
    y = (x-alpha[1])**2 + (z-alpha[2])**2
    y = np.sqrt(y)+alpha[0]
    #y = alpha[0] + np.sqrt(np.power((alpha[1] - x),2))
    #y = alpha[0] + np.sqrt(np.power((alpha[1] - x),2) + np.power((alpha[2] - z),2)) + np.random.normal(scale=sigma,size=len(x))
    #y = alpha[0] + alpha[1]* x + np.random.normal(scale=sigma,size=len(x))
    
    # two times the data...
    #x = x - 0.5
    #z = alpha[0] - alpha[1]* x + np.random.normal(scale=sigma,size=len(x))
    #y = alpha[0] + alpha[1]*x*x + np.random.normal(scale=sigma,size=len(x))
    #x = [x,x]
    #y = [y,z]
    #x = np.array(x).flatten()
    #y = np.array(y).flatten()
    #print x
    #print y
    #exit()
    
    return zip(x,z,y)

def search_landscape(alpha):
    
    from matplotlib import cm
    from mpl_toolkits.mplot3d import Axes3D
    
    model = MyModel(alpha)
    system = MySystem(len(alpha),model)
    
    B=np.arange(-0.2,0.2,0.01)
    M=np.arange(-0.2,0.2,0.01)
    Es = np.zeros((len(B),len(M)))
   
    for i,b in enumerate(B):
        for j,m in enumerate(M):
            #BM0.append(b)
            #BM1.append(m)
            #print Energy(x,y,m,b)
            print i,j
            Es[i,j]= system.get_potential().getEnergy([0.0,m,b])

    B,M = np.meshgrid(B,M)
    fig = plt.figure()
    #ax = fig.add_subplot(111,projection='3d')
    ax = Axes3D(fig)
    #ax.contour(B,M,Es, rstride=1, cstride=1, cmap=cm.coolwarm, antialiased=False)
    ax.contour(B,M,Es, rstride=0.2, cstride=0.2, cmap=cm.coolwarm, antialiased=False)
    #ax.plot_wireframe(B,M,Es, rstride=2, cstride=2, cmap=cm.coolwarm, antialiased=False)
    plt.show()
    
def run_basinhopping(nsteps):
    
    # initial data:
    #alpha = [0.0,1.0,1.0]
    #dat = generage_dataset(MyPot(100),100,alpha)
    alpha = [0.0]
    points = [(1.0,1.0),(1.0,-1.0)]
    nparams = len(alpha)
    natoms = nparams    
    #model = MyModel(alpha)
    #model = LinearModel(npoints=2,points=points)
    model = LinearModel(npoints=200)
    
    
    system = MySystem(natoms,model)
    #print system.get_potential().getEnergy(alpha)

    database = system.create_database()
    x0 = np.random.uniform(-1,1,[natoms])
    
    step = RandomDisplacement(stepsize=10.3)
    #step = RandomCluster(volume=10.0)
    bh = system.get_basinhopping(database=database, takestep=step,coords=x0,temperature = 10.0)
    #bh.stepsize = 20.
    
    bh.run(nsteps)
    print "found", len(database.minima()), "minima"
    min0 = database.minima()[0]
    print "lowest minimum found has energy", min0.energy
    m0 = database.minima()[0]
    mindist = system.get_mindist()
    for m in database.minima():
        dist = mindist(m0.coords, m.coords.copy())[0]
        print "   ", m.energy, dist
    return system, database

def run_double_ended_connect(system, database):
    # connect the all minima to the lowest minimum
    from pele.landscape import ConnectManager
    manager = ConnectManager(database, strategy="gmin")
    for i in xrange(database.number_of_minima()-1):
        min1, min2 = manager.get_connect_job()
        connect = system.get_double_ended_connect(min1, min2, database)
        connect.connect()

from pele.utils.disconnectivity_graph import DisconnectivityGraph, database2graph
import matplotlib.pyplot as plt

def make_disconnectivity_graph(database):
    graph = database2graph(database)
    dg = DisconnectivityGraph(graph, nlevels=3, center_gmin=True)
    dg.calculate()
    dg.plot()
    plt.show()

def test_potential():
    import numpy as np
    natoms = 5
    pot = MyPot(natoms)
    coords = np.random.uniform(-1,1,natoms*3)
    e = pot.getEnergy(coords)
    print e
    e, g = pot.getEnergyGradient(coords)
    print e

    gnum = pot.NumericalDerivative(coords, eps=1e-6)
    print np.max(np.abs(gnum-g)), np.max(np.abs(gnum))
    print np.max(np.abs(gnum-g)) / np.max(np.abs(gnum))

if __name__ == "__main__":
    
    #model = LinearModel(npoints=100)
    #model.generate_points()
    
    #alpha = [0.0, 1.0,1.0]
    #search_landscape(alpha)

    #test_potential()
    #from mpl_toolkits.mplot3d import Axes3D
    #from matplotlib import cm
   
    mysys, database = run_basinhopping(20)
    run_double_ended_connect(mysys, database)
    make_disconnectivity_graph(database)
    exit()
    for m in database.minima():
        print m.coords

    alpha = [0.0]
    nparams = len(alpha)
    natoms = nparams    
    #model = MyModel(alpha)
    #model = LinearModel(npoints=200)
    model = mysys.model
    system = MySystem(natoms,model)

    Es = []
    Theta=np.arange(-0.5*np.pi,0.5*np.pi,0.01)
    for t in Theta:
        Es.append(system.get_potential().getEnergy([np.tan(t)]))    #print mysys.get_potential().getEnergy((1.0,1.05,1.05))    
    
    #plt.plot(np.tan(Theta),Es,'x')
    plt.plot(Theta,Es,'x-')
    #plt.xlim([-10,10])
    plt.show()
    #exit()
    #print mysys.get_potential().getEnergy((1.0,-1.05,1.04))    
    #print mysys.get_potential().getEnergy((m.coords[0],m.coords[1]))    
    #print mysys.get_potential().getEnergy((m.coords[0],-m.coords[1]))    
    points = np.array(mysys.model.points)
    #plt.plot(points[:,0],np.sqrt(np.power(points[:,1],2)+np.power(points[:,2],2)),'x')
    plt.plot(points[:,0],points[:,1],'x')
    fit = [points[:,0],np.array([m.coords[0]*p for p in points[:,0]])]
    plt.plot(fit[0],fit[1],'-')
    

    #fig = plt.figure()
    #ax = Axes3D(fig)
    #ax.contour(points[:,0],points[:,1],np.diag(points[:,2]), rstride=1, cstride=1, cmap=cm.coolwarm, antialiased=False)
    #plt.show()
    #ax = fig.add_subplot(1, 2, 1, projection='3d')
    #p = ax.plot_surface(points, rstride=4, cstride=4, linewidth=0)
    
    #plt.plot(points[:,2],m.coords[0]+np.sqrt(np.power(points[:,0]-m.coords[1],2)+np.power(points[:,1]-m.coords[2],2)),'x')
    #fit = [points[:,0],np.array([m.coords[0]+m.coords[1]*p for p in points[:,0]])]

    #plt.plot(fit[0],fit[1])
    #plt.show()
