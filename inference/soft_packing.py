import numpy as np
import matplotlib.pyplot as plt
from pele.systems import AtomicCluster
from pele.potentials import HS_WCA,BasePotential
from bh import run_double_ended_connect, make_disconnectivity_graph,do_nothing_mindist,my_orthog_opt
#from pele.potentials import WCA

class WCACluster(AtomicCluster):

    def __init__(self,natoms,ndim=2):
        super(AtomicCluster, self).__init__()
        self.natoms = natoms
        self.ndim = ndim
        radii = 0.1*np.random.random(size=self.natoms)
        #self.wca = WCA(self.natoms,ndim=ndim)
        #self.wca = HS_WCA(self.natoms,1.0,1.0,radii)
        self.wca = HS_WCA(1.0,1.0,radii,ndim=self.ndim)
        #self.pot0 = self.get_potential()
        
    def get_potential(self):
        #E0 = self.wca.getEnergy(*args)
        #E0 = wcapot.getEnergy(*args)
        #return E0 + harmonic(*args)
        return WCA_with_external(self.wca,self.natoms,ndim=self.ndim)

    def get_mindist(self):
        # no permutations of parameters
        return do_nothing_mindist
    
    def get_orthogonalize_to_zero_eigenvectors(self):
        return None
        return my_orthog_opt

    def get_compare_exact(self, **kwargs):
        # no permutations of parameters
        mindist = self.get_mindist()
        return lambda x1, x2: mindist(x1, x2)[0] < 1e-3

class WCA_with_external(BasePotential):
    
    def __init__(self,wcapot,*args,**kwargs):
        #super(WCA_with_external,self).__init__()
        self.wcapot = wcapot
        
    def getEnergy(self,coords):
        return self.wcapot.getEnergy(coords) + harmonic(coords)
#    def getEnergy(self,coords):
#        return 

def harmonic(coords):
    E=0.
    k=0.1
    for c in coords:
        E = E + 0.5*k*(c)**2
    return E
  
natoms = 5
coords = np.random.random(size=(natoms*2))
  
system = WCACluster(natoms)
pot = system.get_potential()

database = system.create_database()
x0 = np.random.uniform(0.,3,[2*natoms])

#step = RandomDisplacement(stepsize=0.3)
#step = RandomCluster(volume=10.0)
bh = system.get_basinhopping(database=database,coords=x0,temperature = 10.0)
pot = system.get_potential()

bh.run(10)

for m in database.minima():
    #dist = mindist(m0.coords, m.coords.copy())[0]
    print "   ", m.energy, m.coords

run_double_ended_connect(system, database)
make_disconnectivity_graph(database)
    