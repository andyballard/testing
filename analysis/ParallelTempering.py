import numpy as np
import pylab as plt
import rlx_analysis
#from scripts.Cv_est import Cv_est
#from landscape import MinimaDistribution
import re
import os

from pt_utils import *

class HeatCapacity():
    
    def __init__(self,Edists):
        
        self.Edists = Edists
        
        #replica properties
        self.Tlist = self.Edists[0].results.Tlist
        
        self.hists = None
        self.Ebins = None
        self.Cv = None
        self.Nrep = len(self.Edists)
        self.do_wham()
        
    def do_wham(self):
        
        import histogram_reweighting.histogram_reweighting1d as weighting
        #gather E-distributions in WHAM format
        self.gather_edists()
        
        #do WHAM
        print np.shape(self.hists.transpose())
        print np.shape(self.Tlist[:-1]), np.shape(self.Ebins)
        wham = weighting.Wham1d(self.Tlist[:-1],self.Ebins,self.hists.transpose())
        print self.hists.transpose()
        wham.minimize()
        ndof=3*75-6
        plt.plot(wham.logn_E)
        plt.show()
        exit()
        self.Cv = wham.calc_Cv(ndof)
        
    def gather_edists(self):
        
        #print self.Edists[0].E
        #print len(self.Edists[0].E[1])
        #exit()
        self.hists = np.ndarray(shape=(len(self.Edists[0].E[1]),self.Nrep))
        for e in self.Edists:
            np.append(self.hists,e.E[0])
        
        self.Ebins = e.E[1]
        
        print self.hists
        exit()
        print self.hists,self.Ebins
    
class Wdistribution(object):
    
    def __init__(self,results,respair,maxlines=None,distrange=None,nbins=100):
        
        self.results = results
        
        self.respair = respair
        self.results_direc = results.results_direc+"wdists/"
        self.distrange = distrange
        self.nbins = nbins
        self.F = None
        self.R = None
        self.edist = self.results.edist
        
        # selects reservoir work distributions
        if self.respair[0] == self.respair[1]:
            self.Ffile = self.results_direc + "wFres."+str(self.respair[0])
            self.Rfile = self.results_direc + "wRres."+str(self.respair[0])

        # selects PT work distributions
        else:
            self.Ffile = self.results_direc + "wFdist."+str(self.respair[0])
            self.Rfile = self.results_direc + "wRdist."+str(self.respair[0])
        
        # E distribution
        if self.edist:
            self.Efile = self.results_direc + "Edist."+str(self.respair[0])
            self.edistrange = np.arange(-400.,-377.,10)

        # make new work directory if needed
        if os.path.exists(self.results_direc) == False:
            os.makedirs(self.results_direc)
        
        E = Extractor(self,recompute=self.results.recompute)
        E.get_results()
            

    def read_from_files(self):
        
        self.F = np.loadtxt(self.Ffile)
        self.R = np.loadtxt(self.Rfile)
        
        if self.edist:
            self.E = np.loadtxt(self.Efile)

        
    def calculate_results(self):
        
        f = self.results.direc + self.results.outfile+str(self.respair[0]) 
        #print f
        
        #search for reservoir/ PT results
        if self.respair[0] == self.respair[1]:
            search_string="reservoir> hsa"
            self.distrange = (-30.,30.)
        else:
            search_string ="tryexchange> work"    
        
        op = OutputParser(f,search_string=search_string,cols=[2,3])
        
        wF,wR = op.get_data()
 
        self.F = np.histogram(wF,normed=True,range=self.distrange,bins=self.nbins)
        self.R = np.histogram(wR,normed=True,range=self.distrange,bins=self.nbins)

        # save pickle objects to files in local simulation directory
        np.savetxt(self.Ffile,zip(self.F[0],self.F[1]))
        np.savetxt(self.Rfile,zip(self.R[0],self.R[1]))

        # calculate edists:
        if self.edist:
            self.calculate_edist(wF,wR)
        
      
    def calculate_edist(self,wF,wR):
          
            Tlist = self.results.Tlist
            r = self.respair[0]
            
            if r<=self.results.Nrep:
                dBeta = 1./Tlist[r]-1./Tlist[r-1]
                print dBeta
                E = wF / dBeta
                print E
                
                self.E = np.histogram(E,bins=100,normed=True,range=[self.edistrange[0],self.edistrange[-1]])
                np.savetxt(self.Efile,zip(self.E[0],self.E[1]))      

            
            # get final Edist from reverse work values of ultimate replica
            #if r==self.results.Nrep:
            #    dBeta = 1./Tlist[r-2]-1./Tlist[r-1]
            #    E = wR / dBeta

            #    self.E = np.histogram(E,bins=100,normed=True,range=[self.edistrange[0],self.edistrange[-1]])
            #    np.savetxt(self.Efile,zip(self.E[0],self.E[1]))      


class Reservoir():
    
    def __init__(self,results,resnum):
        
        self.results = results
        
        self.results_direc = self.results.results_direc
        self.resnum = resnum
        self.T = self.results.Tlist[resnum-1]
        
        self.Pacc = None
        self.wdist = Wdistribution(self.results,(self.resnum,self.resnum),distrange=None,nbins=100)
       
        self.Pwell = None
        E = Extractor(self,recompute=self.results.recompute)
        E.get_results()
         
    def __str__(self):
        
        string = " \n"
        string += "# Reservoir Details:\n"
        string += "# Tres: "+str(self.T) +"\n"
        string += "# Pacc: "+str(self.Pacc)+"\n"
        
        return string
    
    def read_from_files(self):
        
        self.Pacc = np.loadtxt(self.results.results_direc+"PaccvRes")
        self.Pwell = np.loadtxt(self.results.results_direc+"Pwell")
      
    def calculate_results(self):
        
        op = OutputParser(self.results.direc+self.results.outfile+str(self.resnum),search_string="reservoir> hsa",cols=[5,7])
        accvals,wells = op.get_data()
        
        self.Pacc = np.average(accvals)
        
        self.Pwell  = np.histogram(wells, bins=np.arange(-0.5,99.5,1.0), normed=True)
        # save copy in local directory
        np.savetxt(self.results_direc+"PaccvRes",[self.Pacc])  
        np.savetxt(self.results_direc+"Pwell",zip(self.Pwell[0],self.Pwell[1]))  

    def plot_results(self):
        
        pass
        #plt.plot(zip(self.Pwell[1],self.Pwell[0]))
        #plt.savefig(self.results_direc+"Pwell.png")
        #plt.close()
        

class Thermo():
    
    def __init__(self,results,bash=False):
        
        self.Cvlist = None        
        self.Pacc = None
        self.results = results
        self.thermbash = self.results.thermbash
                
        self.S = None
        
        E = Extractor(self,recompute=self.results.recompute)
        E.get_results()
        
    def __str__(self):
        
        string = ""
        for i,p in enumerate(self.Pacc):
            string += str(self.results.Tlist[i]) +" "+str(p)+"\n"
    
        return string
    
    def read_from_files(self):
        
        dat = np.loadtxt(self.results.results_direc+"Cv",unpack=True)
        self.Tlist = dat[0]
        self.Cvlist = dat[1]
        
        try: 
            dat = np.loadtxt(self.results.results_direc+"PaccvT",unpack=True)
            self.Pacc = dat[1]
         
            dat = np.loadtxt(self.results.results_direc+"S",unpack=True)
            self.S = dat   
    
        except:
            print "files PaccvT, S could not be read"
            
    def calculate_results_bash(self):
        
        #self.Tlist = []
        self.Cvlist = []
        self.Pacc = []
         
        for i in range(1,self.results.Nrep):
            os.system("grep 'VOLD,' " + self.results.direc+ self.results.outfile+str(i)+" > " + self.results.direc+"tmp")
            E = np.genfromtxt(self.results.direc+"tmp",usecols=[2])
            #op = OutputParser(self.results.direc+"output."+str(i),search_string="tryexchange> work",cols=[2,5])
            #wF,P = op.get_data()
            #E = PT_work_to_E(wF,self.results.Tlist[i-1],self.results.Tlist[i])
            Cv = np.var(E) / (self.results.Tlist[i-1]**2)
            print Cv
            
            print i
            #aveP = np.average(P)

            self.Cvlist.append(Cv)
            #self.Pacc.append(aveP)

        # calculate replica entropy
        if self.results.conv:
            dat = rlx_analysis.rlx(self.results.direc+"exchanges.permutations",self.results.Nrep,0.1)
            self.S = dat
            np.savetxt(self.results.results_direc+"S",zip(self.S[0],self.S[1]))        

        # save copy in local directory
        np.savetxt(self.results.results_direc+"Cv",zip(self.results.Tlist,self.Cvlist))
        #np.savetxt(self.results.results_direc+"PaccvT",zip(self.results.Tlist,self.Pacc))        
        #np.savetxt(self.results.results_direc+"S",zip(self.S[0],self.S[1]))        
            
    def calculate_results(self):
        
        #self.Tlist = []
        self.Cvlist = []
        self.Pacc = []
         
        for i in range(1,self.results.Nrep):
            op = OutputParser(self.results.direc+self.results.outfile+str(i),search_string="tryexchange> work",cols=[2,5])
            wF,P = op.get_data() 
            E = PT_work_to_E(wF,self.results.Tlist[i-1],self.results.Tlist[i])
            Cv = np.var(E) / (self.results.Tlist[i-1]**2)
            aveP = np.average(P)

            self.Cvlist.append(Cv)
            self.Pacc.append(aveP)

        # calculate replica entropy
        dat = rlx_analysis.rlx(self.results.direc+"exchanges.permutations",self.results.Nrep,0.1)
        self.S = dat
        np.savetxt(self.results.results_direc+"S",zip(self.S[0],self.S[1]))        

        # save copy in local directory
        np.savetxt(self.results.results_direc+"Cv",zip(self.results.Tlist,self.Cvlist))
        np.savetxt(self.results.results_direc+"PaccvT",zip(self.results.Tlist,self.Pacc))        
        #np.savetxt(self.results.results_direc+"S",zip(self.S[0],self.S[1]))        

    def plot_results(self):
        
        plt.plot(self.results.Tlist[:-1],self.Cvlist,'-o')
        plt.savefig(self.results.results_direc+"Cv.png")
        plt.close()

        plt.plot(self.results.Tlist[:-1],self.Pacc)
        plt.savefig(self.results.results_direc+"PaccvT.png")        
        plt.close()

class Results():
    
    def __init__(self,direc,Nrep,recompute=False,reservoir=None,wdist=False,maxlines=None,conv=True,edist=False,thermbash=False,outfile=None):
        
        self.direc = direc
        self.results_direc = direc+"results/"

        self.outfile = "output."
        if outfile != None:
            self.outfile = outfile
        
        #self.results_direc = "/home/ab2111/sinister.tmp/"+"results/"
        self.Nrep = Nrep
        self.Tlist = np.loadtxt(self.direc+"temperatures")[:Nrep]
        self.reservoir = None
        self.conv = conv
        self.simlength = None
        self.maxlines = maxlines
        self.recompute = recompute
        
        self.recompute = recompute
        self.wdists = []
        self.maxlines = maxlines
        
        self.edist = edist
              
        self.thermbash=thermbash
          
        # make results directory if not already there
        if os.path.exists(self.results_direc) == False:
            os.makedirs(self.results_direc)
        
        self.therm = Thermo(self)

        if reservoir!=None: 
            #resT = self.Tlist[int(reservoir)-1]
            self.reservoir = Reservoir(self,reservoir)        
        
        if wdist==True:
            
            self.gather_wdistributions()
                       
            
    def gather_wdistributions(self):
        
        self.wdists = []
                
        for r in range(1,self.Nrep):
            dist = Wdistribution(self,(r,r+1),distrange=None,nbins=100)
            self.wdists.append(dist)
                                                   
            
    def plot_results(self):
        
        items = [self.therm,self.reservoir]
        
        for a in items: 
            if a: 
                #a.calculate_results()
                a.plot_results()
                
        #for w in self.wdists: w.calculate_results()
        
    def __str__(self):
        
        string = "\n\n"
        string += "Simulation Details:\n"
        string += "wd: "+self.direc +"\n"
        string += "Nreplicas: "+str(self.Nrep) +"\n"
        string += "\n\n"
        
        string += "Simulation Results:\n"
        string += "T vs Pacc:\n"
        
        string += self.therm.__str__()
        string += self.reservoir.__str__()
        
        
        return string
        
        

def main():        
    """
    from hsa_utils.utils import ODM
    # simulation details:
    dir="/scratch/ab2111/plutoscratch/projects/kirkwood/TTodm/"
    RREXdir = dir+"RREX_ener_T_20_50_100_200_kJmol/RREX_ener_T_20_50_100_200_kJmol_thinned.dat"
    #RREXdir = dir+"RREX_ener_T_20_50_100_200_kJmol/short"
    TREXdir = dir+"TREX_ener_T_20_50_100_200_kJmol/TREX_ener_T_20_50_100_200_kJmol_thinned.dat"
    dirs = [RREXdir,TREXdir]
    titles = ["TREX","RREX"]
    Nreplicas=4

    # Temperature list in Kelvin  and kJ/mol
    Tlist = np.array([20.,50.,100.,200.])
    R=0.008314
    kTlist = Tlist * R
    import time
    t = time.time()
    
    for r in range(Nreplicas-1):

        for i,d in enumerate(dirs):
            E0,E1 = get_workvals_from_file(d,cols=(r,r+1))

            # convert energies into work values
            dBeta = 1./kTlist[r+1]-1./kTlist[r]
            wFdat = np.array(E0) * dBeta
            wRdat = np.array(E1) * (-dBeta)

            #print E0,E1,dBeta    
            odm = ODM(wFdat,wRdat)
    
            plt.plot(odm.deltaL[0],odm.deltaL[1],'-x')
    
    print "time", time.time()-t
    plt.show()        
    exit()
    """
    
    #puma results %%%%%%%%%%%%%%%%%%%%%%%%%%%
    #f="/scratch/ab2111/tosinister/puma/randomMC/biased_gaussian_sampling/pt/sidechain_new/lnJfix/restart/"
    #results = Results(f,20,thermbash=True,recompute=True)
    #exit()
    #puma results %%%%%%%%%%%%%%%%%%%%%%%%%%%
    #direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/"
    direc="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/"
    #direclist = [direc +"hsaest/res/",direc+"geom/res/",direc+"hsaest/M20res/",direc+"geom/M20res/"]
    direclist = [direc+"hsaest_at_transition/print/",direc+"geom_at_transition/print/"]
    #direclist = [direc+"hsaest_from_gmin_mindata/M20res/",direc+"geom_from_gmin_mindata/M20res/"]
    Nreplist=[12,12]
    reslist=[6,6]
    #Nreplist=[16,16,20,20]
    #Nreplist=[16,16]
    #Nreplist=[20,20]
    #Nrep =16
    #reslist = [13,13,13,13]
    #reslist = [13,13]
    #direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest/nores/"
    #direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/optimize_Tres/M2/"
    #direclist = [direc+"T.070/",direc+"T.075/",direc+"T.080/",direc+"T.085/"]
    #reslist = [1,1,1,1]
    #direc="/Users/ajb/projects/testing/sample_of_LJ75_data/"
    #Nrep =2
    
    #wdist = Wdistribution(direclist[0],(1,2),recompute=True,distrange=None,nbins=100)
    #direclist = direclist[:2]
    
    plt.figure(1)
    
    for i,direc in enumerate(direclist):
        results = Results(direc,Nreplist[i],outfile="te.",recompute=True,reservoir=reslist[i],wdist=True)
        #results.plot_results()

        plt.subplot(211)
        plt.plot(np.arange(len(results.therm.Pacc)),results.therm.Pacc,'-o')        
        plt.subplot(212)
        plt.plot(results.therm.S[0],results.therm.S[1])
        #plt.plot(results.Tlist[:-1],results.therm.Cvlist,'-x')        
        print "reservoir acceptance: ",results.reservoir.Pacc
        #plt.plot(np.log(results.reservoir.Pwell[:,0]),'-o')        
        #plt.plot(results.Tlist[:-1],'-o')        
        
        #f = results.direc+"output.1"
        """
        from hsa_utils.utils import ODM
        f="/scratch/ab2111/plutoscratch/projects/kirkwood/TTodm/RREX_ener_T_20_50_100_200_kJmol/RREX_ener_T_20_50_100_200_kJmol.dat"
        Tlist = np.array([20.,50.,100.,200.])
        R=0.008314
        kTlist = Tlist * R
        wFdat, wRdat = get_workvals_from_file(f,cols=(0,1))
        wFdat = wFdat / kTlist[0]
        wRdat = wRdat / kTlist[0]
        odm = ODM(wFdat,wRdat)

        print odm.LF
        plt.plot(odm.LF[0],odm.LF[1])
        #plt.plot(odm.deltaL[0],odm.deltaL[1])
    
        plt.show()
        exit()
        """
        #dist = results.wdists[1]
    """
    HSAcv = []
    from testing.landscape import MinimaDistribution
    mdist = MinimaDistribution(results.Tlist[0],kappa=0.5*(3*75-6),minfile="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest/nores/min.data",sort=False,gas_constant=False)
    #for t in np.arange(results.Tlist[0],results.Tlist[-1],0.001):
    for t in np.arange(results.Tlist[0],0.2,0.001):
        mdist.T = t
        HSAcv.append(mdist.Cv)
        #print t,mdist.Cv
    """
    plt.show()
    exit()
    #files = ["/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest/nores/min.data","/scratch/ab2111/plutoscratch/projects/testing/input_files/min.data.lj75"]
    direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/"

    files = [direc+"hsaest_from_gmin_mindata/res/min.data"]
    #for f in files:
    # plot HSA well probabilities for T = Tres:
        #mdist = MinimaDistribution(results.Tlist[reslist[0]-1],kappa=0.5*(3*75-6),minfile=f,sort=True,gas_constant=False)
        #for i,p in enumerate(mdist.Pwell):
        #    print i,p
        #plt.plot(np.arange(1,len(mdist.Pwell)+1,1.),np.log(mdist.Pwell),'-x')
        #plt.plot(np.arange(1,len(mdist.Pwell)+1,1.),np.log(sorted(mdist.Pwell,reverse=True)),'-x')
        #plt.plot(np.arange(1,len(mdist.E)+1,1.),sorted(mdist.E),'-x')
    x = np.arange(0.0,5.0e7,1.0e6)
    #plt.subplot(212)
    plt.plot(x,[np.log(Nreplist[0]) for i in x],'--')
    plt.ylim(0.0,np.log(Nreplist[0])+0.1)
    #plt.plot(x,[np.log(14.) for i in x],'--')
    #plt.plot(x,[np.log(13.) for i in x],'--')
    #plt.plot(np.arange(results.Tlist[0],results.Tlist[-1],0.001),HSAcv)
    #plt.plot(np.arange(results.Tlist[0],0.2,0.001),HSAcv)
    #dist = results.reservoir.dist
    

    #plt.legend(["hsa-M16","geom-M16","hsa-M20","geom-M20"],loc=1)
    plt.legend(["hsa-from-gmin","geom-from-gmin","hsa"],loc=3)

    plt.show()


if __name__=="__main__":
    #main()
    pass
