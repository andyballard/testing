import numpy as np
import pylab as plt
#from scripts.Cv_est import Cv_est
#from landscape import MinimaDistribution
import re
import os

def parse_data(f,search_string,cols=None):

    data = []
    ret = []

    fp_in = open(f,"r")
    
    lines = fp_in.readlines()
    expr = re.compile(search_string)

    for i,l in enumerate(lines):
        match = expr.search(l)
        if match:
            data.append(l.split())
            #print i,l[2]
    
    for c in cols:
        #print c
        print np.shape(data)
        ret.append(data[:][c])
    
    print ret    
    return ret

class Results():
    
    def __init__(self,direc,recompute=False):
        
        self.direc = direc + "results/"
        self.recompute = recompute
        # make results directory if not already there
        if os.path.exists(self.direc) == False:
            os.makedirs(self.direc)
    
        if self.recompute==False:    
            try: 
                self.read_from_files()
            except:
                self.load_data()
                
    def read_from_files(self):

        self.Workdist = np.loadtxt(self.results.results_direc+"work.hst")

        dat = np.loadtxt(self.direc+"Pacc")
        self.Pacc = dat[0]
         
        self.Deltadist = np.loadtxt(self.direc+"Delta.hst")
        
        self.Stepdist = np.loadtxt(self.direc+"Step.hst")
        

    
    def load_data(self):

        f = self.direc + "../output.2.short" 
        #print f
        
        #search_string="dE/T+dweight"
        search_string="dweight"
                
        delta,step,work = parse_data(f,search_string,cols=[3,7,13])

        self.Deltadist = np.histogram(delta,normed=True,range=[0.,1.],bins=20)
        self.Stepdist = np.histogram(step,normed=True,range=[0.,1.],bins=20)
        self.Workdist = np.histogram(work,normed=True,range=[-2.,10.],bins=20)

        # save objects to files in local simulation directory
        np.savetxt(self.Dfile,zip(self.Deltadist[0],self.Deltadist[1]))
        np.savetxt(self.Sfile,zip(self.Stepdist[0],self.Stepdist[1]))
        np.savetxt(self.Wfile,zip(self.Workdist[0],self.Workdist[1]))

    
def main():
    f="/scratch/ab2111/tosinister/puma/randomMC/biased_gaussian_sampling/test_G_helix/a300b10/"
    
    results = Results(f)
    #results = Results(f,20,recompute=True,reservoir=None,wdist=True)

if "__name__==main()":
    main()
    