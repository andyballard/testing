import numpy as np
import sys
import math

def calcEntropy(p):
    S = 0.0
    for i in range(len(p)):
        if p[i] != 0.0: S += - p[i] * math.log(p[i])

    return S

def get_rt_time(f,nrep,freq):
    
    T,R0,RM = np.loadtxt(f, usecols=[0,1,-1],unpack=True)
    
    skip = int(1./freq)
    #fp_in=open(f,"r")
    #lines = fp_in.readlines()
    
    times = []
    
    for l,r0 in enumerate(R0[::skip]):     
    #for l, line in enumerate(lines):
         
        t0 = T[l*skip]#float(line.split()[0])
        #r0 = int(line.split()[1])
        
        print t0,r0
        
        for line2 in lines[l:]:
            t = float(line2.split()[0])
            rM = int(line2.split()[nrep])

            if rM == r0:
                time = t-t0
                times.append(time)
                print time
                break
        
        
    tau = np.average(times)           
    stdev = np.sqrt(np.var(times)) 
    
    return tau

def rlx(f,nrep,freq):

    fp_in=open(f,"r")
#     fp_in=open("exchanges.permutations","r")

    t = 0.0
    tprev = 0.0
    dt = 0.0

    p = np.zeros(nrep)
    f = np.zeros(nrep)
    Tau = 0.0
#     Tauf = np.zeros(nrep)
    Tauf = np.zeros(shape=(nrep,nrep))
    State = [i for i in range(nrep)]

    #print "# time <S> sigma_S"
    i=1
    l=0

    tlist = []
    slist = []
    slist0 = []

    for l, line in enumerate(fp_in): 
        if l==0:
            tprev = float(line.split()[0])    

#     for line in lines:
        if(l % int(1./freq)==0 and l!=0):
            t = float(line.split()[0])
            dt = t-tprev

            Tau = Tau + dt
            entropyave = 0.0
            fluct = 0.0
            for n in range(nrep):
                State[n] = int(line.split()[n+1])
                Tauf[n,State[n]] += dt
                #p = Tauf[n,:] / t
                p = Tauf[n,:] / Tau
                S = calcEntropy(p)
                fluct = fluct + S*S
                entropyave = entropyave + S
            slist0.append(S)
            entropyave = entropyave / nrep
            fluct = math.sqrt(fluct/nrep - entropyave*entropyave)
            tlist.append(t)
            slist.append(entropyave)
            #print t,entropyave,fluct
            tprev = t
            
    return tlist,slist,slist0


def main():

    import matplotlib.pyplot as plt
    
    #f = "/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest_from_gmin_mindata/res/exchanges.permutations"
    #t,s,s0 = rlx(f,16,0.01)
    
    f1 = "/scratch/ab2111/plutoscratch/projects/kirkwood/Scalc/exchanges.perm2"
    f2 = "/scratch/ab2111/plutoscratch/projects/kirkwood/Scalc/ex.permall"
    files = [f1,f2]

    t,s,s0 = rlx(f,4,0.01)
    
    plt.plot(t,s0)

    plt.show()
        
if "__name__==main()":
    #main()
    pass