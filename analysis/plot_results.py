import ParallelTempering
import sys
import matplotlib.pyplot as plt
import numpy as np

try:
    dir = "./"
    Nrep = int(sys.argv[1])
    reservoir = int(sys.argv[2])
except:
    print "Usage:\n python analyse.py kappa Nrep reservoir"
    exit()
    
results = ParallelTempering.Results(dir,Nrep,recompute=False,reservoir=reservoir,wdist=True)
        
plt.subplot(211)
plt.plot(np.arange(len(results.therm.Pacc)),results.therm.Pacc,'-o')        
plt.title(r"$P_{\rm acc}$")
plt.subplot(212)
plt.plot(results.therm.S[0],results.therm.S[1])
d = np.arange(0.,results.therm.S[0][-1],(results.therm.S[0][-1])/100)
plt.plot(d,[np.log(Nrep) for x in range(len(d))])
plt.title(r"Replica Entropy vs time")
#plt.plot(results.Tlist[:-1],results.therm.Cvlist,'-x')        
plt.show()