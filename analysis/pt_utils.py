import numpy as np
import re
import matplotlib.pyplot as plt

def PT_work_to_E(work,T1,T2):
    """ convert work values of PT exchange to absolute energies:
        here w = dBeta * E
        where dBeta = 1./(kB T_2) - 1./(kB T_1)
    """
    #E = np.zeros(len(work))
    dBeta = 1./T2 - 1./T1
    
    return work / dBeta
    #for i,w in enumerate(work):
    #    E[i] = w / dBeta
    
    #return E


def get_workvals_from_file(f,cols=(2,3)):

    #data = parse_output(f,None)
    data = parse_output_SS(f)
    wFdat = np.array([])
    wRdat = np.array([])
    
    for l in data:
        wFdat = np.append(wFdat,float(l.split()[cols[0]]))
        wRdat = np.append(wRdat,float(l.split()[cols[1]]))
        
        #if i%1000==0: print i
        #wFdat.append(float(l.split()[cols[0]]))
        #wRdat.append(float(l.split()[cols[1]]))
   
    return wFdat,wRdat

def parse_output_SS(f):

    fp_in = open(f,"r")
    lines = fp_in.readlines()
    
    return lines
    
def parse_output(f,maxlines,res=False):
    
    import time
    
    #t = time.time()
    
    # selects ordinary PT exchange or reservoir exchange
    if res == False:
        expr = re.compile(r'tryexchange> work')
    else:
        expr = re.compile(r'reservoir> hsa')
        
    fp_in = open(f,"r")
    work =[]
    lines = fp_in.readlines()

    # skip over lines if the file is too large
    skip=1
    if maxlines != None:
        skip = int(len(lines)/maxlines)
    
    for i in range(0,len(lines),skip):
    #for d in lines:
        match = expr.search(lines[i])
        if match:
            # search for merged columns
            work.append(re.sub(r'E-(?P<n>\d{2})-','E-\g<n> -',lines[i]))
            #work.append(d)
    
    #print "time elapsed: ",time.time()-t

    return work

def process_PT_resrun(direc,resnum,maxlines):
    
    f = direc+"output."+str(resnum)
    
    simdat = parse_output(f,maxlines,res=True)
    
    dat =  np.loadtxt(simdat,usecols=[2,5])
    pacc = dat[:,1]
    Pacc = np.average(pacc)
        
    return Pacc    

def process_PT_run(direc,rep,maxlines):
    
    Tlist = np.loadtxt(direc+"temperatures")
    T = Tlist[rep-1]

    f = direc + "output."+str(rep)

    print f
    simdat = parse_output(f,maxlines)
    #dat =  np.loadtxt(simdat,usecols=[2,5])
    work = []
    pacc = []
    for line in simdat:
        work.append(float(line.split()[2]))
        pacc.append(float(line.split()[5]))
    #work = dat[:,0]
    #pacc = dat[:,1]
    E = PT_work_to_E(work,Tlist[rep-1],Tlist[rep])
    #Cv.append(calc_cv(E,Tlist[i]))
    Cv = np.var(E) / (Tlist[rep-1]**2)
    Pacc = np.average(pacc)

    # save local copy of results
    #np.savetxt(direc+"Cv",zip(Tlist,Cv))
    #np.savetxt(direc+"PaccvT",zip(Tlist,Pacc))
    
    return T,Cv,Pacc

def plot_PT_run(direc):
    
    Cvdat = np.loadtxt(direc+"Cv")
    
    plt.plot(Cvdat[:,0],Cvdat[:,1])    

                 
class OutputParser():
    
    def __init__(self,file,search_string=None,cols=None,maxlines=None):
        
        self.file = file
        self.search_string = search_string
        self.cols = cols
        self.maxlines = maxlines

        self.plines =[]
        
        self.parse()
        #self.get_data()
        
    def parse(self):
        
        fp_in = open(self.file,"r")
        self.plines =[]
        lines = fp_in.readlines()

        
        if self.search_string==None:
            self.plines = lines
        
        else:    
            expr = re.compile(self.search_string)
        

            # skip over lines if the file is too large
            skip=1
            if self.maxlines != None:
                skip = int(len(lines)/self.maxlines)
    
            for i in range(0,len(lines),skip):
                if self.search_string != None:
                    match = expr.search(lines[i])
                    if match:
                        # search for merged columns
                        try:
                            map(float,lines[i].split()[2:])
                            self.plines.append(lines[i])
                        except:                        
                            sub = re.sub(r'E-(?P<n>\d{2})-','E-\g<n> -',lines[i])
                            sub = re.sub(r'(?P<one>\d{6,})-(?P<two>\d{2,})','\g<one>E-\g<two>',sub)
                            sub = re.sub(r'(?P<one>\d{2,})-(?P<two>\d{2,})','\g<one> -\g<two>',sub)
            
                            #self.plines.append(re.sub(r'E-(?P<n>\d{2})-','E-\g<n> -',lines[i]))
                            #self.plines.append(re.sub(r'(?P<one>\d{6,})-(?P<two>\d{2,})','\g<one>E-\g<two>',lines[i]))
                            print "fixed improperly-formatted line:\n",lines[i]
                            try:
                                map(float,sub.split()[2:])
                                self.plines.append(sub)
                            except:
                                print "ignoring impropoerly-formatted line:\n",sub

                        #work.append(d)
         
        #print self.plines   

    def get_data(self):
        
        coldata = [[]*len(self.cols)]
        coldata = [[] for i in range(len(self.cols))]
        #coldata = [np.array([]) for i in range(len(self.cols))]
        
        for l in self.plines:
            #lcols = tuple(l.split())
            lcols = l.split()
            for i,c in enumerate(self.cols):
                #print coldata[i]
                #coldata[i] = np.append(coldata[i],[float(lcols[c])])
                coldata[i].append(float(lcols[c]))
                
        #print coldata[0]
        
        return [np.array(coldata[i]) for i in range(len(coldata))]

class Extractor():
    
    def __init__(self,obj,recompute=False):
        self.obj = obj
        self.recompute = recompute
        
    def get_results(self):
        
        if self.recompute:
            #if self.obj.thermbash == True:
            #    self.obj.calculate_results_bash()
            #else:
            #    self.obj.calculate_results()
            self.obj.calculate_results()
        else:
            try:
                self.obj.read_from_files()
            except:
                print "can't read from files, calculating results instead"
                #print self.obj.respair
                #if self.obj.thermbash == True:
                #    self.obj.calculate_results_bash()
                #else:
                #    self.obj.calculate_results()
                self.obj.calculate_results()
