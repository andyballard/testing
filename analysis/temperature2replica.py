import numpy as np
from testing.analysis.ParallelTempering import get_workvals_from_file,parse_output
import matplotlib.pyplot as plt

class Base(object):
    
    def __init__(self,direc,*args,**kwargs): 
        self.direc = direc
        super(Base,self).__init__(*args,**kwargs)

class Mapping(Base):
    
    def __init__(self,*args,**kwargs):

        super(Mapping,self).__init__(*args,**kwargs)       
        
        #dat = np.loadtxt(self.direc + "ex.thinned")
        dat = np.loadtxt(self.direc + "exchanges.permutations")
        self.time_series = dat[:,0]
        #self.temperature_time_series = np.reshape(dat[:,1:], ())
        self.temperature_time_series = dat[:,1:].T
        #print self.time_series
        print self.temperature_time_series
        #exit()
        self.T_list = []
        #plt.plot(self.time_series,self.temperature_time_series[2])
        #plt.show()
        
        # T_list[i] is trajectory Temperature(t|replica=i)
        for r in range(len(dat[:,1:])):
            self.T_list.append(np.nonzero(self.temperature_time_series.T==r)[1])
        
    def map_to_replicas(self,time,observable_list,T_index,r_index):
        """ Map observable list (where each column corresponds to a temperature,
                to an observable list (where each column corresponds to a replica)
        """
        
        obs_mapped = np.array([])
        t_mapped = np.array([])
 
        mapped_obj_list = np.array([])
        mapped_obj_time_list = np.array([])       
        
        """"map time series to coincide with mapping series -- data points can be lost because of this"""
        for i,t in enumerate(time):
            for j,t_me in enumerate(self.time_series):
                if t >= t_me:
                    if t<= self.time_series[j+1]:
                        t_mapped = np.append(t_mapped,t_me)
                        obs_mapped = np.append(obs_mapped,observable_list[i])
        
                        #if self.temperature_time_series[T_index][i] == self.T_list[r_index][j]:
                        #    mapped_obj_list = np.append(mapped_obj_list,obs_mapped[i])
                        #    mapped_obj_time_list = np.append(mapped_obj_time_list,t_mapped[i])
        
                        break

        for i,t in enumerate(t_mapped):
            if t_me in self.time_series:
                mapped_obj_list = np.append(mapped_obj_list,obs_mapped[i])
                mapped_obj_time_list = np.append(mapped_obj_time_list,t_mapped[i])
                        

        #print np.shape(t_mapped),np.shape(self.time_series)
        #self.T_list = np.nonzero(self.temperature_time_series.T==r_index)[1]
        #print self.T_list
        #plt.plot(self.time_series,self.T_list,'-x')
        #plt.show()

        """
        print self.temperature_time_series[T_index]
        for i,r in enumerate(self.temperature_time_series[T_index]):
            if self.time_series[i]==t_mapped[i]:
                if r == self.T_list[r_index][i]:
                    mapped_obj_list = np.append(mapped_obj_list,obs_mapped[i])
                    mapped_obj_time_list = np.append(mapped_obj_time_list,t_mapped[i])
        """
        return mapped_obj_time_list, mapped_obj_list

class TemperatureData(Base):
    
    def __init__(self,Nrep,nres,*args,**kwargs):
 
        
        super(TemperatureData,self).__init__(*args,**kwargs)       
        #self.file = file
        
        self.nres = nres
        self.Nrep = Nrep
        self.file = file

        res_data = parse_output(self.direc+"output."+str(self.nres),None,res=True)

        self.Emin = []
        self.reswell = np.array([])
        timelist = np.array([])

        self.data_objects = []
        
        for r in res_data:
            self.reswell = np.append(self.reswell,int(r.split()[7]))
            timelist = np.append(timelist,float(r.split()[8]))
            self.data_objects.append([timelist,self.reswell])
        #for i in range(Nrep): 
        #    dat = get_workvals_from_file(self.f)
        
        #self.load_emin_data()
                        
    def load_emin_data(self):
        
        for i in range(self.Nrep):
            time,e = np.loadtxt(self.direc+"vmin"+str(i+1),usecols=(2,3),unpack=True)
            #print e
            self.Emin.append(e)
            self.data_objects.append([time,e])

    
class ReplicaData(Mapping,TemperatureData):
    
    def __init__(self,*args,**kwargs):
        
        #print(ReplicaData.__mro__)
        super(ReplicaData,self).__init__(*args,**kwargs)
        
        self.data_mapped_objects = []
        r_index=0
        T_index=2
        i=0
        for dummy,d2 in enumerate(self.data_objects):
 
            t=d2[1]
            d=d2[0]
            #print t, "t"
            #print d, "d"
            #print "length(t)",len(t)
            mapped_o = self.map_to_replicas(t,d,i,r_index)
            self.data_mapped_objects.append(mapped_o)
            i=i+1
            plt.plot(mapped_o[0],mapped_o[1],'x')

        plt.show()
        print self.data_mapped_objects

def main():
    
    #f = "/scratch/ab2111/plutoscratch/projects/LJ75/testing_all_in_14/output.3"
    #direc= "/scratch/ab2111/plutoscratch/projects/LJ75/testing_all_in_1/"
    direc="/scratch/ab2111/tosinister/puma/randomMC/biased_gaussian_sampling/pt/"
    #mappingfile = "/scratch/ab2111/plutoscratch/projects/LJ75/testing_all_in_14/ex.thinned"
    Nrep=6
    nres=3
    temp_data = ReplicaData(Nrep,nres,direc)
    
    objects = temp_data.data_mapped_objects[0]
    t = objects[0]
    well = objects[1]
    #plt.plot(temp_data.data_timelist,temp_data.data_objects[0],'x',color="blue")
    #plt.plot(t,well,'x',color="green")
    #plt.show()
    
if __name__ =="__main__":
    main()
    