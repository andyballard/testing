import numpy as np
import matplotlib.pyplot as plt

def state_trajectory(datr,R=0):

    out = np.nonzero(datr == R)

    T = out[1]

    return T

def main():
    
    #f="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/hsaest_at_transition/print/exchanges.permutations"
    #f="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/geom_at_transition/print/exchanges.permutations"
    f="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/geom_from_gmin_mindata/M12_res_at_transition/restart/exchanges.permutations"
    #f="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest_from_gmin_mindata/M12_res_at_transition/restart/exchanges.permutations"
    #analysis_dir="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/analysis_at_transition/"
    #analysis_dir="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/analysis_at_transition/"
    #R = 1
    Tpt = 6
    #traj = state_trajectory(f,R=R)
    
    #binary = map(int,traj < Tpt)
    #np.savetxt("R"+str(R)+".traj.bin",binary)
    #exit()
    
    dat = np.genfromtxt(f)

    datr = dat[:,1:]
   
    #for R in range(1):
    for R in range(len(datr[0,:])):
        print R
        traj = state_trajectory(datr,R=R)
        #binary = map(int,traj < Tpt)
        #np.savetxt("R"+str(R)+".traj.bin",binary)
        
        plt.plot(traj)
        plt.xlabel("time / ")
        plt.ylabel("Temperature index")
        plt.show()
        #np.savetxt(analysis_dir+"geomR"+str(R)+".traj",traj)
    
if __name__=="__main__":
    #pass
    main()