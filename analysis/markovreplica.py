import numpy as np
import matplotlib.pyplot as plt

M=12

dir = "/scratch/ab2111/tosinister/clusters/LJ31/tspacing/"
#dir = "/scratch/ab2111/tosinister/clusters/LJ75/tspacing/"
files = [dir+"geom_at_transition/print/results/PaccvT",dir+"hsaest_at_transition/print/results/PaccvT"]
#files = [dir+"geom_from_gmin_mindata/M12_res_at_transition/restart/results/PaccvT",dir+"hsaest_from_gmin_mindata/M12_res_at_transition/restart/results/PaccvT"]
paccs = []
taus = []
f = files[0]
#for j,f in enumerate(files):
for j in range(10):

    Pacc = np.loadtxt(f,usecols=(1,))

    Pacc[4] = 0.2*(1.-j/10.)
    #Pacc[5] = 0.01
    paccs.append(Pacc[4])
    
    #plt.plot(Pacc)
    T = np.zeros((M,M))
    for i in range(M-1):
        T[i,i+1] = Pacc[i]
        T[i+1,i] = T[i,i+1]
        #print Pacc[i]
            
    for i in range(M):
        T[i,i] = 1.-np.sum(T[i,:])
        
    lam = np.linalg.eigvalsh(T)
    print lam
    print "tau ~",1./(1.-lam[-2])
    taus.append(1./(1.-lam[-2]))
    #plt.plot(lam,'-x')
    
plt.plot(paccs,taus,'x-')
#plt.yscale('log')
plt.legend(["geom","hsaest"],loc=2)
plt.show()