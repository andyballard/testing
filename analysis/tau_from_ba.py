import numpy as np
import sys
    

def load_taus(fp,simtype):
    
    if simtype=="geom": f = "geom"
    
    dat = np.loadtxt(fp+"conv."+simtype,unpack=True)

    return dat

def get_taus(fp,simtype):

    kaves = []
    sigs = []
    taus = []

    M=12
    sigk=np.sqrt((M+1)*(M-1)/12)

    if simtype =="geom":
        geom=True
        blocks = np.loadtxt(fp+"geom_block")
        f="geom"
        #LJ31
        #N=1135709
        #LJ75
        N=19660
    else:
        geom=False
        blocks = np.loadtxt(fp+"hsa_block")
        f=""
        #LJ31
        #N=1134908
        #LJ75
        N=18953


    for l,b in enumerate(blocks):

        #traj=np.loadtxt(fp+"../"+f+"R"+str(l)+".traj")
        #N = len(traj)
        
        sig=np.loadtxt(fp+f+"R"+str(l)+".traj.er",usecols=(1,))
        sig=sig[b]

        tau = 0.5*N*(sig/sigk)**2
        print "file: ",f+"R"+str(l)+".traj"
        print "trajectory: ",l
        print "largest block: ",b
        print "sigma: ",sig
        print "sigk", sigk
        print "N", N
        print "tau: ",tau
        print "\n"
        
        taus.append(tau)
        sigs.append(sig)
        
    for m in range(M):
        kave = np.average(np.loadtxt(fp+"../"+f+"R"+str(m)+".traj"))    
        kaves.append(kave)
        print f,m,kave,sigs[m]
    
    return kaves,sigs,taus

def main():
    
    import matplotlib.pyplot as plt
    import numpy as np
    
    kaves = []
    sigs = []
    taus = []
    
    simtypes = ["geom","hsa"]
    #direc="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/analysis_at_transition/avek/"
    direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/analysis_at_transition/avek/"
    
    for i in range(len(simtypes)):
        
        #
        kave,sig,tau = get_taus(direc,simtypes[i])
        #kave,sig,tau = load_taus(direc,simtypes[i])
        
        kaves.append(kave)
        sigs.append(sig)
        taus.append(tau)
        #plt.plot(kave)
        np.savetxt(direc+"conv."+simtypes[i],zip(kave,sig,tau))
        #np.savetxt(direc+"conv."+simtypes[i],zip(kaves,sigs,taus),header="kave\t sigma_kave\t tau")
        #plt.plot(kave)
        print simtypes[i],np.max(sig)
        
        plt.errorbar(range(len(kave)), kave, yerr=sig)
    
    #plt.plot(range(12),[5.5 for i in range(12)])
    #plt.legend(simtypes)
    #plt.show()
    print "average, stdev geom: ", np.average(taus[0]), np.std(taus[0])
    print "average, stdev HSA: ", np.average(taus[1]), np.std(taus[1])
    print "averages ratios:", np.average(taus[1])/np.average(taus[0])
    #print "max ratios:", np.max(taus[1])/np.max(taus[0])
    
    
if __name__=="__main__":
    
    main()