import ParallelTempering
import sys
import matplotlib.pyplot as plt
import numpy as np

#outfile="convergenceLJ75.png"
outfile="convergenceLJ31.png"
plt.rc('lines', linewidth=2)
# plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 25
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 20
plt.rcParams['legend.fancybox'] = True  
plt.rcParams['legend.fontsize'] = 14
plt.rcParams['axes.labelsize'] = 20 
plt.rcParams['xtick.labelsize'] = 20
plt.rcParams['ytick.labelsize'] = 20
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3
plt.rcParams['figure.autolayout'] = True
  
Nrep=12        
#direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/"
direc="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/"
dirlist = []
dirlist.append(direc+"geom_at_transition/print/results/S")
dirlist.append(direc+"hsaest_at_transition/print/results/S")
#dirlist.append(direc+"geom_from_gmin_mindata/M12_res_at_transition/restart/results/S")
#dirlist.append(direc+"hsaest_from_gmin_mindata/M12_res_at_transition/restart/results/S")
func = ['-','--']

for i,d in enumerate(dirlist):
    #results = ParallelTempering.Results(d,Nrep,recompute=False,reservoir=reservoir,wdist=True)
    #plt.plot(results.therm.S[0],results.therm.S[1],'-',ms=15, lw=2)
    S = np.loadtxt(d,unpack=True)
    #plt.plot(S[0]-70000000,(np.log(Nrep)-S[1])/np.log(Nrep),func[i])
    plt.plot(S[0],(np.log(Nrep)-S[1])/np.log(Nrep),func[i])
#d = np.arange(0.,S[0][-1]-70000000,(S[0][-1])/100)
#plt.plot(d,[np.log(Nrep) for x in range(len(d))],'--',ms=15, lw=2)
#plt.plot(d,[np.log(Nrep) for x in range(len(d))],'--',ms=15, lw=2)
plt.legend(["geometric","HSA-estimated","asymptotic limit"],loc=1)
#plt.ylabel("Replica Entropy")
plt.ylabel(r"$1-\frac{S}{S_{\rm eq}}$",fontsize=20)
plt.xlabel("MC iteration",fontsize=20)
#plt.ylim([0.,3.2])
plt.yscale('log')
#plt.title(r"LJ75: Replica Entropy vs time")
#plt.plot(results.Tlist[:-1],results.therm.Cvlist,'-x')        
#plt.show()
plt.savefig(outfile)