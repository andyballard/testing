import numpy as np
import matplotlib.pyplot as plt
from testing.analysis.ParallelTempering import get_workvals_from_file,parse_output

def get_energies(f,timelist):
    
    dat = parse_output(f,None,res=False)
    wF = []
    wR = []
    well = []
    t = []

    for l in dat:
        t = np.append(t,float(l.split()[8]))
        wF = np.append(wF,float(l.split()[2]))
        wR = np.append(wR,float(l.split()[3]))
        well = np.append(well,float(l.split()[7]))
    
    common = set(t).intersection(timelist)
    print t
    print timelist
    print common
    exit()
    #exit()
    #w1,w2 = get_workvals_from_file(f,cols=[2,3])
    #print out
    
    wFlist = []
    wRlist = []
    welllist = []
    print timelist
    for i,o in enumerate(t):
        if o in map(int,timelist):
            wFlist.append(wF[i])
            wRlist.append(wR[i])
            welllist.append(well[i])

    print wFlist,wRlist,welllist
    return wFlist,wRlist,welllist

def calc_Ptrans(data_for_res,T):
    
    #for d in data_for_res:
    isatT = data_for_res == T
    #print isatT
    sum = np.sum(isatT)
    ave = np.average(isatT)
    
    tp1 = 0.
    tm1 = 0.
    for i,t in enumerate(isatT[:-1]):
        if t:
            if data_for_res[i+1] == T+1: tp1 += 1
            elif data_for_res[i+1] == T-1: tm1 += 1
           
    Tp1 = tp1/sum
    Tm1 = tm1/sum
    
    return Tp1,Tm1,ave
    
T_phase_transition_index = 12

#dir = "/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest_from_gmin_mindata/res/"
dir = "/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest_from_gmin_mindata/M20res_pnodes_rep/"
#dat = np.loadtxt(dir+"exchanges.permutations")
dat = np.loadtxt(dir+"ex.thinned2")

# datr[i] is time series of replicas for temperature i 
time = dat[:,0]
datr = dat[:,1:]

M = len(datr[1,:])

# Tlist[i] is time series of temperatures for replica i
Tlist = []
for m in range(M):
    T = np.nonzero(datr==m)[1]
    Tlist.append(T)

    #plt.plot(T)  
    #plt.show() 

probTlist = []
for i,t in enumerate(Tlist):
    H = np.histogram(t,bins=np.arange(-0.5,M-0.5,1.0),normed=True) 
    probTlist.append(H)  
    print H[0]
    print H[1]
    #plt.hist(t,bins=np.arange(-0.5,M-0.5,1.0),normed=True) 
    if i%4==0:
        plt.plot(H[1][1:],H[0],'-o')
plt.show()
exit()
T = T_phase_transition_index
#T = 14
Ptranslist = []
for r in range(M):
    #print Tlist[r]
    Ptrans = calc_Ptrans(Tlist[r],T)
    Ptranslist.append(Ptrans)

Ptranslist = np.array(Ptranslist)
#plt.plot(Ptranslist[:,0])
#plt.plot(Ptranslist[:,1])
#plt.plot(Ptranslist[:,2])
#plt.legend(["P(T+1|T)","P(T-1|T)","P_r(T)"])

isat13 = Tlist[2] == T
Tat13 = []
for i,truth in enumerate(isat13):
    if truth: Tat13.append(time[i])


wF,wR,well = get_energies(dir+"w12",Tat13)
plt.plot(well)
plt.show()