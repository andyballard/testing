import numpy as np
from ParallelTempering import *

# list of directories of current simulations
#dir0 = "/scratch/ab2111/tosinister/clusters/LJ75/tspacing/optimize_Tres/M16geom/"
#dirlist = [dir0 + "ResT12/",dir0+"ResT13/"]
dir0 = "/scratch/ab2111/tosinister/clusters/LJ75/tspacing/"
dirlist = [dir0 + "hsaest/nores/",dir0+"geom/nores/",dir0 + "hsaest/res/",dir0+"geom/res/"]
reslist = [None,None,13,13]
Nreplist = [25,25,25,25]

for i,d in enumerate(dirlist):
    print "Updating simulation results: ",dirlist[i]
    results = Results(d,Nreplist[i],recompute=True,reservoir=reslist[i],wdist=True)


