import ParallelTempering
import sys

try:
    dir = "./"
    Nrep = int(sys.argv[1])
    reservoir = int(sys.argv[2])
    if reservoir < 0:
        reservoir=False
except:
    print "Usage:\n python analyse.py Nrep reservoir"
    exit()

print "Nrep:",Nrep,"reservoir:",reservoir    
#results = ParallelTempering.Results(dir,Nrep,recompute=True,reservoir=reservoir,wdist=False,conv=False)
results = ParallelTempering.Results(dir,Nrep,recompute=True,reservoir=reservoir,wdist=True,edist=True)

Cv = ParallelTempering.HeatCapacity(results.wdists)

print Cv.Cv

print results
