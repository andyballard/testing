import numpy as np
import matplotlib.pyplot as plt

#number of replicas
M = 10

def get_committor(file):
    
    ex = np.loadtxt(file)
    print ex
    ex = ex[:,1:]
    exc = ex

    M = len(exc[1,:])
    A = 0
    B = len(ex[0])-1
    r = 2
    
    pA = np.zeros(M)
    pB = np.zeros(M)
    print len(pA),len(pB)
    # loop over lines
    for r in range(A,B+1):
        for i,l in enumerate(exc):
            n = l[r]
            # for given line, find where it ends up
            for lF in exc[i:]:
                nA = lF[A]
                nB = lF[B]
                #print nA,nB
                if nA == n:
                    pA[r] += 1
                    break
                elif nB == n:
                    pB[r] += 1
                    break
        sum = pA[r]+pB[r]
        pA[r] = pA[r] * 1./sum
        pB[r] = pB[r] * 1./sum
    
        print r,pA[r], pB[r]
        
    return pB


def main():
    
    direc="/home/ab2111/sinister.tmp/"
    files = [direc+"ex.thinned_hsa_LJ31",direc+"ex.thinned_geom_LJ31"]
    for file in files:
        pB = get_committor(file)
        plt.plot(pB,'-x')
    plt.legend(["hsa","geom"])
    plt.show()
    #np.savetxt("pB_obs.out", pB)
    
if __name__=="__main__":
    main()
