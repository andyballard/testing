import numpy as np
from testing.grouprotation.make_gr import Atom
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

#def map_atom(atom):

class Basis(object):
    
    def __init__(self,e):
        
        # e is a vector of bases (3x3)
        if e == None:
            self._e = ([1.,0.,0.],[0.,1.,0.,],[0.,0.,1.])
        self._e = np.array(self._e,dtype=float)
            
        self.check_norm()
        
    @property
    def e(self):
        return self._e
        
    
    @e.setter
    def e(self,value):
        
        self._e = np.array(value,dtype=float)        
        self.check_norm()
        
    def check_norm(self):

        for i,e in enumerate(self._e):
            norm = np.dot(e,e)
            if norm != 1.:
                self._e[i] = self._e[i] *1./ np.sqrt(norm)


class FrenetBasis(Basis):
    
    def __init__(self,atoms,i,*args,**kwargs):
        
        Basis.__init__(self,None)            
            
        self.index = i
        self.r = None
        self.h = None
        self.A = None
        self.kappa = None
        self.tau = None
        
        self.t = None
        self.n = None
        self.b = None
        
        self.helaxis = None
        
        self.get_basis(atoms,i)
        self.get_params(atoms)
        self.get_helaxis()

    def get_basis(self,atoms,i):
        
        self.t,self.n,self.b = self.get_tnb(atoms,self.index) 
        self.e = np.array((self.t,self.n,self.b),dtype=float)
        
    def get_tnb(self,atoms,i):

        t = atoms[i+1].coords - atoms[i].coords
        tprev = atoms[i].coords - atoms[i-1].coords
        
        b = np.cross(tprev,t)
        n = np.cross(b,t)
        
        t = t / np.sqrt(np.dot(t,t))
        b = b / np.sqrt(np.dot(b,b))
        n = n / np.sqrt(np.dot(n,n))
        return (t,n,b)
        
    def get_params(self,atoms):
        
        i = self.index
        tn,nn,bn = self.get_tnb(atoms,i+1)
        dr = np.sqrt(np.dot(atoms[i+1].coords-atoms[i].coords,atoms[i+1].coords-atoms[i].coords))
        self.kappa = np.arccos(np.dot(tn,self.t)) * 1./dr
        self.tau = np.arccos(np.dot(bn,self.b)) * 1./dr
        self.A = np.sqrt(self.kappa**2 + self.tau**2)
        #print np.dot(bn,self.b)
        
        self.r = self.kappa / (self.A**2)
        self.h = self.tau / (self.A**2)
        
      
    def get_helaxis(self):   
         
         # in local tnb frame
        local2 = np.sqrt(1.-(self.h/self.A)**2)
        local = np.array([self.h/self.A,0.0,local2])
        #print np.dot(local,local)
         
        self.helaxis = np.zeros((3)) 
        
        # check this!        
        for i in range(3):
            for j in range(3):
                self.helaxis[j] = self.helaxis[j] + local[i] * self.e[i,j]
        #normalise
        self.helaxis = self.helaxis / np.sqrt(np.dot(self.helaxis,self.helaxis))
            
def map_coords(atoms):

    # translate atom 0 to origin
    for a in atoms[::-1]:
        a.coords = a.coords-atoms[0].coords
        #map_atom(a)

    

f="/home/ab2111/pybgs/ref0.bb.pdb"

dat = np.genfromtxt(f,dtype=str)

atoms = []
x = []
y = []
z = []
for d in dat:
    atom = Atom(int(d[1]),d[2],d[3],int(d[5]))
    coords = np.array(d[6:9], dtype='|S12')
    atom.coords = coords.astype(np.float)
    atoms.append(atom)
    #print atom.coords

map_coords(atoms)

for atom in atoms:    
    x.append(atom.coords[0])
    y.append(atom.coords[1])
    z.append(atom.coords[2])
#print dat

helaxes = []
for i in range(1,len(atoms)-4):
    frenet0 = FrenetBasis(atoms,i)
    #helax = np.array([a*frenet0.helaxis for a in range(1,-50,-1)])
    helaxes.append(frenet0.helaxis)
    print i, frenet0.helaxis, np.dot(frenet0.helaxis,frenet0.helaxis)
    
#helaxave = np.mean(helaxes,axis=0)
mdat = np.ma.masked_array(helaxes,np.isnan(helaxes))
helaxave = np.mean(mdat,axis=0)
print "helaxave: ", helaxave
cp = np.copy(helaxave)

fig = plt.figure()
ax = fig.add_subplot(111,projection='3d')
ax.plot(x,y,z)
helline = np.array([a*cp for a in range(1,300,1)])
#print [a*helaxave for a in range(1,30,1)]
ax.plot(helline.T[0],helline.T[1],helline.T[2])
plt.show()
