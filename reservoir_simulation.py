from database_eigenvecs import *
from pele.utils.hessian import get_sorted_eig
from pele.takestep import displace 
from bh_sampling import *
from pele.thermodynamics import free_energy
from pele.mindist import MinPermDistAtomicCluster, ExactMatchAtomicCluster

import pylab as plt

import numpy as np
import sys
import pele.systems as systems
from pele.mc import MonteCarlo
from sampling import hsasampling, worksimulation
import hsa_utils.utils as utils

class ReservoirSimulation():
    
    def __init__(self,system,reservoir,Temperature,tau=None):

        self.system = system
        self.reservoir = reservoir
        self.Tsystem = Temperature
        self.Treservoir = reservoir.Treservoir
        
        # 
        #self.Ehsa = self.reservoir.getEhsa()    
        self.Esys = self.system.get_potential()   
        # initial coordinates at global minimum            
        coords = reservoir.db.minima()[0].coords
        self.coords = coords

        # storage for well index of physical replica
        self.well = None
        
        # initialize MC protocol for system
        self.stepsize = 0.0025
        self.mcstep = displace.RandomDisplacement(stepsize=self.stepsize)
        self.mc = MonteCarlo(coords,self.Esys,temperature=self.Tsystem,takeStep=self.mcstep,iprint=1)
        
        self.tau = tau

        # initialize work simulation
        if self.tau != None:
            self.wsim = worksimulation.WorkSimulation(self.system,self.reservoir,coords,coords,self.Tsystem,self.tau)
        
        
        # work values list, array initialization
        self.wFvals = []
        self.wRvals = []
        self.wvals = []
        self.swapbin = []

    
    def printStats(self,wF,wR,w,swap):
        """print acceptance stats of exchanges"""
        
        print wF,wR,w,swap,self.reservoir.well,self.well


    def display(self):
        
        plt.plot(self.wFvals)
        plt.plot(self.wRvals)
        plt.legend(["work F","work R"])
        plt.show()
        
    def display_hist(self):
        self.wFvals = np.array(self.wFvals)
        self.wRvals = -np.array(self.wRvals)
        #bins = np.arange(min(self.wFvals+-1.*self.wRvals),max(self.wFvals+-1.*self.wRvals))
        n, bins, patches = plt.hist(self.wFvals, 40, normed=1, facecolor='green', alpha=0.5)
        n, bins, patches = plt.hist(-self.wRvals, 40, normed=1, facecolor='blue', alpha=0.5)

        #plt.legend(["work F","work R"])
        plt.show()

       
    def run(self,Niteration,iprint=False,Nmc=100):
        
        coords = np.copy(self.coords)
        
        for i in range(Niteration):
            
            mc = MonteCarlo(self.coords,self.Esys,temperature=self.Tsystem,takeStep=self.mcstep,iprint=10000000)
            #print "ebefore", self.Esys.getEnergy(self.coords)
            mc.run(Nmc)
            #print "eafter", mc.markovE
            coords0 = np.copy(mc.coords)
                        
            rescoords = self.reservoir.sample()
            #print self.Esys.getEnergy(rescoords)

            #rescoords = self.reservoir.rescoords
            
            #rescoords = np.copy(self.reservoir.rescoords)
        
            if self.tau != None:
                # Work simulation variant:
                (workF,workR) = self.wsim.run(self,coords0,rescoords)
                rescoords = self.wsim.rescoords
                print "iteration", i
            else:                 
                (workF,workR)  = self.calcWorkvals(coords0,rescoords)    
            
            (work,swap,self.coords) = self.exchange(workF,workR,rescoords,coords0)

            if iprint != False:
                if (i%iprint)==0: 
                    print i,
                    self.printStats(workF,workR,work,swap)
    
    def exchange(self,workF,workR,rescoords,coords0):
        
        work = workF + workR
        
        ran = np.random.random()
        
        swap = False
        
        if work < 0.0:
            swap=True
        elif ran < np.exp(-work):
            swap=True
        
        #swap = False
        
        if swap:
            x = rescoords
            #self.coords = rescoords
            #self.mc.coords = rescoords
        else:
            x = coords0
            #self.coords = coords0
       #     self.mc.coords = np.copy(coords0)
        
        # update 
        self.wFvals.append(workF)
        self.wRvals.append(workR)
        self.wvals.append(work)
        if swap:
            self.swapbin.append(1)
        else:
            self.swapbin.append(0)
        return work, swap, x
    
    def calcWorkvals(self,coords,rescoords):
        
        #EhsaF,wellF = self.reservoir.getEhsa(rescoords)
        EhsaF = self.reservoir.Ehsa
        EphysF = self.Esys.getEnergy(rescoords)
        EhsaR,self.well = self.reservoir.getEhsa(coords)
        EphysR = self.Esys.getEnergy(coords)
        
        workF = EphysF/self.Tsystem - EhsaF/self.Treservoir
        workR = EhsaR/self.Treservoir - EphysR/self.Tsystem 

        #print "res:", self.reservoir.Ehsa, self.Esys.getEnergy(rescoords), 
        #print "sys:", self.Esys.getEnergy(coords), self.reservoir.getEhsa(coords) 
        return workF,workR
    
def main():
    
    Niterations = int(sys.argv[1])
    Tsystem = float(sys.argv[2])
    outfile = str(sys.argv[3])
    #Niterations = 100
    #Tsystem = 0.03
    #outfile = "test"
    dir = "/home/ab2111/projects/testing/nrre/pele/"
    kappa = 87
    
    Treservoir = Tsystem
    
    # System initialization
    #system = systems.BLJCluster(natoms=31,ntypeA=31,epsAB=1.2,sigAB=1.0,epsBB=1.0,sigBB=1.0,rcut=10.0)
    system = systems.LJCluster(natoms=31)

    # Reservoir initialization
    db = system.create_database("/home/ab2111/projects/testing/input_files/lj31_183.db")   
    #get_thermodynamic_information(system,db)
    reservoir = hsasampling.HSA(system,Treservoir,kappa,db)
    print reservoir.Pw    

    # Work simulation parameter:
    tau = 10
    # Run simulation 
    simulation = ReservoirSimulation(system,reservoir,Tsystem,tau=tau)
    simulation.run(Niterations,iprint=True)

    # display Bennett's ODM plot
    utils.writeWvals(simulation.wFvals, simulation.wRvals, simulation.swapbin,dir + "workT"+str(Tsystem)+"N"+str(Niterations)+"tau"+str(tau))
    utils.plotBennettODM(simulation.wFvals, simulation.wRvals, needhist=True,minval=-5.,maxval=10.)
    #simulation.display_hist()
if __name__ == "__main__":
    #main()
    pass