import numpy as np
import Pacc_estimation 
import matplotlib.pyplot as plt
import pele.systems as systems
from bh_sampling import *

def launch_normalmode_browser(system, coords):
    import OpenGL
    from PyQt4 import QtGui, QtCore, Qt
    from OpenGL.GLUT import glutInit
    from pele.gui.normalmode_browser import NormalmodeBrowser
    import sys
    glutInit()
    app = QtGui.QApplication(sys.argv)
    
    wnd = NormalmodeBrowser(app=app, system=system)
    wnd.set_coords(coords)
    
    wnd.show()
    sys.exit(app.exec_())     


class ThermEst():
    
    def __init__(self,Nat):
        
        self.natoms = Nat
    
    def Generate_database(self,db0,sys,permlist,maxstep=0.001):

    
        db = sys.create_database()
        quench = sys.get_minimizer(tol=1e-6)
        from pele.optimize import lbfgs_py, fire
        pot = sys.get_potential()
        
#         quench = lambda x: lbfgs_py(x, pot, iprint=2, maxstep=.01, tol=1e-10, armijo=True)
#         quench = lambda x: fire(x, pot, iprint=2)
        for m in db0.minima():
            yd = np.copy(m.coords)
            #yd = self.random_displacement(np.copy(m.coords),maxstep)
            for pindex in permlist:
                pindex = 0
                #y0 = np.copy(m.coords)
                y0 = np.copy(yd)
                y = self.get_permutation(y0,pindex)
                Esys = pot.getEnergy(y)
                #y = np.copy(y0)
                #print x
                quenched = quench(y)
                #print y
                #print pindex,quenched.energy
                ret = db.addMinimum(quenched.energy, quenched.coords)
                
                #get_thermodynamic_information(sys,db)
                try:
                    #nm = m.hessian_eigs[0]
                    #evals = nm.eigenvalues
                    #vectors = nm.eigenvectors
                    get_thermodynamic_information(sys,db)
                
                except ValueError as err:
                    print "Err",pindex, err
                    print quenched.energy,Esys,m.energy
                    #yd = self.random_displacement(y0,maxstep)
                    #dquenched = quench(yd)
                    #print dquenched.energy
                    #get_thermodynamic_information(sys,db)
                    e,g,hess = pot.getEnergyGradientHessian(quenched.coords)
                    eval, evec = get_eig(hess)
                    #print eval#, evec[0]
#                     launch_normalmode_browser(sys, quenched.coords)
                    
                    from pele.transition_states._tstools import minima_from_ts
                    mode = evec[0,:]
                    ret1, ret2 = minima_from_ts(pot, quenched.coords, mode, quench=quench, stepmin=.1)
                    
                    newcoords = ret1.coords
                    newenergy = ret1.energy
                    e,g,hess = pot.getEnergyGradientHessian(newcoords)
                    eval, evec = get_eig(hess)#
                    assert min(eval) > -1e-3
#                     launch_normalmode_browser(sys, newcoords)

                    
#                     exit()
                    #np.savetxt("problem_config",quenched.coords)
                    #np.savetxt("problem_evals",eval)
                    #np.savetxt("problem_evec0",evec[0])
                    #exit()
                    #print quenched.coords
                    #exit()

        get_thermodynamic_information(sys,db)

        
        return db
    
    def random_displacement(self,x,maxstep):
        
        r = np.array(np.random.random(3*self.natoms))
        r = r * maxstep
        
        return x + r
    
    def get_permutation(self,x,index):
        
        natoms = int(len(x)/3)
        x0 = np.reshape(x,(natoms,3))
        #print x0[-1]
        #print x0[index]
        
        temp = np.copy(x0[-1])
        #x0[-1] = np.copy(x0[index])
        x0[-1] = x0[index]
        x0[index] = temp
        #x0[-1] = x0[index]
        #(dummy,x0[index]) = (x0[index],x0[-1])
        
        #print x0[-1]
        #print x0[index]
        return x0.flatten()
        
    
def getEvibpgo(db):
    
    E = np.array([ m.energy for m in db.minima() ])
    pgo = np.array([ m.pgorder for m in db.minima() ]) 
    vib = np.array([ m.fvib for m in db.minima() ] )

    return E, vib, pgo
        


def main():

    Nat = 38

    mult = 4
    Nit=mult*mult
    Ndof = 3*Nat-6

    #Nit = 1    
    # reference system setup
    system0 = systems.LJCluster(natoms=31)
    db0 = system0.create_database("/home/ab2111/projects/testing/input_files/lj38_100min.sqlite")
    E0,vib0,pgo0 = getEvibpgo(db0)

    pacc = Pacc_estimation.Paccest_Iteration(kappa=0.5*Ndof, E=E0,vib=vib0,pgo=pgo0);
    mindat=zip(E0,vib0,pgo0)
    cv = pacc.calcCv_curve(Tstart=0.01,Tfinish=0.4,Ncalcs=40)
    np.savetxt("Cveps1sig1", cv)
    np.savetxt("min.dateps1sig1", mindat)
    
    #pot0 = system0.get_potential()
    #print "E0 ", pot0.getEnergy(db0.minima()[0].coords)

    estimation = ThermEst(Nat)
    permlist = np.arange(Nat-1)
    
    for i in range(Nit):

        # declare new binary system
        epsAB=0.8 + 0.5 * (i / mult) / mult
        #epsAB=1.1
        #sigAB=0.9
        sigAB=0.8 + 0.5 * (i % mult) / mult
        print epsAB,sigAB,i

        sys = systems.BLJCluster(natoms=38,ntypeA=37,epsAB=epsAB,sigAB=sigAB,epsBB=1.0,sigBB=1.0,rcut=10.0)
        #pot = sys.get_potential()
        #print "E ", pot.getEnergy(db0.minima()[0].coords)
        #exit()
        # generate minima of new system from original
        db = estimation.Generate_database(db0,sys,permlist,maxstep=0.03)
        E,vib,pgo = getEvibpgo(db)
        
        # get cv curve of new system minima
        pacc = Pacc_estimation.Paccest_Iteration(kappa=0.5*Ndof, E=E,vib=vib,pgo=pgo);
        mindat=zip(E,vib,pgo)
        cv = pacc.calcCv_curve(Tstart=0.01,Tfinish=0.4,Ncalcs=40)
        np.savetxt("Cveps"+str(epsAB)+"sig"+str(sigAB), cv)
        np.savetxt("min.dateps"+str(epsAB)+"sig"+str(sigAB), mindat)

if __name__=="__main__":
    main()
    