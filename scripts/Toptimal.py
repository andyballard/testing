import numpy as np
import Pacc_estimation 
import matplotlib.pyplot as plt
from pele.thermodynamics import free_energy
from collections import defaultdict
import hsa_utils.utils as utils

def main():
    #puma
    kappa = 0.5*(3*581)
    #tetra-ala
    #kappa = 0.5*(3*52)
    #LJ31
    #kappa = 0.5(3*31)

    minfile ="/home/ab2111/projects/pacc_est/puma/min.data" 
    p = Pacc_estimation.Paccest_Iteration(kappa=kappa,minfile=minfile)
    #p.Ti = 300.
    p.iterate(N=15,T0=223.68)
    #exit()


if __name__ == "__main__":
    main()