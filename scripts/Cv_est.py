from Pacc_estimation import Minima
import matplotlib.pyplot as plt
import numpy as np

def Cv_est(minfile,kappa,Tmin,Tmax):

    minima = Minima(kappa=kappa,minfile=minfile,gas_constant=False,sort=True)
    #p = Pacc_estimation.Paccest_Iteration(kappa=kappa,minfile=minfile,gas_constant=True,sort=True)

    Cv = minima.calcCv_curve(Tmin,Tmax)
    Cvplot=zip(*Cv)
    
    return Cvplot
    # 
    # for (t,c) in Cv:
    #     print t, c*p.R
    # plt.plot(Cvplot[0],[x*p.R for x in Cvplot[1]])
    # np.savetxt("CvvsT",zip(Cvplot[0],[x*p.R for x in Cvplot[1]]))
    # plt.show()
