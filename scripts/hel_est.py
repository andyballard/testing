from Pacc_estimation import Minima
import matplotlib.pyplot as plt
import numpy as np

def hel_est(Tmin,Tmax,**kwargs):
    #puma
    kappa = 0.5*(3*581)
    #tetra-ala
    #kappa = 0.5*(3*52)
    #LJ31
    #kappa = 0.5(3*31)
    
    #minfile="/scratch/ab2111/tosinister/puma/quench-from-ycc-sim/SecStructure/min.dathel"
    #p = Pacc_estimation.Paccest_Iteration(kappa=kappa,minfile=minfile,gas_constant=True,sort=True,helicity=True)
    minima = Minima(**kwargs)
    
    # Plot HSA estimation of Cvcurve between Tmin and Tmax
    #Tmin = 220.
    #Tmax = 650.
    hel = minima.calcHel_curve(Tmin,Tmax)
    helplot=zip(*hel)
    
    nbins = 36.
    
    #phel = minima.calcPhel(T, nbins=nbins)

    #plt.plot(helplot[0],helplot[1])
 
    #return phel
    return helplot
    #np.savetxt("CvvsT",zip(Cvplot[0],[x*p.R for x in Cvplot[1]]))