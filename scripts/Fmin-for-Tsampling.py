import numpy as np
import Pacc_estimation 
import matplotlib.pyplot as plt
from pele.thermodynamics import free_energy
from collections import defaultdict

#puma
kappa = 0.5*(3*581)
#tetra-ala
#kappa = 0.5*(3*52)
#LJ31
#kappa = 0.5(3*31)

minfile ="/home/ab2111/projects/pacc_est/puma/min.data" 
p = Pacc_estimation.Paccest_Iteration(kappa=kappa,minfile=minfile)

T=300.
F=free_energy(p.E,p.pgo,p.vib,T,kappa)
Fhel=free_energy(p.helE,p.helpgo,p.helvib,T,kappa)
F0 = min(F)
F    -= F0
Fhel -= F0
    
# Arrange the minima into groups definied by the Temperature at which they were sampled
Ts = set(p.Tindex)
FofTsampling = defaultdict(list)
for k,val in enumerate(F):
    FofTsampling[p.Tindex[k]].append(val/T)

# Plot F_m for all minima m at various temperatures
leg = []
for t,Flist in FofTsampling.items():
    plt.plot(Flist,'-o')
    leg.append(str(t))

# Plot F_helix for alpha-helical minimum
plt.plot(np.arange(75),[Fhel/T for i in range(75)],'--')
leg.append('helix')
plt.legend(leg)
plt.title("Temp = "+str(T))
plt.show()
