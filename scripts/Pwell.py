import numpy as np
import Pacc_estimation 
import matplotlib.pyplot as plt
from pele.thermodynamics import free_energy
from collections import defaultdict
import hsa_utils.utils as utils

def get_cdf(p,T):

    p0 = utils.calcPw(p.E,p.pgo,p.vib,T=T,kappa=p.kappa)
    c=0.
#     cdf = [c + p for p in sorted(p0,reverse=True)]
    cdf = []
    for p in sorted(p0,reverse=True):
        c = c + p
        cdf.append(c)
    plt.plot(cdf,'-')

def main():

    #puma
    kappa = 0.5*(3*581)
    minfile ="/home/ab2111/projects/pacc_est/puma/min.data" 

    Tmin=250.
    Tmax=400.
    N=10

    p = Pacc_estimation.Paccest_Iteration(kappa=kappa,minfile=minfile,sort=True)

    Tlist = np.linspace(Tmin,Tmax,N)
    print Tlist.astype('|S3')

    for t in Tlist:
        get_cdf(p,t)

    plt.legend(Tlist.astype(str))

    plt.show()

if __name__=="__main__":
    main()
