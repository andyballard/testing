import numpy as np
from testing.analysis.ParallelTempering import Results
from testing.Optimize import *
import matplotlib.pyplot as plt

direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/optimize_Tres/M16geom/ResT13/"
Nrep =16
kappa = 0.5*(3*75-6) #LJ75
minfile="/scratch/ab2111/plutoscratch/projects/testing/input_files/min.data.lj75"
    
results = Results(direc,Nrep)

plt.plot(results.Tlist,results.Pacc,'-o')

HSAest = TSpacingOptimizer(results.Tlist[0],kappa=kappa,minfile=minfile,gas_constant=False,sort=True,maxminima=1000)

HSApacc = []
for i in range(len(results.Tlist)-1):
    HSAest.T = results.Tlist[i]
    HSAest.mdist_pert.T = results.Tlist[i+1]
    HSApacc.append(HSAest.calcPaccest())
    
plt.plot(results.Tlist[:-1],HSApacc,'-x')
plt.legend(["simulation","HSA"])
plt.show()
