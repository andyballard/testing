import sys
import matplotlib.pyplot as plt
import numpy as np
from testing.analysis.trackrep import state_trajectory

#outfile="convergenceLJ75.png"
outfile="trajectory-LJ75geom.png"
plt.rc('lines', linewidth=2)
# plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 25
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 20
plt.rcParams['legend.fancybox'] = True  
plt.rcParams['legend.fontsize'] = 14
plt.rcParams['axes.labelsize'] = 20 
plt.rcParams['xtick.labelsize'] = 20
plt.rcParams['ytick.labelsize'] = 20
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3
plt.rcParams['figure.autolayout'] = True
  

f="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/geom_from_gmin_mindata/M12_res_at_transition/restart/exchanges.permutations"
dat = np.genfromtxt(f)

datr = dat[:,1:]

R=0
traj = state_trajectory(datr,R=R)

plt.plot(np.arange(0,len(traj)*1./1000,0.001),traj)
plt.xlabel(r"time [ x $10^6$ MC steps ] ")
plt.ylabel(r"Temperature index $k(t)$")
#plt.show()


plt.savefig(outfile)