import numpy as np
import pylab as plt
from scripts.hel_est import hel_est
from scripts.Pwell import MinimaDistribution

plt.xlabel('fraction helicity')
plt.ylabel('occurance')
plt.title('Distribution of fraction helicity')
#outfile="hel-approx-dist.png"
outfile="hel-approx-ave.png"


plt.rc('lines', linewidth=5)
plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 10
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 15
#plt.rcParams['legend.fancybox'] = True  
#plt.rcParams['legend.fontsize'] = 8
plt.rcParams['axes.labelsize'] = 10 
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

#puma
kappa = 0.5*(3*581)
#tetra-ala
#kappa = 0.5*(3*52)
#LJ31
#kappa = 0.5(3*31)
minfile="/scratch/ab2111/tosinister/puma/mindata-from-ycc-sim-11000structures/min.data.all"

#generate HSA-approximation
#helplot = hel_est(kappa=kappa,minfile=minfile,gas_constant=True,sort=True,helicity=True)

leg = []
Tmin = 220.
Tmax = 650.
helplot = hel_est(Tmin,Tmax,kappa=kappa,minfile=minfile,gas_constant=True,sort=True,helicity=True)
#print helplot
#plt.plot(helplot[0],helplot[1])

#midbin = np.zeros(len(helplot[1])-1)
#for i in range(len(helplot[1])-1):
#    midbin[i] = 0.5*(helplot[1][i]+helplot[1][i+1])

#plt.subplot(1,2,1)
#plt.plot(midbin,helplot[0],'-',ms=15, lw=2, alpha=0.7, mfc='blue')

#Tlist = [250.,340.,478.,600.]
Tlist = [230.,320.,430.]


mdistribution = MinimaDistribution(6050.,kappa=kappa,minfile=minfile,gas_constant=True,sort=True,helicity=True)
s = sorted(range(len(mdistribution.E)),key=lambda i: mdistribution.Pwell[i])
s = s[::-1]
Tindexordered = [mdistribution.Tindex[i] for i in s]

# replica index vs energy
print mdistribution.E
#plt.plot(mdistribution.Tindex,mdistribution.R*mdistribution.E,'x')
plt.plot(Tindexordered,'x')
plt.show()
exit()

ax = []

for i,t in enumerate(Tlist):
    mdistribution.T = t
    #ax.append(plt.subplot(len(Tlist),1,len(Tlist)-i))
    #helplot = ax[i].hist(mdistribution.hel,weights=mdistribution.Pwell,normed=True,histtype='bar')
    #ax[i].legend(["T="+str(t)])
    #ax[i].set_ylim(0.,12.)
    #ax[i].set_xlim(0.,1.)
    #if i==0:
    #    ax[i].set_ylabel("occurance")
    #    ax[i].set_xlabel("fraction helicity")
    #leg.append("T="+str(t))

#plt.suptitle("Distribution of fraction helicity from well minima")
#get simulation data
dat = np.loadtxt("/scratch/ab2111/tosinister/puma/quench-from-ycc-sim/SecStructure/helicity-sim-ave-temp.dat",usecols=(0,2))
plt.plot(dat[:,0],dat[:,1],'-')
plt.legend(["hsa-approx","Amber PT simulation"])
#plt.legend(leg)
#plt.ylim(0.,12.)
#plt.xlim(0.,1.)

plt.show()
#plt.savefig(outfile)