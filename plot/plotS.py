import numpy as np
import pylab as plt
import math

plt.xlabel('time [ x $10^7$ ]')
# plt.xlabel('time')
plt.ylabel('Entropy')
plt.title('Shannon Entropy of Replica Occupation')
outfile="S.png"


plt.rc('lines', linewidth=2)
# plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 10
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 15
plt.rcParams['legend.fancybox'] = True  
plt.rcParams['legend.fontsize'] = 17
plt.rcParams['axes.labelsize'] = 10 
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

#dat = np.loadtxt("ptpacc",usecols=(0,4))
data= np.loadtxt("S_geom",usecols=(0,1))
data2= np.loadtxt("S_hsaest",usecols=(0,1))

data[:,0] = data[:,0] / 10000000
data2[:,0] = data2[:,0] / 10000000
#fig,ax = plt.subplots()
plt.plot(data[:,0],data[:,1],'-',ms=15, lw=2, alpha=0.7, color='black')
plt.plot(data2[:,0],data2[:,1],'-',ms=15, lw=2, alpha=0.7, color='blue')
# polt.plot(math.log(10))
x = np.arange(0, 4.2, 0.2) 
y = [math.log(10) for xi in x]
plt.plot(x, y,'--',color="red") 

plt.legend(['geometric','HSA-enhanced','log(M)'],loc=4)
#plt.savefig(outfile)
plt.show()
