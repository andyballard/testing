import sys
import matplotlib.pyplot as plt
import numpy as np
from testing.analysis.trackrep import state_trajectory

#outfile="convergenceLJ75.png"
outfile="trajectory-LJ75geom.png"
plt.rc('lines', linewidth=2)
# plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 25
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 20
plt.rcParams['legend.fancybox'] = True  
plt.rcParams['legend.fontsize'] = 14
plt.rcParams['axes.labelsize'] = 20 
plt.rcParams['xtick.labelsize'] = 20
plt.rcParams['ytick.labelsize'] = 20
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3
plt.rcParams['figure.autolayout'] = True
  

files = []
files.append("/scratch/ab2111/tosinister/clusters/LJ31/tspacing/analysis_at_transition/geomR.ave.corr.cut")
files.append("/scratch/ab2111/tosinister/clusters/LJ31/tspacing/analysis_at_transition/R.ave.corr.cut")

for f in files:
    Corr = np.loadtxt(f)
    plt.plot(Corr)
    
plt.legend(['geometric','HSA-enhanced'])
plt.yscale('log')

plt.xlabel(r"time [ x $10^6$ MC steps ] ")
plt.ylabel(r"C_k(t)/C_k(0)")
plt.ylim([0.001,1.0])
plt.show()

#plt.plot(np.arange(0,len(traj)*1./1000,0.001),traj)

plt.show()
#plt.savefig(outfile)