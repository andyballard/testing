import numpy as np
import pylab as plt
from scripts.Cv_est import Cv_est
from scripts.Pwell import MinimaDistribution, find_dominant_wells
from Pacc_estimation import Minima

plt.xlabel('Temperature')
plt.ylabel('number of wells')
plt.title('\# of wells needed for 99\% of probability')
outfile="n99vsTemp-all.png"


plt.rc('lines', linewidth=4)
plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 10
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 15
plt.rcParams['legend.fancybox'] = False  
plt.rcParams['legend.fontsize'] = 12
plt.rcParams['axes.labelsize'] = 10 
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

#puma
kappa = 0.5*(3*581)
#tetra-ala
#kappa = 0.5*(3*52)
#LJ31
#kappa = 0.5(3*31)
    

minfile="/scratch/ab2111/tosinister/puma/mindata-from-ycc-sim-11000structures/min.data.all"
#minfile="/scratch/ab2111/tosinister/trpzip/min.data"

#minfile="/scratch/ab2111/tosinister/puma/mindata-from-ycc-gmin/min.data"

#p = Pacc_estimation.Paccest_Iteration(kappa=kappa,minfile=minfile,sort=True,gas_constant=True)

Tmin = 220.
Tmax = 650.
Tlist = np.arange(Tmin,Tmax,10.)
mdistribution = MinimaDistribution(Tmin,kappa=kappa,minfile=minfile,sort=True,gas_constant=True)
minima = Minima(kappa=kappa,minfile=minfile,sort=True,gas_constant=True,helicity=True)
#Tlist = np.array([250.,320.,450.,500.,600.])
#Tlist = [250.,340.,478.,600.]
#Tlist = [250.]
#plot_FETS(p,300.)

ratios = find_dominant_wells(minima,230.,320.)
#plt.plot(zip(*ratios)[0][0:100])
plt.plot(zip(*ratios)[0],zip(*ratios)[1],'x')
#plt.plot([i*100 for i in zip(*ratios)[2][0:100]])

plt.show()
exit()

leg=[]
num=[]
#plot_FETS(p,550.)
for t in Tlist:
    #pass
    mdistribution.T = t
    num.append(mdistribution.num_to_cdfval(0.99,sort=True))
    #mdistribution.plot_FETS(t)
    #leg.append("T="+str(t))
plt.plot(Tlist,num,'-')
#plt.legend(leg)
#plt.xlim([0,100])
#plt.ylim([-52700,-52300])
#plt.xlim([-4800,-4400])
#plt.savefig(outfile)
plt.show()
