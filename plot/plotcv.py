import numpy as np
import pylab as plt
#from scripts.Cv_est import Cv_est
from landscape import MinimaDistribution

plt.xlabel('Temperature (K)')
plt.ylabel('Cv')
plt.title('Heat Capacity')
outfile="schematic-fig.png"


plt.rc('lines', linewidth=2)
plt.rc('axes', color_cycle=['b', 'g', 'r', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 20
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 15
plt.rcParams['legend.fancybox'] = False  
plt.rcParams['legend.fontsize'] = 12
plt.rcParams['axes.labelsize'] = 20 
plt.rcParams['xtick.labelsize'] = 15
plt.rcParams['ytick.labelsize'] = 15
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
#plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

#kappa = 0.5*(3*581) #puma
#puma-wt???
#tetra-ala
#kappa = 0.5*(3*52)
#LJ31
#kappa = 0.5*(3*31-6)
#LJ75
kappa = 0.5*(3*75-6)

minfile="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest_from_gmin_mindata/res/min.data"  
#minfile ="/scratch/ab2111/tosinister/clusters/LJ31/benchmarking/treservoir/min.data"
  
#minfile ="/home/ab2111/projects/pacc_est/puma/min.dataall" 
#minfile="/scratch/ab2111/tosinister/trpzip/min.data"
#minfile="/scratch/ab2111/plutoscratch/projects/testing/input_files/min.data.lj75"
#minfile="/home/ab2111/projects/testing/input_files/min.data"
    
# Plot HSA estimation of Cvcurve between Tmin and Tmax
Tmin = 0.01
Tmax = 0.12
Nbins = 100
Tlist = np.arange(Tmin,Tmax,(Tmax-Tmin)/Nbins)

  
rho = MinimaDistribution(Tmin,kappa=kappa,minfile=minfile,gas_constant=False,sort=True)
Cv = []

for t in Tlist:
    rho.T = t
    Cv.append(rho.Cv)
plt.plot(Tlist,Cv)
"""
plt.figure(1)

plt.subplot(211,aspect=0.001)
plt.plot(Tlist,Cv)
plt.yticks(np.arange(40,85,20))
plt.xticks(np.arange(0.02,0.07,0.02))
plt.ylabel(r"$C_{\rm v}$")

plt.subplot(212,aspect=0.285)
"""
direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/"
files = [direc+"geom_from_gmin_mindata/M12_res_at_transition/restart/results/Cv",direc+"hsaest_from_gmin_mindata/M12_res_at_transition/restart/results/Cv"]
for f in files:
    Tlist,Cv = np.loadtxt(f,unpack=True)
    plt.plot(Tlist,Cv, '-o',ms=12, lw=2)
#plt.ylabel(r"$\langle P_{\rm acc} \rangle$")
plt.ylabel(r"$C_V$")
plt.xlabel("Temperature")
#plt.xticks(np.arange(0.02,0.07,0.02))
#plt.yticks(np.arange(0.1,0.3,0.05))
plt.xlim(Tmin,Tmax)
#plt.ylim(0.12,0.26)
#generate HSA-approximation
#cvplot = Cv_est(minfile,kappa,Tmin,Tmax)
#cvplotgmin = Cv_est(minfilegmin,kappa,Tmin,Tmax)
#cvplotminus = Cv_est(minfileminus,kappa,Tmin,Tmax)
#cvploto = Cv_est(minfileold,kappa,Tmin,Tmax)
#print cvplot[0]
#print cvplot[1]
#R = 0.001987
#R = 1.0
#plt.plot(cvplot[0],[x*R for x in cvplot[1]],'-',ms=15, lw=2, alpha=0.7, mfc='blue')
#plt.plot(cvplotgmin[0],[x*R for x in cvplotgmin[1]],'-',ms=15, lw=2, alpha=0.7, mfc='blue')
#plt.plot(cvplotminus[0],[x*R for x in cvplotminus[1]],'--',ms=15, lw=2, alpha=0.7, mfc='blue')
#plt.plot(cvploto[0],[x*R for x in cvploto[1]],'--',ms=15, lw=2, alpha=0.7, mfc='blue')
#data= np.loadtxt("/scratch/ab2111/tosinister/puma/quench-from-ycc-sim/Cvplots/HeatCapacityvsT.dat")
#plt.plot(data[:,0],data[:,1],'-',ms=15, lw=2, alpha=0.7, mfc='blue')
plt.legend(["HSA estimation","PT: geometric","PT: hsa-enhanced"])

#plt.savefig(outfile)
plt.show()
