import numpy as np
import pylab as plt
#from scripts.Cv_est import Cv_est
from landscape import MinimaDistribution

kappa = 0.5*(3*75-6)
    
#minfile ="/home/ab2111/projects/pacc_est/puma/min.dataall" 
#minfile="/scratch/ab2111/tosinister/puma/quench-from-ycc-sim/min.data_old"
#minfile="/scratch/ab2111/tosinister/trpzip/min.data"
#minfile="/scratch/ab2111/tosinister/puma/mindata-from-ycc-sim-11000structures/min.data.new"
minfile="/scratch/ab2111/plutoscratch/projects/testing/input_files/min.data.lj75"
#minfile="/home/ab2111/projects/testing/input_files/min.data"
#minfile="/scratch/ab2111/tosinister/puma/mindata-from-ycc-sim-11000structures/min.data.new"
#minfileold="/scratch/ab2111/tosinister/puma/mindata-from-ycc-sim-11000structures/halfNEW/m"
#minfileminus="/scratch/ab2111/tosinister/puma/mindata-from-ycc-sim-11000structures/NEW/m_minus"
#minfilegmin="/scratch/ab2111/tosinister/puma/mindata-from-ycc-gmin/min.data"
#minfileold="/scratch/ab2111/tosinister/puma/mindata-from-ycc-sim-11000structures/OLD/m"
#minfile="/scratch/ab2111/tosinister/puma/quench-from-amber-pt/quench3/min.data"
    
# Plot HSA estimation of Cvcurve between Tmin and Tmax
Tmin = 0.065
Tmax = 0.09
Nbins = 10
#Tlist = np.arange(Tmin,Tmax,(Tmax-Tmin)/Nbins)
#Tlist=[0.7393747737E-01]
Tlist=[0.6258341293E-01]

#dir="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/optimize_Tres/T"
dir="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/optimize_Tres/M16geom/ResT12"

files = []
#sub = [".065",".070",".075",".080"]
#for s in sub:
#    files.append(dir+s+"/w")
files.append(dir+"/w")

for f in files:    
    simdat = np.loadtxt(f,usecols=[6,7])
    hist,edges = np.histogram(simdat[:,1],bins=100,range=(-0.01,99.99),normed=True)
    plt.plot(np.log(hist))
#plt.legend(sub)

rho = MinimaDistribution(Tmin,kappa=kappa,minfile=minfile,gas_constant=False,sort=True)
leg=[]
for t in Tlist[0:1]:
    rho.T = t
    plt.plot(range(1,len(rho.Pwell)+1),np.log(rho.Pwell))
    leg.append(str(t))

plt.xlim([0,100])
#plt.ylim([-5, 0])
plt.legend(leg)
plt.show()
