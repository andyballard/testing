from testing.hsa_utils.utils import ODM
import numpy as np
from testing.analysis.ParallelTempering import get_workvals_from_file
import matplotlib.pyplot as plt

#plt.xlabel('Temperature (K)')
#plt.ylabel('Cv')
#plt.title('Heat Capacity')
outfile="10a_figure_overlap_test.png"


plt.rc('lines', linewidth=1)
plt.rc('axes', color_cycle=['b', 'g', 'r', 'y'])
plt.rc('font',**{'family':'sans-serif','sans-serif':['Arial']})
plt.rcParams['axes.titlesize'] = 20
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 14
plt.rcParams['legend.fancybox'] = False  
plt.rcParams['legend.fontsize'] = 12
plt.rcParams['axes.labelsize'] = 20 
plt.rcParams['xtick.labelsize'] = 20
plt.rcParams['ytick.labelsize'] = 20
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
plt.rcParams['lines.linewidth']=2
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

# simulation details:
dir="/scratch/ab2111/plutoscratch/projects/kirkwood/TTodm/"
RREXdir = dir+"RREX_ener_T_20_50_100_200_kJmol/RREX_ener_T_20_50_100_200_kJmol_thinned.dat"
#RREXdir = dir+"RREX_ener_T_20_50_100_200_kJmol/short"
TREXdir = dir+"TREX_ener_T_20_50_100_200_kJmol/TREX_ener_T_20_50_100_200_kJmol_thinned.dat"
#dirs = [RREXdir,TREXdir]
dirs = [RREXdir]
titles = ["TREX","RREX"]
Nreplicas=4

# Temperature list in Kelvin  and kJ/mol
Tlist = np.array([20.,50.,100.,200.])

#for r in range(Nreplicas-1):
for r in range(1,2):

    for i,d in enumerate(dirs):
        E0,E1 = get_workvals_from_file(d,cols=(r,r+1))

        plt.hist(E0, bins=100,range=[-30.,20.], normed=True, color='b',histtype='step',linewidth=2)
        plt.hist(E1, bins=100,range=[-30.,20.], normed=True, color='g',histtype='step',linewidth=2)
        
        plt.text(-15., 0.25, r'$p^2(E)$', fontsize=20, withdash=False)
        plt.text(-8., 0.1, r'$p^3(E)$', fontsize=20, withdash=False)

        #wFdat = np.array(E0) * dBeta
        #wFdat = np.array(E0)
        #wRdat = np.array(E1) * (-dBeta)
        #wRdat = -np.array(E1) 

 
        plt.xlabel('E (kJ/mol)',fontsize=20)
        plt.ylabel('Probability Density',fontsize=20)
        


plt.savefig(outfile)
#plt.show()