import numpy as np
import pylab as plt
#from scripts.Cv_est import Cv_est
from landscape import MinimaDistribution

plt.xlabel('Temperature (K)')
plt.ylabel('Cv')
plt.title('Heat Capacity')
outfile="schematic-fig.png"


plt.rc('lines', linewidth=2)
plt.rc('axes', color_cycle=['b', 'g', 'r', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 20
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 15
plt.rcParams['legend.fancybox'] = False  
plt.rcParams['legend.fontsize'] = 12
plt.rcParams['axes.labelsize'] = 27 
plt.rcParams['xtick.labelsize'] = 15
plt.rcParams['ytick.labelsize'] = 15
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
#plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

systems = ["LJ31","LJ75"]
direc="/scratch/ab2111/tosinister/clusters/"  

minfiles = []
minfiles.append(direc+"LJ31/benchmarking/treservoir/min.data")
minfiles.append(direc+"LJ75/tspacing/hsaest_from_gmin_mindata/res/min.data")
kappas = [0.5*(3*31-6),0.5*(3*75-6)]
Tmins = [0.0125,0.02]
Tmaxs = [0.14,0.135]
ranges = [[0.01,0.06],[0.04,0.12]]

plt.figure(1)

for i,s in enumerate(systems):
    
    kappa = kappas[i]
    minfile = minfiles[i]
    Tmin = Tmins[i]
    Tmax = Tmaxs[i]
    plotrange=ranges[i]

    Nbins = 1000
    Tlist = np.arange(Tmin,Tmax,(Tmax-Tmin)/Nbins)
    
      
    rho = MinimaDistribution(Tmin,kappa=kappa,minfile=minfile,gas_constant=False,sort=True)
    Cv = []
    for t in Tlist:
        rho.T = t
        Cv.append(rho.Cv)
    #plt.plot(Tlist,Cv)
    Cvmin = int(round(np.min(Cv)/10)*10.)
    Cvmax = int(round(np.max(Cv)/10)*10.)
    dCv = (Cvmax-Cvmin)/5.
    #l = [round((Cvmin + j*dCv)/10)*10 for j in range(5)]
    if i==0: dCv = 10.
    else: dCv = 20.
    l = [Cvmin + j*dCv for j in range(5)]
    print l
    #plt.subplot(211,aspect=0.001)
    plt.subplot(2,2,i+1)
    plt.plot(Tlist,Cv)
    plt.xlim(plotrange)
    plt.xticks(np.arange(plotrange[0],plotrange[1],0.02))
    #plt.yticks(np.arange(Cvmin,Cvmax,dCv))
    plt.yticks(l)
    #plt.xticks(np.arange(0.02,0.07,0.02))
    if(i==0): plt.ylabel(r"$C_{\rm v}$")


files = []
#files.append(direc+"LJ31/tspacing/geom/results/PaccvT")
#files.append(direc+"LJ75/tspacing/geom_from_gmin_mindata/M12_res_at_transition/restart/results/PaccvT")
files.append(direc+"LJ31/tspacing/geom_at_transition/print/results/PaccvT")
files.append(direc+"LJ75/tspacing/geom_from_gmin_mindata/M12_res_at_transition/restart/results/PaccvT")

for i,s in enumerate(systems):

    plt.subplot(2,2,i+3)

    f =files[i]
    plotrange=ranges[i]
    Tlist,pacc = np.loadtxt(f,unpack=True)
    plt.plot(Tlist,pacc, '-o',ms=12, lw=2)
    
    if(i==0): plt.ylabel(r"$\langle P_{\rm acc} \rangle$")
    
    plt.xlabel("Temperature")
    plt.xticks(np.arange(plotrange[0],plotrange[1],0.02))
    plt.yticks(np.arange(0.1,0.22,0.05))
    plt.xlim(plotrange)
    plt.ylim(0.1,0.22)
    
plt.savefig(outfile)
#plt.show()
