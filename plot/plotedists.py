import numpy as np
import pylab as plt

plt.xlabel('Energy (kJ / mol)')
plt.ylabel('Probability')
plt.title('Replica Energy Distributions')
outfile="edists.png"


plt.rc('lines', linewidth=4)
plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 10
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 15
plt.rcParams['legend.fancybox'] = True  
plt.rcParams['legend.fontsize'] = 8
plt.rcParams['axes.labelsize'] = 10 
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

for i in range(0,16):

    v = np.loadtxt("puma_out"+str(i)+".xvg",usecols=([1]),skiprows=21)
    H = np.histogram(v,bins=100,normed=True)
    p,b = H
    bins = np.zeros(len(p))
    for i in range(len(p)): bins[i] = 0.5 * (b[i] + b[i+1])

    plt.plot(bins,p)

plt.xlim(-9000.,-5000)
plt.savefig(outfile)
#plt.show()
