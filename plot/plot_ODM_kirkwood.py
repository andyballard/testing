from testing.hsa_utils.utils import ODM
import numpy as np
from testing.analysis.ParallelTempering import get_workvals_from_file
import matplotlib.pyplot as plt

#plt.xlabel('Temperature (K)')
#plt.ylabel('Cv')
#plt.title('Heat Capacity')
outfile="10b_figure_overlap_test.pdf"


plt.rc('lines', linewidth=1)
plt.rc('axes', color_cycle=['b', 'g', 'r', 'y'])
plt.rc('font',**{'family':'sans-serif','sans-serif':['Arial']})
plt.rcParams['axes.titlesize'] = 20
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 14
plt.rcParams['legend.fancybox'] = False  
plt.rcParams['legend.fontsize'] = 12
plt.rcParams['axes.labelsize'] = 20 
plt.rcParams['xtick.labelsize'] = 16
plt.rcParams['ytick.labelsize'] = 16
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
#plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

# simulation details:
dir="/scratch/ab2111/plutoscratch/projects/kirkwood/TTodm/"
RREXdir = dir+"RREX_ener_T_20_50_100_200_kJmol/RREX_ener_T_20_50_100_200_kJmol_thinned.dat"
#RREXdir = dir+"RREX_ener_T_20_50_100_200_kJmol/short"
TREXdir = dir+"TREX_ener_T_20_50_100_200_kJmol/TREX_ener_T_20_50_100_200_kJmol_thinned.dat"
#dirs = [RREXdir,TREXdir]
dirs = [RREXdir,TREXdir]
titles = ["TREX","RREX"]
Nreplicas=4

# Temperature list in Kelvin  and kJ/mol
Tlist = np.array([20.,50.,100.,200.])
R=0.008314
kTlist = Tlist * R
import time
t = time.time()




#for r in range(Nreplicas-1):
for r in range(1,2):

    for i,d in enumerate(dirs):
        E0,E1 = get_workvals_from_file(d,cols=(r,r+1))

        # convert energies into work values
        dBeta = 1./kTlist[r+1]-1./kTlist[r]
        wFdat = np.array(E0) * dBeta
        #wFdat = np.array(E0)
        wRdat = np.array(E1) * (-dBeta)
        #wRdat = -np.array(E1) 

        #print E0,E1,dBeta    
        odm = ODM(wFdat,wRdat)
        #odm2 = ODM(wF2dat,wR2dat)

        if i==0:
            plt.plot(odm.LF[0]/dBeta,odm.LF[1],'-x',linewidth=2)
            plt.plot(odm.LR[0]/dBeta,odm.LR[1],'-x',linewidth=2)
            plt.plot(odm.deltaL[0]/dBeta,odm.deltaL[1],'-x',linewidth=2)
            
            plt.xlabel('E (kJ/mol)',fontsize=20)
            #plt.xlim([-30,30])
            plt.xticks(np.arange(-30,21,10),fontsize=20)
            plt.yticks(np.arange(-30,31,10),fontsize=20)
    
            plt.text(-23., -13., r'$L_2$', fontsize=20, withdash=False)
            plt.text(-23., 6., r'$L_3$', fontsize=20, withdash=False)
            plt.text(-25, 18., r'$\Delta L$', fontsize=20, withdash=False)

        a = plt.axes([.55, .55, .32, .32])
        plt.setp(a, xlim=[-24,-8],ylim=[17,21],xticks=np.arange(-24,-4,4), yticks=np.arange(17,22))
        plt.rcParams['xtick.labelsize'] = 10
        if i==0:
            a.plot(odm.deltaL[0]/dBeta,odm.deltaL[1],'-x',color='red')
        else:
            a.plot(odm.deltaL[0]/dBeta,odm.deltaL[1],'-o',markerfacecolor='none',markeredgecolor='blue',color='blue')
            #a.set_edgecolor('none')
        #a.plot(odm2.deltaL[0]/dBeta,odm.deltaL[1],'-x')
        plt.xlabel('E (kJ/mol)',fontsize=16)
        plt.ylabel('$\Delta L$',fontsize=16)
        
print "time", time.time()-t
        

"""

for i,direc in enumerate(direclist):

    results = Results(direc,Nreplist[i],recompute=False,reservoir=reslist[i],wdist=True)
    #results.plot_results()

    plt.subplot(211)
    plt.plot(np.arange(len(results.therm.Pacc)),results.therm.Pacc,'-o')        
    plt.subplot(212)
    plt.plot(results.therm.S[0],results.therm.S[1])
    #plt.plot(results.Tlist[:-1],results.therm.Cvlist,'-x')        
    print "reservoir acceptance: ",results.reservoir.Pacc
    #plt.plot(np.log(results.reservoir.Pwell[:,0]),'-o')        
    #plt.plot(results.Tlist[:-1],'-o')        
    
    #f = results.direc+"output.1"
from hsa_utils.utils import ODM
f="/scratch/ab2111/plutoscratch/projects/kirkwood/TTodm/RREX_ener_T_20_50_100_200_kJmol/RREX_ener_T_20_50_100_200_kJmol.dat"
Tlist = np.array([20.,50.,100.,200.])
R=0.008314
kTlist = Tlist * R
wFdat, wRdat = get_workvals_from_file(f,cols=(0,1))
wFdat = wFdat / kTlist[0]
wRdat = wRdat / kTlist[0]
odm = ODM(wFdat,wRdat)

print odm.LF
plt.plot(odm.LF[0],odm.LF[1])
#plt.plot(odm.deltaL[0],odm.deltaL[1])

plt.show()
exit()
    #dist = results.wdists[1]
    """


plt.savefig(outfile)
#plt.show()