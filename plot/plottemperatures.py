import numpy as np
import pylab as plt

plt.xlabel('Temperature index')
plt.ylabel('Temperature (reduced units)')
#plt.title('Replica Exchange Acceptance Probability')
#outfile="paccLJ31.png"
outfile="temperaturesLJ31.png"

plt.rc('lines', linewidth=2)
# plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 25
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 20
plt.rcParams['legend.fancybox'] = True  
plt.rcParams['legend.fontsize'] = 17
plt.rcParams['axes.labelsize'] = 15 
plt.rcParams['xtick.labelsize'] = 13
plt.rcParams['ytick.labelsize'] = 13
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

direc="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/"
#direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/"

#data = np.loadtxt(direc+"geom_from_gmin_mindata/M12_res_at_transition/restart/results/PaccvT")
#data2 = np.loadtxt(direc+"hsaest_from_gmin_mindata/M12_res_at_transition/restart/results/PaccvT")
#T= np.loadtxt(direc+"geom/temperatures")
#Thsa= np.loadtxt(direc+"hsaest/temperatures")
T= np.loadtxt(direc+"geom_at_transition/temperatures")
Thsa= np.loadtxt(direc+"hsaest_at_transition/temperatures")

plt.text(9.5,0.1,r"$T_{\rm geom}$",fontsize=25)
plt.text(10,0.055,r"$T_{\rm HSA}$",fontsize=25)
plt.text(8.5,0.021,r"$T^*$",fontsize=25)

x = range(1,13) 
#fig,ax = plt.subplots()
plt.plot(x,T,'-o', ms=10, lw=2, mfc='none',mec='black',mew=2,color='black')
plt.plot(x,Thsa,'-o',ms=10, lw=2, alpha=0.7, mfc='blue',color='blue')
y = [0.027 for xi in x]
plt.plot(x, y,'--',color="red") 
plt.yscale('log')
plt.xticks=np.arange(1,13)
plt.xlim([1,12])
plt.ylim([0.007,0.2])
#plt.legend(['geometric','HSA-enhanced'],loc=4)

# this is an inset axes over the main axes
a = plt.axes([.2, .55, .35, .32])
plt.ylim([0.9,1.1])
plt.xlim([1,12])
plt.plot(x,Thsa/T,'-o', lw=2, ms=10,mfc='blue',color='blue')
plt.xlabel("Temperature index")
plt.ylabel(r"$T_{\rm HSA} / T_{\rm geom}$")
#title('Probability')
plt.setp(a, xticks=np.arange(2,13,2), yticks=np.arange(0.9,1.1,0.1))



plt.savefig(outfile)
#plt.show()