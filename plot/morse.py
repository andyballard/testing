import numpy as np
import pylab as plt
#from scripts.Cv_est import Cv_est
from landscape import MinimaDistribution

plt.xlabel('Temperature (K)')
plt.ylabel('Cv')
plt.title('Heat Capacity')


plt.rc('lines', linewidth=2)
plt.rc('axes', color_cycle=['b', 'g', 'r', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 20
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 15
plt.rcParams['legend.fancybox'] = False  
plt.rcParams['legend.fontsize'] = 12
plt.rcParams['axes.labelsize'] = 20 
plt.rcParams['xtick.labelsize'] = 15
plt.rcParams['ytick.labelsize'] = 15
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
#plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

#kappa = 0.5*(3*581) #puma
#puma-wt???
#tetra-ala
#kappa = 0.5*(3*52)
#LJ31
#kappa = 0.5*(3*31-6)
#LJ75
kappa = 0.5*(3*6-6)

minfile="/home/ab2111/Desktop/min.data"  
#minfile ="/scratch/ab2111/tosinister/clusters/LJ31/benchmarking/treservoir/min.data"
  
#minfile ="/home/ab2111/projects/pacc_est/puma/min.dataall" 
#minfile="/scratch/ab2111/tosinister/trpzip/min.data"
#minfile="/scratch/ab2111/plutoscratch/projects/testing/input_files/min.data.lj75"
#minfile="/home/ab2111/projects/testing/input_files/min.data"
    
# Plot HSA estimation of Cvcurve between Tmin and Tmax
Tmin = 1.0e-08
Tmax = 4.0e-07
Nbins = 1000
Tlist = np.arange(Tmin,Tmax,(Tmax-Tmin)/Nbins)

  
rho = MinimaDistribution(Tmin,kappa=kappa,minfile=minfile,gas_constant=False,sort=True)
Cv = []
p0=[]
p1=[]
print np.shape(rho.Pwell)
from decimal import Decimal
depsilon=rho.E[1]-rho.E[0]
for t in Tlist:
    rho.T = t
    #p0.append(rho.Pwell[0])
    #p1.append(rho.Pwell[1])
    #Cv.append(Decimal(np.dot(rho.E**2,rho.Pwell)/(rho.T*rho.T))-Decimal(np.dot(rho.E,rho.Pwell)**2/(rho.T*rho.T)))
    
    Cv.append(kappa+(depsilon/t)**2*rho.Pwell[1]*rho.Pwell[0])
    #print np.dot(rho.E**2,rho.Pwell),np.dot(rho.E,rho.Pwell)**2    
    #print np.dot((rho.E/rho.T)**2,rho.Pwell)-np.dot(rho.E/rho.T,rho.Pwell)**2


    #print Decimal(Decimal(np.dot(rho.E**2,rho.Pwell))-Decimal(np.dot(rho.E,rho.Pwell)**2))
    #print Cv
    #print np.dot(rho.E**2,rho.Pwell)
    #print rho.Cv
#plt.plot(Tlist,p0)
#plt.plot(Tlist,p1)
plt.plot(Tlist,Cv)

plt.legend(["HSA estimation","PT: geometric","PT: hsa-enhanced"])

#plt.savefig(outfile)
plt.show()
