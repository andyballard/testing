import numpy as np
import pylab as plt

plt.xlabel('Temperature index',fontsize=25)
plt.ylabel(r'$\langle P_{\rm acc} \rangle$',fontsize=25)
#plt.title('Replica Exchange Acceptance Probability')
outfile="paccLJ31.png"
#outfile="paccLJ75.png"

plt.rc('lines', linewidth=2)
# plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 25
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 20
plt.rcParams['legend.fancybox'] = True  
plt.rcParams['legend.fontsize'] = 14
plt.rcParams['axes.labelsize'] = 20
plt.rcParams['xtick.labelsize'] = 13
plt.rcParams['ytick.labelsize'] = 13
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=2
#plt.rcParams['lines.linewidth']=0.5
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

direc="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/"
#direc="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/"

#data = np.loadtxt(direc+"geom_from_gmin_mindata/M12_res_at_transition/restart/results/PaccvT")
#data2 = np.loadtxt(direc+"hsaest_from_gmin_mindata/M12_res_at_transition/restart/results/PaccvT")
data= np.loadtxt(direc+"geom_at_transition/print/results/PaccvT")
data2= np.loadtxt(direc+"hsaest_at_transition/print/results/PaccvT")

r = range(1,12) 
#fig,ax = plt.subplots()
plt.plot(r,data[:,1],'-o',ms=15, lw=2, alpha=0.7, mfc='none',mec='black',mew=2,color='black')
plt.plot(r,data2[:,1],'-o',ms=15, lw=2, alpha=0.7, mfc='blue',color='blue')
x = np.arange(1.01, 11, 0.01) 
y = [0.22 for xi in x]
plt.plot(x, y,'--',color="red") 
plt.xlim([1,11])

plt.legend(['geometric','HSA-enhanced','target'],loc=3)

plt.savefig(outfile)
#plt.show()