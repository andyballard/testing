import numpy as np
import pylab as plt

plt.xlabel('time')
plt.ylabel('Replica index')
plt.title('Replica Exchanges')
outfile="exchanges_hsa.png"


plt.rc('lines', linewidth=2)
plt.rc('axes', color_cycle=['r', 'g', 'b', 'y'])
plt.rc('font',**{'family':'serif','serif':['Times']})
plt.rcParams['axes.titlesize'] = 10
plt.rcParams["text.usetex"] = True
plt.rcParams["font.size"] = 15
plt.rcParams['legend.fancybox'] = True  
plt.rcParams['legend.fontsize'] = 15
plt.rcParams['axes.labelsize'] = 10 
plt.rcParams['xtick.labelsize'] = 8
plt.rcParams['ytick.labelsize'] = 8
plt.rcParams['savefig.dpi'] = 300
plt.rcParams['figure.dpi'] = 150
plt.rcParams['lines.markersize']=1
plt.rcParams['lines.linewidth']=0.1
plt.rcParams['axes.linewidth']=0.75
plt.rcParams['patch.linewidth'] = 0.3

#dat = np.loadtxt("ptpacc",usecols=(0,4))
data= np.loadtxt("exchanges.permutations_geom_thin",usecols=(0,1))
plt.plot(data[:,0],data[:,1],'-',ms=2, lw=1, mfc='blue')

# plt.savefig(outfile)
plt.show()
