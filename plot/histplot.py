import matplotlib.pyplot as plt
import numpy as np

def less_than(dat,x):
    
    n = 0
    for d in dat:
        if d < x:
            n += 1.
    
    print n*1./len(dat)
    

col=4
#xmin=-0.01
#xmax=20.01
xmin=-10.
xmax=100.

dir="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/optimize_Tres/T"

files = []
sub = [".03",".05",".06",".07",".08"]
for s in sub:
    files.append(dir+s+"/w")

conditioned_dat = []
for f in files:
    dat = np.loadtxt(f,usecols=[col,6])
    for d,n in dat:
        if n>1:
            conditioned_dat.append(d)
    
    #plt.hist(dat[:0],bins=100,range=[xmin,xmax])
    plt.hist(conditioned_dat,bins=100,range=[xmin,xmax])
    #print less_than(dat,1000)
    
plt.legend(sub)
plt.show()