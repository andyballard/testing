import numpy as np

class Molecule(object):

    def __init__(self,natoms=None):
        self.residues = []
        self.atoms = []
        self.natoms = natoms
        
    def __repr__(self):
        
        string = "Residue information for Molecule:\n"
        for r in self.residues:
            
            string = string + " " + str(r.id)+" "+r.name + "\n"
            #print r.id, r.name
    
        return string
    
    def add_residue(self,residue):
        
        self.residues.append(residue)
        
        for a in residue.atoms:
            self.atoms.append(a)
        # update total number of atoms
        #self.natoms = max([a.id for a in residue.atoms])
    
class Residue(object):
    
    def __init__(self,id,resname,cap=False):
        
        self.atoms = []
        self.rgroup = []
        self.id = id
        self.name = resname
        self.C = None
        self.CA = None
        self.CB = None
        self.CG = None
        self.N = None
        
        self.cap = cap
        
        self.phi_atoms = []
        self.psi_atoms = []
        self.omega_atoms = []
        self.chi_atoms = []
        
    def __repr__(self):
        
        string = "Atom information for Residue:\n"
        for a in self.atoms:
            
            string = string + " " + str(a.id)+" "+a.name + "\n"
            #print r.id, r.name
    
        return string
    
    def add_atom(self,atom):
        
        self.atoms.append(atom)
        
        # save location of special backbone atoms
        if atom.name =="N":
            self.N = atom
        elif atom.name =="CA":
            self.CA = atom
        elif atom.name == "CB":
            self.CB = atom
        elif "CG" in atom.name and self.CG == None: #  this adds only one c-gamma atom to an amino acid, optionally labeled cg1 or cg2
            self.CG = atom
        elif atom.name == "C":
            self.C = atom
            if not self.cap: self.set_dihedral_atoms()
            
            # make list of rgroup indices    
            if self.N != None:    
                for i in [a.id for a in self.atoms]:
                    if i > self.N.id and i != self.CA.id and i < self.C.id:
                        self.rgroup.append(i)         

    def set_dihedral_atoms(self):
        
        #dirty hack: self.N.id-2 = id of C atoms of previous residue
        self.phi_atoms = [self.N.id-2,self.N.id,self.CA.id,self.C.id]
        self.psi_atoms = [self.N.id,self.CA.id,self.C.id,self.C.id+2]
        self.omega_atoms = [self.CA.id,self.C.id,self.C.id+2,self.C.id+4]
        if self.CB != None and self.CG != None:
            self.chi_atoms = [self.N.id,self.CA.id,self.CB.id,self.CG.id]

class Atom(object):
    
    def __init__(self,atnum,element,residue,resid):
        self.id = atnum
        self.name = element
        self.resname = residue
        self.resid = resid
        self.coords = None
            
class Group(object):
    
    def __init__(self,mol,atom1,atom2,amp,prob,addrgroup=False,overridegroup=None):
        
        self.mol = mol
        self.axisatoms = [atom1.id,atom2.id]
        self.atom1 = atom1
        self.atom2 = atom2
        self.amp = amp
        self.prob = prob
        self._addrgroup = addrgroup
        
        # this allows one to override the default that the first group atom listed is index atom2+1
        self.overridegroup = overridegroup
        
        # assumes all atoms with index largest than atom2 are rotated
        self.groupatoms = []
        self.set_groupatoms()

    @property 
    def addrgroup(self):
        #self.set_groupatoms()
        return self._addrgroup
          
    @addrgroup.setter
    def addrgroup(self,value):
        """ This ensures the rgroup list is updated whenever addrgroup is changed"""
        self._addrgroup = value

        self.set_groupatoms()
    
    def set_groupatoms(self):
        
              
        for i in range (self.atom2.id+1,self.mol.natoms+1):
            self.groupatoms.append(i)

        #if self._addrgroup==False:
        #    
        #    residue = self.mol.residues[self.atom2.resid-1]
        #    rgroup = residue.rgroup
        #    
        #    setgroup = set(self.groupatoms)-set(rgroup)
        #    new = [a for a in self.groupatoms if a in setgroup] 
        #    print new == self.groupatoms
        #    self.groupatoms = new
        
        #if self.atom2.id==327:
        #    for i in self.groupatoms:
        #        print i,
        #    exit()    
    
    def __repr__(self):
        
        s =  "GROUP "+str(self.atom1.resid)+"_"+str(self.atom1.resname)+"_"+str(self.atom1.name) +"_"+self.atom2.name +" "+ str(self.atom1.id) + " "+str(self.atom2.id) +" "+ str(len(self.groupatoms)) +" "+ str(self.amp) +" "+ str(self.prob) 
        s += "\n"
        #for a in self.groupatoms:
        #    s += str(a) + "\n"
        
        return s
        
class Dihedral_Group(Group):
    
    def __init__(self,angle,*args,**kwargs):
        
        self.angle = angle
        self.dihedralatoms = []
        Group.__init__(self,*args,**kwargs)
  
        if self.angle != None:
            self.process_dihedral()
        if self.angle == "phi":
            self.addrgroup = True
        elif self.angle == "psi":
            self.addrgroup = False
        elif self.angle == "oga":
            self.addrgroup = False  
        elif self.angle == "chi":
            self.addrgroup = False  
        
        self.override_group_selection()
        
    def override_group_selection(self):
        
        self.groupatoms = []
        
        if self.angle != "chi": 
            for i in range (self.dihedralatoms[2],self.mol.natoms+1):
                self.groupatoms.append(i)
        else:
            myresidue = self.mol.residues[self.atom1.resid-1]
            if myresidue.CB != None and myresidue.CG !=None:
                lastRgroupatom = myresidue.atoms[-3].id
                print self.dihedralatoms[2],lastRgroupatom
                for i in range (self.dihedralatoms[2],lastRgroupatom+1):
                    self.groupatoms.append(i)
           

        #if self._addrgroup==False:
        #    
        #    residue = self.mol.residues[self.atom2.resid-1]
        #    rgroup = residue.rgroup
        #    
        #    setgroup = set(self.groupatoms)-set(rgroup)
        #    new = [a for a in self.groupatoms if a in setgroup] 
        #    print new == self.groupatoms
        #    self.groupatoms = new

        #for i in self.groupatoms:
        #    print i,
        #exit()               
    
    def __repr__(self):
        
        #s =  "GROUP "+str(self.atom1.resid)+"_"+str(self.atom1.resname)+"_"+str(self.atom1.name) +"_"+self.atom2.name +" "+ str(self.atom1.id) + " "+str(self.atom2.id) +" "+ str(len(self.groupatoms)) +" "+ str(self.amp) +" "+ str(self.prob)  + " "+str(self.dihedralatoms[0])+" "+str(self.dihedralatoms[-1])
        s =  "GROUP "+str(self.atom1.resid)+"_"+str(self.atom1.resname) + "_"+str(self.angle) 
        s += " " + str(self.angle)
        for a in self.dihedralatoms:
            s += " " + str(a) 
        s += " "+ str(len(self.groupatoms)) +" "+ str(self.amp) +" "+ str(self.prob)
        s += "\n"
        for a in self.groupatoms:
            s += str(a) + "\n"
        
        return s
    
    def process_dihedral(self):

        #myresidue = self.mol.residues[self.atom2.resid-1]
        myresidue = self.mol.residues[self.atom1.resid-1]
        
        if   self.angle == "phi": self.dihedralatoms = np.copy(myresidue.phi_atoms)
        elif self.angle == "psi": self.dihedralatoms = np.copy(myresidue.psi_atoms)      
        elif self.angle == "oga": self.dihedralatoms = np.copy(myresidue.omega_atoms)      
        elif self.angle == "chi": self.dihedralatoms = np.copy(myresidue.chi_atoms)      
        else:
            print "Angle ", self.angle, "not understood"
            exit()
        #print "process> myresidue.phi_atoms",self.angle,self.dihedralatoms[0],self.dihedralatoms[-1]

          
def read_from_pdb(molecule,f):
        
    lines = open(f,"r")
    
    for li,line in enumerate(lines):

        atnum = int(line.split()[1])
        element = line.split()[2]
        resname = line.split()[3]
        resid = int(line.split()[5])
        atom = Atom(atnum,element,resname,resid)
        
        if li == 0:
            residue = Residue(atom.resid,atom.resname,cap=True)            
        #elif li == len(lines):
        #    molecule.add_residue(residue)
        #    residue = Residue(atom.resid,atom.resname)        
        elif atom.resid != residue.id:
            
            molecule.add_residue(residue)
            cap=False
            if atom.name == "NME": cap=True
            residue = Residue(atom.resid,atom.resname,cap=cap)
        
        residue.add_atom(atom)
    
    molecule.add_residue(residue)


def main():

    mol = Molecule(natoms=581)
    read_from_pdb(mol,"/scratch/ab2111/plutoscratch/projects/puma/ref0.pdb")
    #read_from_pdb(mol,"/scratch/ab2111/plutoscratch/projects/puma/ref.pdb")
    #print mol.residues[2].atoms[0].id
    #g = make_psi_group(mol,mol.residues[1],mol.residues[2])
    #amp=0.4
    #prob=1.0
    amp=1.0
    prob=0.01 # 0.015 will give 1 move per step on average
        
    groups = []
    for i in range(1,35):
    
        
    #for i in range(20,21):
        #angle="phi"
        #amp=-60./180
        #groups.append(Dihedral_Group(angle,mol,mol.residues[i].N,mol.residues[i].CA,amp,prob))
        #angle="psi"
        #amp=-45./180
        #groups.append(Dihedral_Group(angle,mol,mol.residues[i].CA,mol.residues[i].C,amp,prob))
        #angle="oga"
        #groups.append(Dihedral_Group(angle,mol,mol.residues[i].C,mol.residues[i+1].N,amp,prob))
        myres = mol.residues[i]
        if myres.CB != None and myres.CG !=None:
            angle="chi"
            groups.append(Dihedral_Group(angle,mol,mol.residues[i].CA,mol.residues[i].CB,amp,prob))

        #groups.append(Group(mol,mol.residues[i].N,mol.residues[i].CA,amp,prob,addrgroup=True))
        #groups.append(Group(mol,mol.residues[i].CA,mol.residues[i].C,amp,prob,addrgroup=False))
    for g in groups:    print g
    
    #res = mol.residues[5]
    #print res.name,res.id,res.phi_atoms,res.psi_atoms
    #np.savetxt("atomgroups-new",g)

if __name__=="__main__":
    pass
    #main()