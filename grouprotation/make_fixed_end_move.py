from make_gr import *

mol = Molecule(natoms=581)
read_from_pdb(mol,"/scratch/ab2111/plutoscratch/projects/puma/ref0.pdb")
#read_from_pdb(mol,"/scratch/ab2111/plutoscratch/projects/puma/ref.pdb")
#print mol.residues[2].atoms[0].id
#g = make_psi_group(mol,mol.residues[1],mol.residues[2])
#amp=0.4
#prob=1.0
amp=0.1
prob=0.01 # 0.015 will give 1 move per step on average

groups = []

CAlist = []
for r in mol.residues:
    for a in r.atoms:
        if a.name=="CA":
            CAlist.append(a)
        #print a.id,a.name
    #print r.CA.name

for i in range(len(CAlist)-4):
    atom1 = CAlist[i]
    atom2 = CAlist[i+4]
    groupatoms = np.arange(atom1.id+1,atom2.id,1)
    s =  "GROUP "+str(atom1.resid)+"_"+str(atom1.resname)+"_"+str(atom1.name) +"_"+atom2.name +" "+ str(atom1.id) + " "+str(atom2.id) +" "+ str(len(groupatoms)) +" "+ str(amp) +" "+ str(prob) 
    print s
    
    #for a in groupatoms:
    #    print a
    #print     