import numpy as np
#from Pacc_estimation import Minima
import matplotlib.pyplot as plt
from pele.thermodynamics import free_energy
from collections import defaultdict
import hsa_utils.utils as utils

class Minima(object):
    
    def __init__(self,kappa=None,E=None,vib=None,pgo=None,minfile=None,pointsfile=None,sort=False,redundancy_check=True,gas_constant=False,helicity=False,maxminima=None):

        self.kappa = kappa
        self.R = 1.0
        
        self.maxminima = maxminima
        
        if gas_constant:
            self.R = 0.001987 # kcal / mol K conversion        
        
        if minfile is not None:
            try: 
                #data = np.loadtxt("/home/ab2111/projects/pacc_est/puma/min.data")
                #print minfile
                data = np.genfromtxt(minfile)
                if self.maxminima != None:
                    data = data[0:self.maxminima]
                    
                heldata = np.genfromtxt("/scratch/ab2111/plutoscratch/projects/pacc_est/puma/min.data.helix")
                #heldata = np.genfromtxt("/home/ab2111/projects/pacc_est/puma/min.data.helix")
                #data = np.genfromtxt("/home/ab2111/projects/pacc_est/puma/min.data")
                #data = np.loadtxt("/home/ab2111/projects/testing/input_files/Tala-min-amber.data") 
            except:
                print "Minfile: ", minfile, "unable to be read"
                exit()
        
            data = np.array(data[0:1000])            
            if sort:
                data = np.array(sorted(data,key=lambda x: x[0]))
            if redundancy_check:
                data = self.check_for_redundancies(data)
            # use only first Nwells
            #Nwells = 70
            #data = data[:Nwells]
            # energy set to units of Kelvin
            self.E = data[:,0]/self.R
            self.vib = data[:,1]
            self.pgo = data[:,2]
            try:
                self.Tindex = data[:,6]
                self.runindex = data[:,7]
                if helicity:
                    self.hel = data[:,8]
            except:
                if helicity:
                    self.hel = data[:,8]
            
            self.helE =heldata[0]/self.R
            self.helvib = heldata[1]
            self.helpgo = heldata[2]
            # sort energy vib pgo lists by Temperature index:
            

            #print self.E
            #print self.vib
                
        elif np.array([E,vib,pgo]).all():
            self.E = E
            self.vib = vib
            self.pgo = pgo
            
        else:
            print "Error: no minimum data selected";
            exit()
            
        if pointsfile is not None:
            self.points = np.genfromtxt(pointsfile)
            Nminima = len(self.E)
            length = len(self.points.flatten())
            self.points.resize((Nminima,length/Nminima))
         

    def check_for_redundancies(self,data):
        
        return data
        #ncols = 3
        
        #E_fvib_pgo = dat[:,:3]
        
        #for c in range(ncols)
   
class MinimaDistribution(Minima):
    
    def __init__(self,Temperature,*args,**kwargs):
        """ minima = minima object"""

        Minima.__init__(self,*args,**kwargs)
        #self.minima = Minima()
        self._T = Temperature
        self.Pwell = utils.calcPw(self.E, self.pgo, self.vib, self.T, self.kappa)
        self.CDFwell = utils.calcCDF(self.Pwell)
        self.Eave = utils.calcEave(self.T,self.Pwell,self.E,self.kappa)
        self.Cv = utils.calcCv(self.T,self.Pwell,self.E,self.kappa)
        
    @property
    def T(self):        
        return self._T    
    
    @T.setter
    def T(self,value):
        """ Update distribution objects whenever T is updated """
        
        self._T = value
        
        self.Pwell = utils.calcPw(self.E, self.pgo, self.vib, self.T, self.kappa)
        self.CDFwell = utils.calcCDF(self.Pwell)
        self.Eave = utils.calcEave(self.T,self.Pwell,self.E,self.kappa)
        self.Cv = utils.calcCv(self.T,self.Pwell,self.E,self.kappa)

    
    
class MinimaPlots(MinimaDistribution):    

    def __init__(self,*args,**kwargs):
        """ minima = minima object"""

        MinimaDistribution.__init__(self,*args,**kwargs)

        #self.minima = Minima()
   
    def plot_cdf(self,plot=False):
    
        plt.plot(self.CDFwell,'-')
    
    def num_to_cdfval(self,cdfval,sort=False):
        
        p0 = self.Pwell
        if sort:
            p0 = np.sort(p0)
        p0 = p0[::-1]
        print p0
    
        c=0.
        n = 0
        for p in p0:
            c = c + p
            n = n + 1
            if c > cdfval:
                break
        
        return n
    
        
    def plot_pwell(self):
        
        p0 = self.Pwell
        
        plt.plot(np.log(p0),'-')
    
    def plot_FETS(self,makeplot=True):
    
        F,E,mTS = utils.get_F_E_TS(self.E,self.pgo,self.vib,self.T,self.kappa)
        print self.E[0],self.E[0]/self.T
        #plt.plot(F/T-np.min(F)/T,'x')
        if makeplot:
            plt.plot(np.sort(F)/self.T-np.min(F)/self.T,'x-')
        #s = sorted(range(len(F)), key=lambda k: F[k])
        #for i in s[:15]:
        #    print i,F[i]/self.T-min(F)/self.T
        #return s[:15]
        #plt.plot(s,'x-')
        #plt.plot(E/T,-mTS/T,'x')
        #print mTS
        #plt.plot(mTS/T,'-') # mTS = -TS
        #plt.legend(["F","E","-TS"])

def plot_wellnvT(self,minima,Tlist,Nlist):
        
    leg=[]
    print "Nlist",Nlist
    for n in Nlist:
        pn = []
        for t in Tlist:
            p0 = utils.calcPw(self.E,self.pgo,self.vib,T=t,kappa=self.kappa)
            pn.append(p0[n])
        plt.plot(Tlist,pn,'-')
        leg.append("n="+str(n))
    #calculate contribution from rest of minima v T
    prestall=[]
    for t in Tlist:
        p0 = utils.calcPw(minima.E,minima.pgo,minima.vib,T=t,kappa=minima.kappa)
        prest = 0.
        for n in Nlist:
            prest += p0[n]  
        prest = 1. - prest
        prestall.append(prest)
    plt.plot(Tlist,prestall,'-')
    leg.append("remaining")
    #plt.yscale('log')
    plt.legend(leg,loc=2)
        
def calc_relative_entropy(minima,T1,T2):
    """ minima = minima object"""
    
    p1 = utils.calcPw(minima.E,minima.pgo,minima.vib,T=T1,kappa=minima.kappa)
    p2 = utils.calcPw(minima.E,minima.pgo,minima.vib,T=T2,kappa=minima.kappa)

    D = 0.
    for i,pi in enumerate(p1):
        D += pi * np.log(pi/p2[i])
        
    return D

def find_dominant_wells(minima,T1,T2):
    
    p1 = utils.calcPw(minima.E,minima.pgo,minima.vib,T=T1,kappa=minima.kappa)
    p2 = utils.calcPw(minima.E,minima.pgo,minima.vib,T=T2,kappa=minima.kappa)

    # sort by decreasing p1:
    s = sorted(range(len(p1)), key=lambda k: p1[k])
    s = s[::-1]

    ratio = []
    for si in s:
        r = p2[si]/p1[si]
        ratio.append((r,p1[si],minima.hel[si]))
    #for i,pi in enumerate(p1):
    #    ratio=p2[i]/pi
    #    if ratio > 2. and p2[i]>0.01:
    #        print T1,T2,p2[i],ratio,minima.Tindex[i],minima.Runindex[i]
        

    return ratio

def main():

    #puma
    #kappa = 0.5*(3*581)
    #minfile ="/home/ab2111/projects/pacc_est/puma/min.dataall" 
    #minfile="/scratch/ab2111/tosinister/puma/quench-from-ycc-sim/minfile_deprecated/min.dataall"
    #minfile="/scratch/ab2111/tosinister/puma/mindata-from-ycc-sim-11000structures/min.data.new"
    #minfile="/scratch/ab2111/tosinister/puma/quenched-from-ycc/min.data"
    #minfile="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest_from_gmin_mindata/res/min.data"
    #pointsfile="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest_from_gmin_mindata/res/points.min"
    kappa = 0.5*(3*31-6)
    minfile="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/hsaest/min.data"
    pointsfile=None
    #minfile="/scratch/ab2111/tosinister/puma/quench-from-amber-pt/quench3/min.data"

    #puma
    #Tmin=250.
    #Tmax=476.
    N=10

    0.0267
    #lj75
    #kappa = 0.5*(3*75-6)
    #kappa = 3*75-6
    Tmin=0.015
    Tmax=0.039

    cvlist=[]
    etalist=[]
    f1 = []
    f0 = []
    f2 = []
    f3 = []
    f4 = []
    #mdist = MinimaPlots(Tmin,kappa=kappa,minfile=minfile,sort=False,gas_constant=False)
    Tlist = np.arange(Tmin,Tmax,0.0005)
    for T in Tlist:
    #Tpt=0.0267
        mdist = MinimaDistribution(T,kappa=kappa,minfile=minfile,pointsfile=pointsfile,sort=False,gas_constant=False)
        print mdist.E
        Elist = mdist.E
        Pw = mdist.Pwell
        Eave = np.average(Elist,weights=Pw)
        print np.shape((Elist-Eave)**2)
        var = np.dot(Pw,(Elist-Eave)**2)
        skew = np.dot(Pw,(Elist-Eave)**3)
        g = skew / var**(1.5)
        #print Pw
        #print skew, var, g*np.sqrt(var)
        #exit()
        #eta = g*0.5*np.sqrt(var)/T
        #etalist.append(eta)
        #f0.append(skew)
        f1.append(g*np.sqrt(var))
        #f2.append(2*T/np.sqrt(var))
        #f4.append(var)
        #f3.append(g*np.sqrt(var))
    plt.plot(Tlist,f1,'-x')
    #plt.plot(Tlist,f2,'-x')
    #plt.plot(Tlist,f0,'-x')
    plt.plot(Tlist,2*Tlist,'-x')
    #plt.plot(Tlist,f1,'-x')
    #plt.plot(Tlist,f4,'-x')
    #plt.plot(Tlist,etalist,'-x')
    #plt.plot(Tlist,[1. for x in Tlist])
    #plt.plot(np.log(mdist.Pwell/mdist.Pwell[0]),'-x')
    plt.legend(["g*sig","2*T"])
    #plt.legend(["g*sig","g","sig"])
    plt.show()    
    exit()
    
    #p = Pacc_estimation.Paccest_Iteration(kappa=kappa,minfile=minfile,sort=False,gas_constant=True)

    #find_dominant_wells(p,Tmin,350.)
    #find_dominant_wells(p,350.,Tmin)

    #Tlist = np.linspace(Tmin,Tmax,N)
    #Tlist = [0.073370783873]
    #Tlist = np.array([250.,320.,450.,500.,600.])
    #Tlist = np.array([250.,300.,350.])
    #Tlist = np.array([320.])

    #Nlist=[6,923,1126,1162,]
    #Nlist=[46,271,355,1100]
    #plot_wellnvT(p,np.arange(300.,700.,10.),Nlist)
    cvlist = []
    for t in Tlist:
        #pass
        #plot_cdf(p,t)
        #mdist.updateTemperature(t)
        mdist.T = t
        cvlist.append(mdist.Cv)
        #plot_FETS(p,t)
        #plt.plot(Tlist,np.array(cvlist)-kappa)
        #plt.plot(np.log(mdist.Pwell))
    plt.plot(Tlist,cvlist)
    #plt.plot(p.vib-114000)
    #plt.legend(Tlist.astype(str),loc=2)
    #print p.E[0]
    plt.show()

if __name__=="__main__":
    pass
    #main()
