import numpy as np
import pele.thermodynamics as pele
import sys
from math import *
from pele.optimize import mylbfgs as opt 
from scipy.optimize import fmin_l_bfgs_b
from scipy.interpolate import interp1d
from scipy import interpolate
from collections import defaultdict

import matplotlib.pyplot as plt

import hsa_utils.utils as utils
from pele.thermodynamics import free_energy
from landscape import Minima, MinimaDistribution

                        
class TSpacingOptimizer(MinimaDistribution):

    def __init__(self,*args,**kwargs):
        
        MinimaDistribution.__init__(self,*args,**kwargs)

        self.mdist_pert = MinimaDistribution(*args,**kwargs)
        # LJ31 parameters:
#        self.kappa = 87*0.5
        self.g = 1.18331
        
        #self.kappa = kappa
        self.ptarget = 0.22

        self.ExactGammaAve = False

        self.asymm = None

        self.Tsequence = []
        self.asymmsequence = []
    
        self.T_nonoptimized_sequence = []
        
    def calc_nonoptimized_Tsequence(self,N=10,Nreverse=0):
        """
        Find desired temperature progression assuming constant heat capacity = kappa
        uses mpmath module to compute inverse error function to find desired gamma := T_{i+1}/T_i
        """
        # module that can calculate inverse error function
        from mpmath import erfinv
        
        # compute inverse error function of x = \sqrt(kappa/2) * (gamma-1) / \sqrt(\gamma^2+1)
        x = erfinv(1-self.ptarget)
        x = float(x)

        # coefficients of polynomial in gamma: gamma^2 * p[0] + gamma * p[1] + p[2] = 0
        p_coeffs = [1-(2./self.kappa)*x*x,-2,1-(2./self.kappa)*x*x]
        roots = np.roots(p_coeffs)
        # find solution for gamma > 1 s/t T_{i+1}>T_i
        for r in roots:
            if r > 1.0: 
                gamma = r
                break
        
        self.T_nonoptimized_sequence.append(np.copy(self.T))    
        
        #copy into gamma
        for n in range(1,N+1):
            self.T_nonoptimized_sequence.append(self.T*gamma**n)
            
        for n in range(1,Nreverse+1):
             self.T_nonoptimized_sequence.insert(0,self.T*gamma**(-n))
           
        for i,t in enumerate(self.T_nonoptimized_sequence):
            print t

    
    def calcPaccest(self):

        Ti = self.T
        Tc = self.mdist_pert.T
        
        E = self.E
        if self.ExactGammaAve == True : kappa = self.kappa-1
        else: kappa = self.kappa

        #Pi = utils.calcPw(self.E,self.pgo,self.vib,T=Ti,kappa=self.kappa)
        #Pc = utils.calcPw(self.E,self.pgo,self.vib,T=Tc,kappa=self.kappa)
        Pi = self.Pwell
        Pc = self.mdist_pert.Pwell

        deltaT = Tc - Ti
        gamma = Tc/Ti

        Pacc_est = 0.0
        self.asymm = 0.0

        for w in range(len(E)):
            if Pi[w] > 0.00001:
                for o in range(len(E)):
                    if Pc[o] > 0.00001:
                        deltaV =E[o]-E[w]
                
                        well_conditioned_acc_prob = self.calcPacc_per_well_pair(deltaV,kappa,gamma,deltaT)
                        Pacc_est = Pacc_est + Pi[w] * Pc[o] * well_conditioned_acc_prob
                        
                        # calculate value of "asymmetry" variable
                        self.asymm = self.asymm + Pi[w] * Pc[o] * deltaV/(kappa*deltaT)
                        #if Pi[w]*Pc[o]*pacc[w,o] > 0.01:
                        #    print w,o,pacc[w,o]*Pi[w]*Pc[o],erfc(chi0/sqrt(2))*Pi[w]*Pc[o]

        return Pacc_est

    def calcPacc_per_well_pair(self,deltaV,kappa,gamma,deltaT):
        """ Well-conditioned acceptance probability of REX move"""
        
        chi0=sqrt(kappa)*(gamma-1)*sqrt(1./(gamma**2+1))
        chi = chi0 * (1+deltaV/(kappa*deltaT))
        
        return erfc(chi/sqrt(2))
        

    def getCostFunction(self,Tc):

        # update Temperature of perturbed distribution
        self.mdist_pert.T = Tc
        
        paccest = self.calcPaccest()
        
        # Harmonic cost function with zero at target acceptance
        Cost = 0.5 * ((paccest-self.ptarget)**2)

        # Gradient of cost function
        Grad = self.calcCostGradient()
        
        return Cost, Grad


    def calcCostGradient(self):
        
        Ti = self.T
        Tc = self.mdist_pert.T
                
        E = self.E
        kappa = self.kappa
        #Pi = utils.calcPw(self.E,self.pgo,self.vib,T=self.T,kappa=self.kappa)
        Pi = self.Pwell
        #Pc = utils.calcPw(self.E,self.pgo,self.vib,Tc,kappa=self.kappa)
        Pc = self.mdist_pert.Pwell

        deltaT = Tc - Ti
        gamma = Tc/Ti
            
        dPaccdT = 0.0
            
        paccest = self.calcPaccest()

        chi0=sqrt(kappa)*(gamma-1)*sqrt(1./(gamma**2+1))
        dchi0dT = (sqrt(kappa) / Ti) * ((gamma+1)/(gamma**2+1)**(1.5))

        # average V for Tc replica needed for derivative below
        aveVc = self.mdist_pert.Eave
        #aveVc = self.calcEave(Tc)

        for w in range(len(E)):
            if Pi[w] > 0.00001:
                for o in range(len(E)):
                    if Pc[o] > 0.00001:
                        deltaV =E[o]-E[w]
                    
                        chi = chi0 * (1+deltaV/(kappa*deltaT))

                        dchidT = dchi0dT*(1+deltaV/(kappa*deltaT)) - chi0*deltaV/(kappa*deltaT**2)

                        dPaccdT = dPaccdT + Pi[w]*Pc[o]*(-(2./sqrt(pi))*exp(-(chi**2)/2)) * dchidT/sqrt(2)

                        #add term that calculates well probabilities
                        #pacc[w,o]=erfc(chi/sqrt(2))
                        well_conditioned_acc_prob = self.calcPacc_per_well_pair(deltaV,kappa,gamma,deltaT)

                        dPcdT = Pc[o]*(aveVc-self.kappa*Tc - E[o])  / Tc**2  

                        dPaccdT = dPaccdT + Pi[w]*well_conditioned_acc_prob*dPcdT
                

        gradE = (paccest-self.ptarget) * dPaccdT

        return gradE
    
    def pot(self,Tc):

        E = self.E
        kappa = self.kappa
        Ti = self.T

        paccest = self.calcPaccest(Tc)
        Pi = utils.calcPw(self.E,self.pgo,self.vib,T=self.T,kappa=self.kappa)
        Pc = utils.calcPw(self.E,self.pgo,self.vib,Tc,kappa=self.kappa)
        Energy = 0.5 * ((paccest-self.ptarget)**2) #+ 1000.0 * (1-np.sign(Tc-Ti))

        deltaT = Tc - Ti
        gamma = Tc/Ti
            
        dPaccdT = 0.0
            
        pacc = np.zeros(shape=(len(E),len(E)))

        # average V for Tc replica needed for derivative below
        aveVc = self.calcEave(Tc)

        for w in range(len(E)):
            for o in range(len(E)):

                deltaV =E[o]-E[w]

                chi0=sqrt(kappa)*(gamma-1)*sqrt(1./(gamma**2+1))
                chi = chi0 * (1+deltaV/(kappa*deltaT))

                dchi0dT = (sqrt(kappa) / Ti) * ((gamma+1)/(gamma**2+1)**(1.5))
                dchidT = dchi0dT*(1+deltaV/(kappa*deltaT)) - chi0*deltaV/(kappa*deltaT**2)

                dPaccdT = dPaccdT + Pi[w]*Pc[o]*(-(2./sqrt(pi))*exp(-(chi**2)/2)) * dchidT/sqrt(2)

                #add term that calculates well probabilities
                pacc[w,o]=erfc(chi/sqrt(2))

                dPcdT = Pc[o]*(aveVc-self.kappa*Tc - E[o])  / Tc**2  

                dPaccdT = dPaccdT + Pi[w]*pacc[w,o]*dPcdT
                

        gradE = (paccest-self.ptarget) * dPaccdT

        return Energy, gradE


    def FindOptimalTemperature(self):

        #self.mdist_pert.T = np.copy(self.T)*(1.+0.001)
        self.mdist_pert.T=np.copy(self.T)*(1.0001)
        Tc = np.copy(self.mdist_pert.T)
        
        #ret = fmin_l_bfgs_b(self.getCostFunction,[Tc],bounds=[(Ti+0.0001,self.g*self.g*Ti)],iprint=1,factr=1e2)
        #print self.mdist_pert.T
        #Tc = np.copy(self.T)*(1.0001)
        ret = fmin_l_bfgs_b(self.getCostFunction,[Tc],bounds=[(self.T+0.0001,self.g*self.g*self.T)],iprint=0,factr=1e2)
        #ret = fmin_l_bfgs_b(self.getCostFunction,Tc,bounds=[(self.T+0.0001,self.g*self.g*self.T)],iprint=0,factr=1e2)

        return ret


    def iterate(self,N=2):

        self.Tsequence.append(np.copy(self.T))
        
        for i in range(N-1): 
            
            # update current "low replica" temperature to previous high one
            self.T=self.Tsequence[i]
            
            # find optimized "high replica" temperature
            tmin=self.FindOptimalTemperature()
            
            #print tmin[0],self.mdist_pert.T
            self.Tsequence.append(list(tmin[0])[0])
            self.asymmsequence.append(self.asymm)
            
            #Tcmin[i+1]=tmin[0]
            print self.Tsequence[i:i+2]

        print "optimized temperature sequence for pacc_target = ",self.ptarget
        for t in self.Tsequence:
            print t

    """
        alast=0
        Ti=T0
        Tc=Ti+0.001
        while(Tc<Ti+0.020):
        #while(Tc<Ttrue*5):
            alist = pot(Tc)
            a=alist[0]
            print a,alist[1],(a-alast)/0.0001,Tc#,(Tc-Ttrue)/Ttrue
            Tc = Tc+0.0001
            alast = a
    """
    def paccest_estimation_from_Tlist(self,Tlist):

        self.Tsequence.append(np.copy(self.T))
        paccest = []
        
        for i in range(len(Tlist)-1): 
            
            # update current "low replica" temperature to previous high one
            self.T=Tlist[i]
            self.mdist_pert.T = Tlist[i+1]
            
            paccest.append(self.calcPaccest())

        return paccest
    
class TSpacingOptimizer_Fromlist(TSpacingOptimizer):
    
    def __init__(self,*args,**kwargs):

        super(TSpacingOptimizer,self).__init__()
        
    def calcCvfrominterp(self,T):

        Cv = interpolate.splev(T,self.f,der=0)
        Cvprime = interpolate.splev(T,self.f,der=1)
#             Cv = self.f(T)
#             Cvprime = 1.0

        return Cv, Cvprime

    def calcCvfromlist(self,T):

        i=0
        Tupper = self.Tlist[i]
        while Tupper < T: 
            i=i+1
            Tupper = self.Tlist[i]

        deltaT = self.Tlist[i]-self.Tlist[i-1]
        deltaCv = self.Cvlist[i]-self.Cvlist[i-1]
        x = Tupper - self.Tlist[i-1]

        Cv = self.Cvlist[i-1] + x * deltaCv

        return Cv
        
    def calcCostfromlist(self,Tc):

        E = self.E
        kappa = self.kappa
        Ti = self.T

        dat = self.calcPaccestfromlist(Tc)
        p = dat[0]
        pprime = dat[1]

        C = 0.5 * ((p-self.ptarget)**2) 
        Cprime = (p-self.ptarget) * pprime

        return C, Cprime

    def calcPaccestfromlist(self,Tc):

#         Ts = Tc[0]
        # calc Pacc
        Ts = Tc
        # calc paccest
        deltaT = Ts-self.T
        Vavei = self.calcEave(Ts)
        Vavec = self.calcEave(Ts)
        deltaVave = Vavec-Vavei
        Cvidat = self.calcCvfrominterp(self.T)
        Cvi = Cvidat[0]
        Cvcdat = self.calcCvfrominterp(Ts)
        Cvc = Cvcdat[0]
        dCvcdT = Cvcdat[1]

        delta = self.kappa*deltaT + deltaVave
        r = sqrt(Cvi*self.T**2 + Cvc*Ts**2)
        Pacc = erfc(delta/(sqrt(2)*r)) 

        # calc dPacc/dT 
        dPaccdT = 1. - (delta)/(2*r**2) * (2*Ts + Ts**2 * dCvcdT/Cvc)
        dPaccdT = dPaccdT * Cvc/(sqrt(2)*r) * (- exp(-delta**2/(2*(r**2))))

        return Pacc, dPaccdT

    def calcPaccest_ww(self,Tlow,Tc,w,o):

        Ti = Tlow
        E = self.E
        kappa = self.kappa
        deltaT = Tc - Ti
        gamma = Tc/Ti
        Pacc_est = 0.0
        pacc = np.zeros((len(E),len(E)))

        Pi = utils.calcPw(self.E,self.pgo,self.vib,Ti)
        Pc = utils.calcPw(self.E,self.pgo,self.vib,Tc)

        deltaV =E[o]-E[w]

        chi0=sqrt(kappa)*(gamma-1)*sqrt(1./(gamma**2+1))
        chi = chi0 * (1+deltaV/(kappa*deltaT))
        pacc[w,o]=erfc(chi/sqrt(2))

        #return Pi[w]*Pc[o]*pacc[w,o]
        return pacc[w,o]

    def TSpacingOptimizerfromlist(self,Tlow):

        Tc = Tlow*(1.+0.001)
        self.T = Tlow
        g = self.g
        
#         Pi = self.calcPw(Tlow)
#         print "I am here", Tc,Tlow
#         ret = fmin_l_bfgs_b(self.pot,[Tc],bounds=[(Tlow+0.0001,g*g*Tlow)],iprint=1,factr=1e6)
        print "TSpacingOptimizerfromlist> Tc", Tc
#         ret = fmin_l_bfgs_b(self.calcCostfromlist,[Tc],bounds=[(Tlow+0.0001,g*g*Tlow)],iprint=0,factr=1e2)
        a = Tc
        ret = fmin_l_bfgs_b(self.calcCostfromlist,[a],bounds=[(Tlow+0.0001,g*g*Tlow)],iprint=1,factr=1e2)

#         return ret[0]
        return ret
    
    def paccest_list(self):
        
        temps=np.loadtxt("temperatures")
        pest = np.ndarray(shape=len(temps))
        for i in range(len(temps)-1):
            self.T = temps[i] 
            Tc = temps[i+1]
            #print self.T, self.calcPaccest(Tc)
            pest[i] = self.calcPaccest(Tc)

        return (temps,pest)
    
def main():
    import matplotlib.pyplot as plt

    #kappa = 0.5*(3*581) #puma
    #kappa = 0.5*(3*52) #tetra-ala
    kappa = 0.5*(3*31-6) #LJ31
    minfile="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/min.data"

    #minfile="/home/ab2111/projects/testing/input_files/min.data"
    #minfile="/scratch/ab2111/plutoscratch/projects/testing/input_files/min.data.lj75"
    #minfile="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest/res/min.data"
    
    #kappa = 0.5*(3*75-6) #LJ75
    #minfile="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest_from_gmin_mindata/res/min.data"
    
    #minfile="/scratch/ab2111/tosinister/clusters/LJ75/get_minima_info/optim_new/min.data.all"
    #minfile="/home/ab2111/min.data.LJ75.david"
    #minfile="/scratch/ab2111/tosinister/puma/mindata-from-ycc-sim-11000structures/min.data.new"
    #p = TSpacingOptimizer(kappa=kappa,minfile=minfile)
    
    #p = TSpacingOptimizer(0.08208352465,kappa=kappa,minfile=minfile,gas_constant=False,sort=True,maxminima=100)
    p = TSpacingOptimizer(0.007391,kappa=kappa,minfile=minfile,gas_constant=False,sort=True,maxminima=100)
    
    #Tlist = np.loadtxt("/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest/res/temperatures")
    #Tlist = np.loadtxt("/scratch/ab2111/tosinister/clusters/LJ75/tspacing/hsaest_from_gmin_mindata/res/T_est")
    #pacc = p.paccest_estimation_from_Tlist(Tlist)
    #plt.plot(pacc,'-x')
    #plt.ylim(0.12,0.4)
    #plt.show()
    #exit()
    #Tlist = [0.002,0.005]
    
    #import math
    #for t in Tlist:
    #    p.mdist_pert.T = t
    #    gamma = t/0.00125
    #    pacc = p.calcPaccest()
    #    print t, pacc, 1.-math.erf(np.sqrt(kappa*0.5)*(gamma-1)*1./(np.sqrt(1+gamma**2)))
    #print p.E
    #p.Ti = 300.
    #p.calc_nonoptimized_Tsequence(N=6,Nreverse=5)
    #exit()
    p.iterate(N=12)
    
    exit()
    
    Cvlist = []
    Tlist = np.arange(0.0125,0.15,0.0001)
    #for t in p.Tsequence:
    for t in Tlist:
        p.T = t
        Cvlist.append(p.Cv)    
    plt.plot(Tlist,Cvlist,'-x')
    plt.show()
    exit()
    
    Cvlist = []
    for t in np.arange(p.Tsequence[0],p.Tsequence[-1],0.001):
        p.T = t
        Cvlist.append(p.Cv)    
    plt.plot(np.arange(p.Tsequence[0],p.Tsequence[-1],0.001),Cvlist,'-')
    plt.show()
    
    #for i,p in enumerate(p.T_nonoptimized_sequence):
    #    print p
            
    exit()

    plt.plot(p.Tsequence[0:-1],p.asymmsequence,'-x')
    #dat = np.loadtxt("/scratch/ab2111/todexter/davids/LJ31/benchmarking/tspacing/hsaest/new_Test/temperatures")
    #plt.plot(dat,'-x')
    plt.show()
    
    #out = p.paccest_list()
    #p.ExactGammaAve= True
    #out2 = p.paccest_list()
    #print out[0],out2[0]

if "__name__==main()":
    #main()
    pass