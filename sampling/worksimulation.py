import numpy as np
from pele.mc import MonteCarlo
from pele.takestep import displace

class WorkSimulation:
    
    def __init__(self,system,reservoir,coords,rescoords,temperature,tau,xstepsize=0.0025):
        self.tau = tau
        
        self.stepsize = xstepsize
        self.mcstep = displace.RandomDisplacement(stepsize=self.stepsize)
        self.system = system
        self.Esys = self.system.get_potential()
        self.reservoir = reservoir
        self.Temperature = temperature
        #self.MCrelax = MonteCarlo(coords,self.Esys,temperature=self.Temperature,takeStep=self.mcstep,iprint=10000000)
        
        self.coords = coords
        self.rescoords = rescoords
        
    def run(self,reservoirsimulation,coords,rescoords):
        
        # p ==  1: Hsa -> phys
        # p == -1: Hsa <- phys
        
        (self.rescoords,workF) = self.switch(rescoords,1)
        (cpert,workR) = self.switch(coords,-1)
        
        print "all tau", workF, workR
        print "res sim", reservoirsimulation.calcWorkvals(coords,rescoords)[:]
        
        return workF,workR
       
    def switch(self,x,p):

        dlambda = 1./(self.tau+1)
        w = 0.0
        
        Ehsa,well = self.reservoir.getEhsa(x)
        Ephys = self.Esys.getEnergy(x)
            
        for l in np.linspace(dlambda,1,self.tau):
            #Ehsa = self.reservoir.getEhsa(x)
            #Ephys = self.Esys.getEnergy(x)
            w = w + p * dlambda * (Ephys-Ehsa)
            
            x0 = np.copy(x)
            (Ephys0,Ehsa0) = (Ephys,Ehsa)
            
            self.mcstep.takeStep(x)
            Ehsa,well = self.reservoir.getEhsa(x,indexmin=well)
#            Ehsa,well = self.reservoir.getEhsa(x)
            Ephys = self.Esys.getEnergy(x)
            (x,Ehsa,Ephys,a) = self.MCaccept(x,x0,Ehsa,Ephys,Ehsa0,Ephys0,l,p)
            #print Ehsa,Ephys,a,l,p
        #Ehsa = self.reservoir.getEhsa(x)
        #Ephys = self.Esys.getEnergy(x)
        w = w + p * dlambda * (Ephys-Ehsa)
        
        return x , w/self.Temperature
    
    def MCaccept(self,x,x0,Ehsa,Ephys,Ehsa0,Ephys0,l,p):
        
        E_l = self.calcElambda(Ehsa,Ephys,l,p)    
        E_l0 = self.calcElambda(Ehsa0,Ephys0,l,p)    
        
        dE = E_l - E_l0
        
        #print E_l,E_l0,dE,l,p
        
        ran = np.random.random()
        
        accept = ran < np.exp(-dE/self.Temperature)
        if accept==False:
            x = x0
            (Ehsa,Ephys)=(Ehsa0,Ephys0)
            
        return x,Ehsa,Ephys,accept
    
    def calcElambda(self,Ehsa,Ephys,l,p):
        #p==1 : HSA -> phys
        #p==-1: HSA <- phys
        #Ehsa = self.reservoir.getEhsa(x)
        #Ephys = self.Esys.getEnergy(x)
        
        if p:
            return Ehsa + l * (Ephys - Ehsa)
        elif p==-1:
            return Ehsa + (1-l) * (Ephys - Ehsa)    