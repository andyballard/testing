import hsa_utils.utils as utils
from bh_sampling import *
from pele.mindist import MinPermDistAtomicCluster, ExactMatchAtomicCluster
from pele.mindist.rmsfit import findrotation
#from pele.mindist.exact_match import ClusterTransformation
from pele.mindist._minpermdist_policies import TransformAtomicCluster

class HSAMapping():

    def __init__(self,db=None):
        
        pass

    def apply_map(self,m,coords):
        
        coords0 = m.coords
        nm = m.hessian_eigs[0]
        evals = nm.eigenvalues
        vectors = nm.eigenvectors

        c = self.get_cnumbers(coords,vectors)
        alpha = np.zeros(len(evals))
        alpha[6:] = 0.00025 * np.max(evals) * np.divide(1.0,evals[6:])
        #print evals,alpha
        x = np.zeros(len(coords))
        #x = np.copy(coords)

        for i in range(len(evals)):
            x = x + (1.+ alpha[i]) * c[i] * vectors[i] 
    
        return x
        
    def apply_map_inverse(self,m,coords):
        
        #coords0 = m.coords
        nm = m.hessian_eigs[0]
        evals = nm.eigenvalues
        vectors = nm.eigenvectors

        #dist, dummy, newcoords = self.mindist(coords0, coords)

        c = self.get_cnumbers(coords,vectors)
        alpha = np.ones(len(evals))
        alpha[6:] = (np.max(evals)*1./0.00025) * evals[6:]
        
        x = np.zeros(len(coords))
        
        for i in range(len(evals)):
            x = x + alpha[i] * c[i] * vectors[i]
            
        #for i,evec in enumerate(vectors):
        #    coords = coords + alpha[i] * evec 
                
        return x        
    
    def get_cnumbers(self,coords,evecs):
        
        c = np.zeros(len(evecs))
        for i,evec in enumerate(evecs):
            c[i] = np.dot(evec,coords)
        return c
    
        
class EinsteinCrystal():

    def __init__(self):
        
        pass
    
    def sample_crystal(self,temperature,kappa):
    
        # sample configuration from an Einstein crystal with lattice points given by minimum m
        
        m = self.db.minima()[0]

        x0 = m.coords
        x = x0
        ranvec = np.array([np.random.normal() for i in range(len(x))])
        for i in range(len(x)): 
            x[i] = x0[i] + np.sqrt(temperature/kappa) * ranvec[i]

        Ehsa = m.energy + 0.5 * temperature * np.dot(ranvec,ranvec)
        E = self.pot.getEnergy(x)
    
        return x,Ehsa,E

    def calc_crystal_energy(self,coords,kappa):
        
        # calculate energy of configuration coords as einstein crystal
        
        m = self.db.minima()[0]
        
        x0 = self.quench_homogeneous(coords).coords
        #x0 = m.coords
        
        E = m.energy + 0.5 * kappa * np.dot(coords-x0,coords-x0)
        #for i in range(len(coords)):
        #    E = E + 0.5*kappa*(coords[i]-x0[i])**2
        
        return E
    
class SquareWell():

    def __init__(self):
        
        pass

    def sample_square_well(self,R):
        
        mcoords = self.db.minima()[0].coords
        N = len(mcoords)
        ranvec = np.array([np.random.normal() for i in range(N)])
        r = np.dot(ranvec,ranvec)
        r = np.sqrt(r)
        
        unit_ran = np.random.random()
        coords = unit_ran**(1./N) * ranvec / r
        
        coords = R * coords
        
        return mcoords + coords
    
    def calc_square_well_energy(self,coords,R):
        
        mcoords = self.db.minima()[0].coords
        
        r = self.calc_r(coords)
        
        if r < R:
            return 0.0
        else:
            return float("inf") 

class HSA():
    
    def __init__(self,system,Treservoir,kappa,db):
        
        self.Treservoir = Treservoir
        self.kappa = kappa
        self.db = db
        self.rescoords = np.copy(db.minima()[0].coords)
        self.Ehsa = db.minima()[0].energy
        self.quench = system.get_minimizer()
        self.pot = system.get_potential()
        
        permlist = [range(system.natoms)]
        
        self.mindist = MinPermDistAtomicCluster(niter=100, permlist=permlist, verbose=False)
        self.match = ExactMatchAtomicCluster(permlist=permlist)

        # current well of reservoir
        self.well = None
        # get database with eigvals / eigvecs from hsa_utils
        #minfile = hsa_utils.MinDatabaseExtractor("/home/ab2111/projects/testing/input_files/lj31.db")
        #(E,vib,pgo) = minfile.get_min_arrays()
        #self.db = minfile.db
        E = np.array([ m.energy for m in db.minima() ])
        pgo = np.array([ m.pgorder for m in db.minima() ]) 
        vib = np.array([ m.fvib for m in db.minima() ] )
        
        self.Pw = utils.calcPw(E,pgo,vib,T=self.Treservoir,kappa=self.kappa)
        #self.Pw =  hsa_utils.MinDataProcessor(minfile).calcPw(T=self.Treservoir, kappa=self.kappa)

    def sample(self):
        
        # forward direction permutations 
        m = utils.select_minimum(self.Pw,self.Treservoir)
        self.well = m
        #self.rescoords,self.Ehsa = self.sample_HSA_basin(self.db.minima()[m],self.kappa,self.Treservoir)
        r,self.Ehsa = self.sample_HSA_basin(self.db.minima()[m],self.kappa,self.Treservoir)
        
        return r
        #Ehsa = self.getEhsa(m,coords)
        
 
    def sample_HSA_basin(self, m, k, T):
        """ Returns a configuration with energy less than Emax sampled m-th basin    
            this assumes the harmonic approximation and is exact in the harmonic approximation
            
            Parameters
            ----------
        
            m : Minimum object
                normal mode frequencies and associated eigenvectors
            k : integer
                number of degrees of freedom
            T : float
                temperature
        """
        m = self.db.minima()[0]
        nm = m.hessian_eigs[0]
        evals = nm.eigenvalues
        vectors = nm.eigenvectors
        nzero = len(evals) - k

        #f = np.random.randn(k,1)
        f = np.random.randn(k)

        # create the random displacement vector
        dx = np.zeros(m.coords.shape)
        for i in range(k):
            if evals[i+nzero] > 1e-4:
                dx += f[i] * vectors[:,i+nzero] / np.sqrt(evals[i+nzero]/T)
        
        Ehsa = 0.5 * T * np.dot(f,f)

        return m.coords+dx,m.energy + Ehsa
    
    def align_coords_to_database(self,coords,indexmin):
        """ Procedure to align coords C to database config Cm:
            1. quench C to Cq
            2. choose minimum m based upon energy of Cq
            3. Find transformation T s/t T Cq = Cm
            4. apply Cprime = T C
            5. Do rotation of Cprime to align with Cm
        """
        
        coordsq = self.quench(coords).coords

        if indexmin != None:
#            pass
            m = self.db.minima()[indexmin]
        
        elif indexmin == None:
            (e,g,Hess) = self.pot.getEnergyGradientHessian(coordsq)
            #Ehstold = 0.5 * np.dot(coords-coordsq,np.dot(Hess,coords-coordsq))
            
            # TODO: make sure we are in minimum m
            de = [abs(1.-m.energy/e) for m in self.db.minima()]
            indexmin,demin = min(enumerate(de),key=lambda s: s[1])
        
            m = self.db.minima()[indexmin]
        
            transformation = self.match.find_transformation(m.coords, coordsq)

            if transformation is False:
                print "they are not the same structure"
            else:
                self.match.apply_transformation(coords, transformation)

        
        # Align T coords to m.coords, only rotations needed:
        #rotation = ClusterTransoformation()
        #rotation.rotation = rot
        dist,rot = findrotation(m.coords,coords)
        transform = TransformAtomicCluster()
        #print coords[0],
        transform.rotate(coords,rot)
        #print coords[0]
        #self.match.apply_transformation(coords, rotation)
        #print dist,rot
        
        return coords,indexmin
    
    def getEhsa(self,coords,indexmin=None):
        
        # align current configuration to minimum in database
        coords,indexmin = self.align_coords_to_database(coords,indexmin)
        #indexmin = 0        
        m = self.db.minima()[indexmin]
        
        nm = m.hessian_eigs[0]
        evals = nm.eigenvalues
        vectors = nm.eigenvectors  
    
        dX = coords - m.coords
        
        # set eigenvectors corresponding to translation / rotation to zero
        for j1 in range(0,6): vectors[:,j1] = 0.0

        Ehsa = 0.0
        # Explicit calculation
        #for j1 in range(len(dX)):    
        #    for j2 in range(len(dX)):
        #        for j3 in range(len(dX)):
        #            Ehsa = Ehsa + 0.5*dX[j1]*vectors[j1,j2]*evals[j2,]*vectors[j3,j2]*dX[j3]
        Ehsa = 0.5 * np.dot(dX.transpose(),np.dot(vectors,np.dot(np.diag(evals),np.dot(vectors.transpose(),dX))))
        
        #Ealt = np.dot(np.diag(evals),np.dot(vectors.transpose(),dX))
        #Ehsa = 0.5 * np.dot(coords-m.coords,np.dot(Hess,coords-m.coords))        

        return m.energy + Ehsa , indexmin
    
    def getEhsa_quench(self,coords):
        
        
        coordsq = self.quench(coords).coords
        (e,g,Hess) = self.pot.getEnergyGradientHessian(coordsq)
        
        # TODO: make sure we are in minimum m
        de = [abs(1.-m.energy/e) for m in self.db.minima()]
        indexmin,demin = min(enumerate(de),key=lambda s: s[1])
        m = self.db.minima()[indexmin]
        
        #for m in self.db.minima():
        #    de = abs(1.-m.energy/e)
        #    if de <= 0.01: 
        #        break 
            #m = self.db.minima()[0]
        
        Ehsa = 0.5 * np.dot(coords-coordsq,np.dot(Hess,coords-coordsq))

        #print indexmin,m.energy + Ehsa
        
#        dist, dummy, newcoords = self.mindist(x0, coords)
#        coords = newcoords
        return m.energy + Ehsa
    
def calc_r(self,coords):
    
    #mcoords = self.db.minima()[0].coords
    
    ret = self.quench_homogeneous(coords)
    mcoords = ret.coords
    menergy = ret.energy
    
    r = 0.0
    for i in range(len(mcoords)):
        r = r + (coords[i]-mcoords[i])**2
    r = np.sqrt(r)

    return r

