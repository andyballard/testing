import numpy as np
import matplotlib.pyplot as plt

class System(object):
    
    def __init__(self,states,E,S):
        
        self.states = states
        self.E = E
        self.S = S
        
class Replica(System):
    
    def __init__(self,temperature,*args,**kwargs):
        
        System.__init__(self,*args,**kwargs)
        self.temperature = temperature
        
        self.Prob = np.exp(-self.E / self.temperature + self.S)
        self.Prob = self.Prob / np.sum(self.Prob)
        

class ParallelTempering(object):
    
    def __init__(self,system,temperatures):
        
        self.temperatures = temperatures
        self.replicas = []
    
        self.Pacc = np.zeros(len(self.temperatures)-1)
            
        for t in self.temperatures:
            self.replicas.append(Replica(t,system.states,system.E,system.S))
    
        self.calc_Pacc()
        
        
    def calc_Pacc(self):
        
        for i in range(len(self.replicas)-1):
          
            r = self.replicas[i]
            rp= self.replicas[i+1]

            dBeta = 1./rp.temperature - 1./r.temperature

            
            pacc = 0
            for s in range(len(r.states)):
                for sp in range(len(rp.states)):
                    dE = rp.E[sp]-r.E[s]            
                    p = r.Prob[s]
                    pp= rp.Prob[sp]
                    #print p,pp,dBeta,dE,np.minimum(1.,np.exp(dBeta*dE))
                    pacc = pacc + p * pp * np.minimum(1.,np.exp(dBeta*dE))
    
            self.Pacc[i] = pacc
            

class Dynamics(ParallelTempering):
    
    def __init__(self,*args,**kwargs):
        
        ParallelTempering.__init__(self,*args,**kwargs)

        # dimension of rate matrx: D x D
        self.D = len(self.replicas) 
        self.Matrix = np.zeros((self.D,self.D))
        
        self.read_in_elements()
        
    def read_in_elements(self):
        
        for i in range(self.D-1):
            self.Matrix[i,i+1] = 0.5 * self.Pacc[i]
            self.Matrix[i+1,i] = self.Matrix[i,i+1]
            
        for i in range(self.D):
            self.Matrix[i,i] = 1.0 - np.sum(self.Matrix[:,i])
            #self.Matrix[i,i] = 1.0 - np.sum(self.Matrix[i,:])
            
        #for i in range(self.D):
        #    print np.sum(self.Matrix[:,i])
        #exit()
                        
def main():
    
    states = ["A","B"]
    S = np.array((0.0,1.0))
    E = np.array((0.0,5.0))
    temperature = 1.0
    
    system = System(states,E,S)
    
    #pt = ParallelTempering(system,np.arange(0.1,1.1,0.1))
    dynamics = Dynamics(system,np.arange(0.1,1.1,0.1))
    #dynamics = Dynamics(system,np.array([0.1,1.1]))
    print dynamics.Matrix
    evals,evecs = np.linalg.eig(dynamics.Matrix)

    for i,l in enumerate(evals):
        print l, evecs[:,i]
        
    plt.plot(sorted(evals))
    plt.show()
    #for i in range(dynamics.D):
        
if __name__ == "__main__":
    main()
    