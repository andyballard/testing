import numpy as np
from testing.landscape import MinimaDistribution
import matplotlib.pyplot as plt

f="/scratch/ab2111/tosinister/clusters/LJ31/tspacing/min.data"
kappa=0.5*(3*31-6 )
#f="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/geom_from_gmin_mindata/M12_res_at_transition/min.data"
#kappa=0.5*(3*75-6 )

Tlist = np.arange(0.0125,0.2,0.002)

dist = MinimaDistribution(Tlist[0],minfile=f,kappa=kappa)

S = []
Cv = []

for t in Tlist:
    dist.T = t
    sw = -np.average(np.log(dist.Pwell),weights=dist.Pwell)
    S.append(sw)
    Cv.append(dist.Cv)
plt.plot(Tlist,S,'-o')
#plt.plot(Tlist,(np.array(Cv)-80.)/40.,'-o')
plt.show()