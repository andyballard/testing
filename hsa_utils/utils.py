import numpy as np
from pele.thermodynamics import free_energy, get_thermodynamic_information
import pele.systems
#from bh_sampling import *
import matplotlib.pyplot as plt
from decimal import Decimal

class MinDataProcessor:

    def __init__(self,extractor):
        self.extractor = extractor
        
    def get_energy_pgo_vib(self):
        return self.extractor.get_min_arrays()
    
    
class MinFileExtractor:
    
    def __init__(self,minfile,pointsfile=None,system=None,dbname=None):
        self.minfile = minfile
        self.pointsfile = pointsfile
        self.system = system
        self.dbname = dbname
        
    def get_min_arrays(self):
        dat = np.loadtxt(self.minfile)
        E = dat[:,0]
        vib = dat[:,1]
        pgo = dat[:,2]
        
        return E,vib,pgo
    
    def create_db(self,natoms):

        (E,vib,pgo) = self.get_min_arrays()
        self.db = self.system.create_database(self.dbname)
        
        coordsall = np.loadtxt(self.pointsfile)
        coordsall2 = np.reshape(coordsall,3*len(coordsall))
        Nminima = len(coordsall2)/(3*natoms)
        print np.shape(coordsall)
        coordlist = np.split(coordsall2,Nminima)        
        for i in range(Nminima):
            #coords = np.array(coordsall[i*3*natoms:(i+1)*3*natoms])
            self.db.addMinimum(E[i],coordlist[i])
            
        get_thermodynamic_information(self.system,self.db)   
        
        return self.db    

class MinDatabaseExtractor:
    def __init__(self,system,dbname):
        self.system = system
        self.db = self.system.create_database(dbname)
        get_thermodynamic_information(self.system,self.db)
    
    def get_min_arrays(self):
        
        E = np.array([ m.energy for m in self.db.minima() ])
        pgo = np.array([ m.pgorder for m in self.db.minima() ]) 
        vib = np.array([ m.fvib for m in self.db.minima() ] )
        
        return E,vib,pgo

def calcPw(E,pgo,vib,T=None,kappa=None,my=False):
    
    F=free_energy(E,pgo,vib,T,kappa,h=1)
    Fmin=np.min(F)
    P=np.exp(-(F-Fmin)/T)
    dummy=0.0

    try:
        for i in range(len(F)): dummy = dummy + P[i]
    except:
        dummy = P

    P = P / dummy

    return P

def calcCDF(Pw):
   
    c=0.
    cdf = []
    for p in Pw:
        c = c + p
        cdf.append(c)

    return cdf

def calcEave(T,Pw,E,kappa):

    Eave = 0.0
    
    for i in range(len(Pw)):
        Eave = Eave + Pw[i] * E[i]

    Eave = Eave + kappa * T

    return Eave


def calcCv(T,Pw,E,kappa):

    var=0.0
    ave=0.0
  
    #ave = Decimal(np.dot(E,Pw))
    #var = Decimal(np.dot(E**2,Pw))-ave**2
    for i in range(len(Pw)):
        var = var + E[i]*E[i]*Pw[i]
        ave = ave + E[i]*Pw[i]

    var = var - ave*ave
    C = kappa + var/(T**2)
    #C = Decimal(kappa) + var/(Decimal(T**2))
    return C

def calcHel(T,Pw,hel):

    h = 0.0        
    for i in range(len(Pw)):
        h += hel[i]*Pw[i]
        

    return h

def calcPhel(T,Pw,hel,nbins):
    
    hist = np.histogram(hel,bins=nbins,weights=Pw,normed=True,range=(0.,1.))
    
    return hist
        

def calcCv_curve(Tstart,Tfinish,Pw,E,kappa,Ncalcs=100):

    curve = []
    for T in np.arange(Tstart,Tfinish,(Tfinish-Tstart)/Ncalcs):
        C = calcCv(T,Pw,E,kappa)
        curve.append((T,C))
    
    return curve

def calcHel_curve(Tstart,Tfinish,Pw,hel,Ncalcs=100):

    curve = []
    for T in np.arange(Tstart,Tfinish,(Tfinish-Tstart)/Ncalcs):
        hel = calcHel(T,Pw,hel)
        curve.append((T,hel))
    
    return curve

def get_F_E_TS(energy, pgorder, fvib, kBT, kappa, h=1.0):  
    beta = 1./kBT
    Fpg = np.log(pgorder)/beta
    Ffrq = kappa*np.log(beta) + 0.5*fvib /beta 
    Ffrq = kappa*(np.log(beta) + np.log(h)) / beta + 0.5*fvib/beta 
    return  energy + Ffrq + Fpg, energy, Ffrq + Fpg

def select_minimum(Pw,T):

    ran = np.random.random()
    dummy = 0.0
    i = -1
    while dummy < ran:
        i = i + 1
        dummy = dummy + Pw[i];
        
    return i 

def writeWvals(datF,datR,swapbin,outfile):

    datF = np.array(datF)
    datR = np.array(datR)    
    np.savetxt(outfile,zip(datF,datR,datF+datR,swapbin),fmt="%1.4f\t%1.4f\t%1.4f\t%d")

def plotBennettODM(datF,datR,needhist=True,minval=None,maxval=None,outfile=None):
    """ Plot the Bennett overlapping distribution method, 
    with input work (or \Delta u) values from datF, datR. 
    work values are dimensionless.
    """
    datF = np.array(datF)
    datR = -np.array(datR)
    
    if minval == None:
        minval = min(np.vstack((datF,datR)).flatten())
    if maxval == None:
        maxval = max(np.vstack((datF,datR)).flatten())
    bins = np.linspace(minval,maxval,40)

    if needhist:
        hF = np.histogram(datF, bins, normed=True)
        hR = np.histogram(datR, bins, normed=True)
    else: 
        hF = datF
        hR = datR
    
    # Get correct bins, centered at value of histogram value
    b = np.zeros(len(hF[1])-1)
    for i in range(len(hF[1])-1):
        b[i] = 0.5 * (hF[1][i+1]+hF[1][i])
    
    # Bennett functions: LR-LF = \Delta f
    LF = np.log(hF[0]) - 0.5 * b
    LR = np.log(hR[0]) + 0.5 * b 
        
    plt.plot(LF)
    plt.plot(LR)
    plt.plot(LR-LF)
    #plt.xlim([minval,maxval])
    plt.legend(["$L_F$","$L_R$","$L_R-L_F$"])
    
    if outfile:
        plt.savefig(outfile)
    else: plt.show()


def write_GMIN_data_from_db(db,mindata_out,mincoords_out,minvec_out,allvecs=False):
    
    with open(mindata_out,"w") as file:
        for m in db.minima():
        
            file.write((str(m.energy)+" " + str(m.fvib)+" " + str(m.pgorder)+" 0.0 0.0 0.0\n"))
            #np.savetxt(file,np.array(m.energy, m.fvib, m.pgorder))
            #np.savetxt(mindata_out,m.energy,m.fvib,m.pgorder)
        
    with open(mincoords_out,"w") as file2:
        for m in db.minima():
            c = m.coords
            for i in range(0,len(c),3):
                file2.write((str(c[i])+" " + str(c[i+1])+" " + str(c[i+2])+"\n"))
            

    # write vector.dump file
    file3 = open(minvec_out,"w")
    for m in db_new.minima():
        write_vector_dump(file3,m,allvecs)
    
    """
    with open(minvec_out,"w") as file3:
        for m in db.minima():
            #nm = m.normal_modes
            nm = m.eigenvalues
            print nm[0]
            exit()
            for i in range(0,len(c),3):
                file3.write((str(c[i])+" " + str(c[i+1])+" " + str(c[i+2])+"\n"))
    """
    
def makedb_blj(self,minimum=0):
    """ WARNING: Not looked at, may not be useful"""

    db_blj = self.db_blj
    
    quench = self.system_blj.get_minimizer()

    c0 = self.db.minima()[minimum].coords
    
    catoms0 = np.reshape(c0,(self.natoms,3))

    """
    # only one min
    cperm = np.copy(c0)
    catoms = np.copy(catoms0)
    (catoms[self.natoms-1],catoms[self.natoms-10])=(catoms0[self.natoms-10],catoms0[self.natoms-1])
    cperm = np.reshape(catoms,3*self.natoms)
    ret = quench(cperm)
    m = db_blj.addMinimum(ret.energy, ret.coords)
    """
    for i in range(self.natoms):
        catoms = np.copy(catoms0)
#             cperm = np.copy(c)
        (catoms[self.natoms-1],catoms[i])=(catoms0[i],catoms0[self.natoms-1])
        cperm = np.reshape(catoms,3*self.natoms)
        ret = quench(cperm)
        m = db_blj.addMinimum(ret.energy, ret.coords)

    get_thermodynamic_information(self.system_blj,db_blj)

    E = np.array([ m.energy for m in db_blj.minima() ])
    pgo = np.array([ m.pgorder for m in db_blj.minima() ]) 
    vib = np.array([ m.fvib for m in db_blj.minima() ] )

    self.Pw = self.calcPw(E,pgo,vib,self.system.k,self.T)
    # TODO : adjust Pw vals to reflect degeneracies in db creation

    return db_blj

def write_vector_dump(file,m,allvecs):
     
    nm = m.hessian_eigs[0]
    eval = nm.eigenvalues
    evec = nm.eigenvectors
    print eval[0:7]
     
    if allvecs: Nstart=0
    if not allvecs: Nstart=6
     
    for i,l in enumerate(eval[Nstart:]):
        file.write((str(l)+"\n"))
        #for j in range(0,len(evec[i]),3):
        #    file.write((str(evec[i,j])+" "+str(evec[i,j+1])+" ")+str(evec[i,j+2])+"\n")
        for j in range(0,len(evec[:,i]),3):
            file.write((str(evec[j,i])+" "+str(evec[j+1,i])+" ")+str(evec[j+2,i])+"\n")
    
def transfer_database(db,system,maxminima=None):
    
    db_new = Database()
    
    if not maxminima:
        maxminima = len(db.minima())
        
    for m in db.minima()[:maxminima]:
        E = np.copy(m.energy)
        coords = np.copy(m.coords)
        pgorder = np.copy(m.pgorder)
        fvib = np.copy(m.fvib)
        print E,pgorder,fvib
        #print coords
        #db_new.addMinimum(E,coords,commit=True,pgorder=pgorder,fvib=fvib)
        mnew = db_new.addMinimum(E,coords)
        
        
    pot = system.get_potential()

    db_new = get_thermodynamic_information(system,db_new)

    return db_new

def move_along_vector(coords,vector,delta=1.0):

    c = np.copy(coords)
    for co,i in enumerate(coords):

        c[i] = c[i] + delta * vector[i]

    return c

def get_eigensystem(h):

    hnew = np.copy(h)
    
    eva,ev = np.linalg.eigh(hnew)
    
    return ev,eva

if __name__=="__main__":
    
    """
    from pele.storage import Database
    natoms=75
    #natoms=31
    system = pele.systems.LJCluster(natoms=natoms)
    
    db = Database("/scratch/ab2111/tosinister/clusters/LJ75/get_minima_info/lj75.db",createdb=False)
    #db = Database("/home/ab2111/projects/testing/input_files/lj31_183.db",createdb=False)
    
    #db_new="/scratch/ab2111/tosinister/clusters/LJ75/get_minima_info/lj75_new.db",
    db_new = transfer_database(db,system,maxminima=100)


    dir="/scratch/ab2111/tosinister/clusters/LJ75/tspacing/test/new"
    #dir="/scratch/ab2111/tosinister/clusters/LJ31/testnew"
    write_GMIN_data_from_db(db_new,dir+"min.data",dir+"points.min",dir+"vector.dump",allvecs=False)
    """
    pass

class ODM():
    
    def __init__(self,datF,datR,distrange=(None,None),nbins=100):
        
        self.datF = datF
        self.datR = datR
        self.nbins = nbins
        self.distrange = distrange
        
        self.wFdist = None
        self.wRdist = None
        
        self.LF = None
        self.LR = None
        self.deltaL = None
        
        self.get_hist()        
        self.get_ODM_functions()

    def get_hist(self):
        
        self.datF = np.array(self.datF)
        self.datR = -np.array(self.datR)
    
        minval,maxval  = self.distrange
        
        if minval == None:
            minval = min(np.vstack((self.datF,self.datR)).flatten())
        if maxval == None:
            maxval = max(np.vstack((self.datF,self.datR)).flatten())
        bins = np.linspace(minval,maxval,self.nbins)

        self.wFdist = np.histogram(self.datF, bins, normed=True)
        self.wRdist = np.histogram(self.datR, bins, normed=True)        
    
    def get_ODM_functions(self):
        

        # Get correct bins, centered at value of histogram value
        b = np.zeros(len(self.wFdist[1])-1)
        for i in range(len(self.wFdist[1])-1):
            b[i] = 0.5 * (self.wFdist[1][i+1]+self.wFdist[1][i])
    
        # Bennett functions: LR-LF = \Delta f
        LFval = np.log(self.wFdist[0]) - 0.5 * b
        LRval = np.log(self.wRdist[0]) + 0.5 * b 
        
        deltaLval = LRval-LFval
    
        self.LF = (b,LFval)
        self.LR = (b,LRval)
        self.deltaL = (b,deltaLval)
